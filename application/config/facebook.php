<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
|  Facebook API Configuration
| -------------------------------------------------------------------
|
| To get an facebook app details you have to create a Facebook app
| at Facebook developers panel (https://developers.facebook.com)
|
|  facebook_app_id               string   Your Facebook App ID.
|  facebook_app_secret           string   Your Facebook App Secret.
|  facebook_login_type           string   Set login type. (web, js, canvas)
|  facebook_login_redirect_url   string   URL to redirect back to after login. (do not include base URL)
|  facebook_logout_redirect_url  string   URL to redirect back to after logout. (do not include base URL)
|  facebook_permissions          array    Your required permissions.
|  facebook_graph_version        string   Specify Facebook Graph version. Eg v2.6
|  facebook_auth_on_load         boolean  Set to TRUE to check for valid access token on every page load.
*/
//$config['facebook_app_id']              = '384772732376388';
//$config['facebook_app_id']              = '595292370971145';
$config['facebook_app_id']              = '243549953202360';
//$config['facebook_app_secret']          = '03da50b5a4797e3e24afa8104020b29f';
//$config['facebook_app_secret']          = '939bf07df74ff5d34710d34a9797d7ab';
$config['facebook_app_secret']          = 'a5edbff463b276a4d763f54059640b8d';
$config['facebook_login_type']          = 'web';
$config['facebook_login_redirect_url']  = 'User/facebookSignup';
$config['facebook_logout_redirect_url'] = 'User/logout';
$config['facebook_permissions']         = array('email');
$config['facebook_graph_version']       = 'v2.6';
$config['facebook_auth_on_load']        = TRUE;