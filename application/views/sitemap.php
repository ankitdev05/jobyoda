<?php 
if($this->session->userdata('usersess')){
	$usersess =  $this->session->userdata('usersess'); 
}

?>
<?php include('header.php'); ?>
<style type="text/css">

#contactform .error {
    color: #f00;
    margin: 0 0 10px 0;
    font-size: 12px;
    width: 100%;
}

label.error{
    left: 0;
    bottom: -4px;
    font-size: 11px;
}
</style>

<div class="managerpart">
     <div class="container-fluid">
        <div class="row">
         
		   <div class="col-md-9 expanddiv">
		   <div class="innerbglay" style="padding-top: 20px;">
			
				 <div class="contentare outerlgos">

				 <div class="row">
				 <div class="col-md-6">
				 <div class="addrsarea">

                       <div class="formmidaress" style="    margin: 0 28px;">
		             <h5>Useful Links:</h5>
						<div class="halfdividedft">
                           <ul>
                              <li><a href="https://jobyoda.com/privacy_policy">Privacy Policy </a></li>
                              <li><a href="https://jobyoda.com/terms">Terms of Service </a></li>
                              <li><a href="https://jobyoda.com/contact">Contact Us </a></li>
                              <li><a href="https://jobyoda.com/about">About Us </a></li>
                             <!-- <li><a href="#">Support  </a></li> -->
                              <li><a href="#">How it Works </a></li>
                           </ul>
                        </div>
						
						<div class="halfdividedft">
                           <ul>
                              <li><a href="<?php echo base_url(); ?>user/job_listing?cat=Mw==">Jobs</a></li>
                              <li><a href="#" onclick="showmodel()">Sign Up</a></li>
                              <li><a href="<?php echo base_url(); ?>recruiter/recruiter/">Recruiter's Portal</a></li>
                              <li><a href="#" data-toggle="modal" data-target="#exampleModalCenter4">Jobseekers</a></li>
                            
                           </ul>
                        </div>
		           
		             </div>  

                </div>

				 </div>
				 <div class="col-md-6">
  				
           		</div>
           		 </div>
	       		</div>
	       </div>
		   </div>
		   
		</div>
	 </div>
</div>

<?php include('footer.php');?>
<script type="text/javascript">
  $(document).ready(function(){
         $('#contactform').validate({
              rules: {
                fname: {required:true,minlength:1,maxlength:30},
                email: {
                  required: true,
                  email: true,
                  maxlength:30
                },
                phone:{
                  required: true,
                  number: true,
                  //minlength:8,
                  //maxlength:10
                },
                message:{
                	required:true,
                }
              },
              messages: {
                fname: {required:'The name field is required'},
                email: {required:'The email field is required',remote:"This Email id is already registered.", maxlength:"The email must not contain more than 30 characters"},
                phone: {
                     required:"The phone field is required",
                    // maxlength:"Please enter no more than {0} number.",
                     //minlength:"Please enter at least {0} number."
                },
                message: {
                	required:"The message field is required",
                }
              }
         });
   });
</script>