<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="<?php echo base_url().'recruiterfiles/';?>images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
   </head>
   <style>
      .under{
            text-decoration: underline;
            color: #084d87;
      }
      a:hover {
      text-decoration: none;
      }
      .tech i.fa.fa-ellipsis-h {
      float: right;
      }
      .progr span.progress-bar-tooltip {
      position: initial;
      color: #fff;
      }
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 11px;
      line-height: 0;
      padding: 10px;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .fall {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 23px 10px;
      }
      .progress-bar{
      background-color: #47b476;
      }
      .details {
      float: left;
      width: 100%;
      padding: 5px 17px;
      background-color: #47b476;
      color: #fff;
      margin: 15px;
      border-radius: 20px
      }
      .leftdetails {
      float: left;
      width: 100%;
      }
      .left {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px;
      }
      .detail span {
      display: block;
      float: right;
      margin: 12px 8px;  
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detail strong {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px 55px;
      font-weight: 700;
      font-size: 20px;
      }
      .detail, .detai {
      float: left;
      width: 100%;
      }
      .detai span {
      display: -webkit-box;
      width: 100%;
      font-weight: 600;
      margin: 0px;
      padding: 12px 0px;
      font-size: 20px;
      }
      .progras {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detai strong {
      float: right;
      font-size: 20px;
      }
      .detail p, .detai p {
      font-weight: 600;
      font-size: 16px;
      color: #000;
      }
      .detail .progress span {
      position: absolute;
      right: 0px;
      font-size: 13px;
      color: #fff;
      border: 0px solid#ddd;
      }
      .head {
      float: left;
      width: 100%;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .num {
      float: left;
      margin: 20px 0px;
      }
      .para {
      float: left;
      width: 100%;
      border: 1px solid#dddd;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .tech {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      margin: 10px 0px;
      }
      .search-container {
      float: left;
      }
      .search button {
      padding: 7px 4px;
      color: #fff;
      background-color: #47b476;
      border: none;
      width: 36%;
      }
      .upload a {
      color: #ffffff;
      text-decoration: none;
      background: #47b476;
      padding: 10px;
      border-radius: 50px;
      }
      .search input[type="text"] {
      padding: 9px 6px;
      width: 63%;
      background-color: #fff;
      border: 1px solid#ddd;
      }
      .details i.fa.fa-filter {
      float: right;
      margin: 5px 0px;
      padding: 0px;
      }
      .details p {
      float: left;
      margin: 0px;
      padding: 0px;
      color: #fff;
      }
      .head p {
      color: #fff;
      }
      .para p {
      color: #fff;
      }
      .upload {
      float: right;
      padding: 8px;
      margin: px;
      }
      border: 1px solid#ddd;
      padding: 10px;
      }
      .dot {
      float: right;
      }
      .progr {
      float: left;
      width: 100%;
      margin-bottom: 18px;
      }
      .progr .progress {
      position: absolute;
      left: 9%;
      height: 20px;
      -webkit-border-radius: 8px;
      -moz-border-radius: 8px;
      -ms-border-radius: 8px;
      -o-border-radius: 8px;
      border-radius: 8px;
      top: initial;
      width: 80%;
      }
      .progr .progress-label {
      margin: 12px 0px;
      }
      .texts p {
      position: absolute;
      top: 70%;
      left: 29%;
      }
      p.dropdown-toggle {
      font-size: 34px;
      margin: -30px;
      }
      .dot {
      float: right;
      }
      .texts {
      margin-top: 20px;
      }
      .text p {
      margin: 0px;
      padding: 0px;
      font-size: 13px;
      }
      .right .progress{
      width: 150px;
      height: 150px;
      line-height: 150px;
      background: none;
      margin: 0 auto;
      box-shadow: none;
      position: relative;
      }
      .right .progress:after{
      content: "";
      width: 100%;
      height: 100%;
      border-radius: 50%;
      border: 2px solid #fff;
      position: absolute;
      top: 0;
      left: 0;
      }
      .right .progress > span{
      width: 50%;
      height: 100%;
      overflow: hidden;
      position: absolute;
      top: 0;
      z-index: 1;
      }
      .progress .progress-left{
      left: 0;
      }
      .right .progress .progress-bar{
      width: 100%;
      height: 100%;
      background: none;
      border-width: 2px;
      border-style: solid;
      position: absolute;
      top: 0;
      }
      .right .progress .progress-left .progress-bar{
      left: 100%;
      border-top-right-radius: 80px;
      border-bottom-right-radius: 80px; 
      border-left: 0;
      -webkit-transform-origin: center left;
      transform-origin: center left;
      }
      .right .progress .progress-right{
      right: 0;
      }
      .right .progress .progress-right .progress-bar{
      left: -100%;
      border-top-left-radius: 80px;
      border-bottom-left-radius: 80px;
      border-right: 0;
      -webkit-transform-origin: center right;
      transform-origin: center right;
      animation: loading-1 1.8s linear forwards;
      }
      .right .progress .progress-value{
      width: 59%;
      height: 59%;
      border-radius: 50%;
      border: 2px solid #ebebeb;
      font-size: 28px;
      line-height: 82px;
      text-align: center;
      position: absolute;
      top: 7.5%;
      left: 7.5%;
      }
      .right .progress.blue .progress-bar{
      border-color: #049dff;
      }
      .right .progress.blue .progress-value{
      color: #47b476;
      }
      .right .progress.blue .progress-left .progress-bar{
      animation: loading-2 1.5s linear forwards 1.8s;
      }
      .progress.yellow .progress-bar{
      border-color: #fdba04;
      }
      .right .progress.yellow .progress-value{
      color: #fdba04;
      }
      .right .progress.yellow .progress-left .progress-bar{
      animation: loading-3 1s linear forwards 1.8s;
      }
      .right .progress.pink .progress-bar{
      border-color: #ed687c;
      }
      .right .progress.pink .progress-value{
      color: #ed687c;
      }
      .right .progress.pink .progress-left .progress-bar{
      animation: loading-4 0.4s linear forwards 1.8s;
      }
      .right .progress.green .progress-bar{
      border-color: #1abc9c;
      }
      .right .progress.green .progress-value{
      color: #1abc9c;
      }
      .right .progress.green .progress-left .progress-bar{
      animation: loading-5 1.2s linear forwards 1.8s;
      }
      @keyframes loading-1{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
      }
      }
      @keyframes loading-2{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(144deg);
      transform: rotate(144deg);
      }
      }
      @keyframes loading-3{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(90deg);
      transform: rotate(90deg);
      }
      }
      @keyframes loading-4{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(36deg);
      transform: rotate(36deg);
      }
      }
      @keyframes loading-5{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(126deg);
      transform: rotate(126deg);
      }
      }
      @media only screen and (max-width: 990px){
      .right .progress{ margin-bottom: 20px; }
      }
      .text-center.success.text-success
      {
          font-size: 20px;
          padding-top: 40px;
      }
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                 
                  <!-- Logo -->
                        <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 psthbs dashpads"> 
                        <div class="row">
                           <div class="col-md-12 col-lg-9">
                              <div class="">
                              <?php if(isset($errors)) {?>
                                       <p class="text-center text-danger">
                                          <?php
                                                if($errors['oldPass']) {echo $errors['oldPass'];}
                                                if($errors['newPass']) {echo $errors['newPass'];}
                                                if($errors['confirmPass']) {echo $errors['confirmPass'];}
                                          ?>  
                                       </p>
                              <?php } ?>
                              <?php if($this->session->tempdata('validationError')) {?>
                                       <p class="text-center"><?php echo $this->session->tempdata('validationError'); ?></p>
                              <?php } ?>
                              <?php if($this->session->tempdata('success')) {?>
                                       <p class="text-center success text-success"><?php echo $this->session->tempdata('success'); ?></p>
                              <?php } ?>
                                 

                                 <div class="right">
                                  <?php
                                    if($comLists) {
                                      foreach($comLists as $comList) {
                                  ?>

                                    <div class="main-hda">
                                       <h3> <?php echo $comList['cname']; ?> </h3>
                                    </div>
                                    <div class="tech">
                                      <div class="row">
                                        <div class="col-md-12 col-lg-12">
                                           <div class="locnsh"> <i class="la la-map-marker"></i> <p><?php echo $comList['address']; ?></p>
                       </div> 
                                        </div>
                                      </div>
                                      <?php  $cid = $comList['cid'];?>
                                       <div class="row">
                                          <div class="col-md-12 col-lg-6">
                                             
                                             <div class="row">
                                                <div class="col-lg-3 col-3">
                                                   <div class="text">
                                                   <?php if(!empty($filters)){
                                                            $filters = $filters;
                                                      }else{
                                                            $filters ='';
                                                      }
                                                   ?>
                                                   <a href="<?php echo base_url(); ?>recruiter/jobpost/manageJobView?cid=<?php echo base64_encode($cid); ?>&status=<?php echo $filters; ?>">
                                                        <p>Jobs  <br>
                                                        Posted</p>
                                                        <p><strong><?php echo $comList['jobcount']; ?></strong></p> 
                                                   </a>
                                                      
                                                   </div>
                                                </div>
                                                
                                                <div class="col-lg-3 col-3">
                                                   <div class="text">
                                                   <a href="<?php echo base_url(); ?>recruiter/candidate/manageCandidates?status=10&id=<?php echo base64_encode($cid); ?>&filters=<?php echo $filters; ?>">
                                                        <p>Total  <br>
                                                        Applications</p>
                                                        <p><strong><?php echo $comList['alljob']; ?></strong></p> 
                                                   </a>
                                                      
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-3">
                                                   <div class="text">
                                                       
                                                   <a href="<?php echo base_url(); ?>recruiter/candidate/manageCandidates?status=1&id=<?php echo base64_encode($cid); ?>&filters=<?php echo $filters; ?>">
                                                      <p>New Application</p>
                                                      <p class="penddark"><strong><?php echo $comList['pending']; ?></strong></p>
                                                     </a> 
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-3">
                                                   <div class="text">
                                                   <a href="<?php echo base_url(); ?>recruiter/candidate/manageCandidates?status=5&id=<?php echo base64_encode($cid); ?>&filters=<?php echo $filters; ?>">
                                                      <p>On Going Applications</p>
                                                      <p><strong><?php echo $comList['open']; ?></strong></p>
                                                      </a>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-12 col-lg-6">
                                             <div class="flex-wrapper">
                                                <div class="single-chart">
                                                   <svg viewBox="0 0 36 36" class="circular-chart orange">
                                                      <path class="circle-bg" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <path class="circle" stroke-dasharray="<?php echo $comList['target']; ?>, 100" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <text x="18" y="20.35" class="percentage"><?php echo $comList['target']; ?></text>
                                                   </svg>
                                                   <a class="under" href="<?php echo base_url(); ?>recruiter/jobpost/manageJobView?cid=<?php echo base64_encode($cid); ?>&status=<?php echo $filters; ?>">
                                                   <p>Target HC</p>
                                                   </a>
                                                </div>
                                                <div class="single-chart">
                                                   <svg viewBox="0 0 36 36" class="circular-chart blue">
                                                      <path class="circle-bg" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <path class="circle" stroke-dasharray="<?php echo $comList['accept']; ?>, 100" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <text x="18" y="20.35" class="percentage"><?php echo $comList['accept']; ?></text>
                                                   </svg>
                                                   <a class="under" href="<?php echo base_url(); ?>recruiter/candidate/manageCandidates?status=6&id=<?php echo base64_encode($cid); ?>&filters=<?php echo $filters; ?>">
                                                   <p>Accepted Job Offer</p>
                                                   </a>
                                                </div>
                                                <div class="single-chart">
                                                   <svg viewBox="0 0 36 36" class="circular-chart green">
                                                      <path class="circle-bg" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <path class="circle" stroke-dasharray="<?php echo $comList['hired']; ?>, 100" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <text x="18" y="20.35" class="percentage"><?php echo $comList['hired']; ?></text>
                                                   </svg>
                                                   <a class="under" href="<?php echo base_url(); ?>recruiter/candidate/manageCandidates?status=7&id=<?php echo base64_encode($cid); ?>&filters=<?php echo $filters; ?>">
                                                   <p>Hired</p>
                                                   </a>
                                                </div>
                                                
                                                <div class="single-chart">
                                                   <svg viewBox="0 0 36 36" class="circular-chart blue">
                                                      <path class="circle-bg" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <path class="circle" stroke-dasharray="<?php echo $comList['reject']; ?>, 100" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <text x="18" y="20.35" class="percentage"><?php echo $comList['reject']; ?></text>
                                                   </svg>
                                                   <a class="under" href="<?php echo base_url(); ?>recruiter/candidate/manageCandidates?status=3&id=<?php echo base64_encode($cid); ?>&filters=<?php echo $filters; ?>">
                                                   <p>Fall-Out</p>
                                                   </a>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                
                                <?php
                                    }
                                  } else {
                                ?>

                                    <div class="main-hda">
                                       <h3>Example</h3>
                                    </div>
                                    <div class="tech">
                                      <div class="locnsh"> <i class="la la-map-marker"></i> <p>At ortigas Metro Manila</p>
                       </div>
                                       <div class="row">
                                          <div class="col-md-12 col-lg-6">
                                         
                                             <div class="row">
                                                <div class="col-lg-3 col-3">
                                                   <div class="text">
                                                      <p>Jobs <br>Posted</p>
                                                      <p><strong>0</strong></p>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-3">
                                                   <div class="text">
                                                      <p>Total <br>Applications</p>
                                                      <p  class="penddark"><strong>0</strong></p>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-3">
                                                   <div class="text">
                                                      <p>New Application</p>
                                                      <p><strong>0</strong></p>
                                                   </div>
                                                </div>
                                                <div class="col-lg-3 col-3">
                                                   <div class="text">
                                                      <p>On Going Application</p>
                                                      <p><strong>0</strong></p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-md-12 col-lg-6"> 
                                             <div class="flex-wrapper">
                                                <div class="single-chart">
                                                   <svg viewBox="0 0 36 36" class="circular-chart orange">
                                                      <path class="circle-bg" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <path class="circle" stroke-dasharray="0, 100" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <text x="18" y="20.35" class="percentage">0</text>
                                                   </svg>
                                                   <p>Target HC</p>
                                                </div>
                                                <div class="single-chart">
                                                   <svg viewBox="0 0 36 36" class="circular-chart green">
                                                      <path class="circle-bg" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <path class="circle" stroke-dasharray="0, 100" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <text x="18" y="20.35" class="percentage">0</text>
                                                   </svg>
                                                   <p>Accepted Job Offer</p>
                                                </div>
                                                <div class="single-chart">
                                                   <svg viewBox="0 0 36 36" class="circular-chart blue">
                                                      <path class="circle-bg" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <path class="circle" stroke-dasharray="0, 100" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <text x="18" y="20.35" class="percentage">0</text>
                                                   </svg>
                                                   <p>Hired</p>
                                                </div>
                                                <div class="single-chart">
                                                   <svg viewBox="0 0 36 36" class="circular-chart blue">
                                                      <path class="circle-bg" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <path class="circle" stroke-dasharray="0, 100" d="M18 2.0845
                                                         a 15.9155 15.9155 0 0 1 0 31.831
                                                         a 15.9155 15.9155 0 0 1 0 -31.831"></path>
                                                      <text x="18" y="20.35" class="percentage">0</text>
                                                   </svg>
                                                   <p>Fall-Out</p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                  <?php
                                    }
                                  ?>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-3 filtersnavs">
                               <form method="post" action="<?php echo base_url();?>recruiter/dashboard">
                           <div class="widget">
                                    <h3 class="">Filter Jobs</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox ">
                                          <select name="filters" id="filtersid">
                                             <option value=""> Select </option>
                                             <option value="Active" selected="" <?php if(!empty($filters) && $filters=='Active'){ echo "selected"; } ?>> Active </option>
                                             <option value="Expired" <?php if(!empty($filters) && $filters=='Expired'){ echo "selected"; } ?>> Expired </option>
                                             <option value="All" <?php if(!empty($filters) && $filters=='All'){ echo "selected"; } ?>> All </option>
                                             
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="widget aplreset">
                                    <button type="submit">Apply</button>
                                    
                                 </div>
                                 </form>
                              <div class="right main-hda">
                                 <h3>Sourcing Status</h3>
                              </div>
                              <div class="graphdataanaylse">
                                 <div class="designationstats">
                                    <p>Positions Filled</p>
                                    <p class="dsncount"><?php echo $comsingle['totalhired'] .' / '; ?> <?php echo $comsingle['target']; ?></p>
                                 </div>
                                 <div class="pro">
                                  <?php 
                                      if($comsingle['target'] == 0) { 
                                        $posFil = 0;
                                      } else{
                                        $posFil = round($comsingle['totalhired'] / $comsingle['target'] * 100);
                                      }
                                  ?>
                                    <p><?php echo $posFil; ?>%</p>
                                    <div class="progress progress-sm">
                                       <div class="progress-bar progress-bar-primary" data-appear-progress-animation="<?php echo $posFil; ?>%" style="width: <?php echo $posFil; ?>%;">
                                          <span class="progress-bar-tooltip" style="opacity: 1;"></span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-- <div class="graphdataanaylse">
                                 <div class="designationstats">
                                    <p>New Applications</p>
                                    <p class="dsncount redo"><?php echo $comsingle['pendingdays']; ?></p>
                                 </div>
                                 <div class="pro">
                                    <p>5 Days</p>
                                 </div>
                                 <p>Average Time to fill</p>
                              </div> -->
                              <div class="graphdataanaylse">

                                 <div class="designationstats">
                                    <p>Fall Out</p>
                                    <p class="dsncount"><?php echo $comsingle['noshow'] + $comsingle['meetrq'] + $comsingle['noaccept']+$comsingle['another']+$comsingle['refer']; ?></p>
                                 </div><br><br>
								 <div class="lsstst">
								 <div class="pro">
                                    <p>No Show</p>
                                    <div class="progress progress-sm">
                                       <div class="progress-bar progress-bar-primary" data-appear-progress-animation="81%" style="width: <?php echo $comsingle['noshowPer']; ?>%;">
                                          <span class="progress-bar-tooltip" style="opacity: 1;"></span>
										  
                                       </div>
									   <div class="countsd"><?php echo $comsingle['noshow']; ?></div>
                                    </div>
                                 </div>
								 
								 
								 <div class="pro">
                                    <p>Did not meet requirement</p>
                                    <div class="progress progress-sm">
                                       <div class="progress-bar progress-bar-primary" data-appear-progress-animation="81%" style="width: <?php echo $comsingle['meetrqPer']; ?>%;">
                                          <span class="progress-bar-tooltip" style="opacity: 1;"></span>
                                       </div>
									   <div class="countsd"><?php echo $comsingle['meetrq']; ?></div>
                                    </div>
                                 </div>
								 
								  <div class="pro">
                                    <p>Did not accept job offer</p>
                                    <div class="progress progress-sm">
                                       <div class="progress-bar progress-bar-primary" data-appear-progress-animation="81%" style="width: <?php echo $comsingle['noacceptPer']; ?>%;">
                                          <span class="progress-bar-tooltip" style="opacity: 1;"></span>
                                       </div>
									   <div class="countsd"><?php echo $comsingle['noaccept']; ?></div>
                                    </div>
                                 </div>
								 
								 
								 <div class="pro">
                                    <p>Day 1 No Show</p>
                                    <div class="progress progress-sm">
                                       <div class="progress-bar progress-bar-primary" data-appear-progress-animation="81%" style="width: <?php echo $comsingle['anotherPer']; ?>%;">
                                          <span class="progress-bar-tooltip" style="opacity: 1;"></span>
                                       </div>
									   <div class="countsd"><?php echo $comsingle['another']; ?></div>
                                    </div>
                                 </div>
								 
								  <div class="pro">
                                    <p>Refer</p>
                                    <div class="progress progress-sm">
                                        <div class="progress-bar progress-bar-primary" data-appear-progress-animation="81%" style="width: <?php echo $comsingle['referPer']; ?>%;">
                                          <span class="progress-bar-tooltip" style="opacity: 1;"></span>
                                       </div>
									   <div class="countsd"><?php echo $comsingle['refer']; ?></div>
                                    </div>
                                 </div>
								 </div>

                              </div>


                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      </section>
    <?php include_once('footer.php'); ?>
    
    <div id="toast"><div id="img"><img src="<?php echo base_url(); ?>recruiterfiles/images/tie.png"></div><div id="desc">A notification message..</div></div>
    
    
      <?php include_once("modalpassword.php");?>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
       <?php
             if(isset($validation_errors)) {

                 /*if($this->session->tempdata('validationError')){$this->session->unset_tempdata('validationError');}*/
          ?>
                <script type="text/javascript">
                    $(window).on('load',function(){
                        $('#exampleModalCenter').modal('show');
                    });
                    if ( window.history.replaceState ) {
                        window.history.replaceState( null, null, window.location.href );
                        //window.location.href = "<?php echo base_url();?>recruiter/recruiter/index";
                    }
                </script>
          <?php
             }
         ?>
     
     <script>

     function launch_toast() {
    var x = document.getElementById("toast")
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
}

function callMe()
{
    $.ajax({
    url : '<?php echo base_url(); ?>recruiter/dashboard/pushweb',
    type : 'POST',
    success : function(data) {              
        var data=data.split("#");
        
      //alert(count(data));
        if(data[0] == 1){
var id,title,body;
if (!('Notification' in window)) {
 
document.getElementById('wn-unsupported').classList.remove('hidden');
document.getElementById('button-wn-show-preset').setAttribute('disabled', 'disabled');
 document.getElementById('button-wn-show-custom').setAttribute('disabled', 'disabled');
} else {

var log = document.getElementById('log');
var notificationEvents = ['onclick', 'onshow', 'onerror', 'onclose'];

var id = data[1];
var title =data[2];
var body = data[3];
//var icon = data[4];

//alert(title);

var title;
var options;


title = title;
 options = {
  body: body,
  //icon: icon,
  tag: 'custom'
}; 


Notification.requestPermission(function() {
var notification = new Notification(title, options);
notification.onclick = function(event) {
  event.preventDefault(); // prevent the browser from focusing the Notification's tab
  window.open('<?php echo base_url(); ?>recruiter/candidate/manageCandidates', '_blank');
}
setTimeout(notification.close.bind(notification), 6000);
//alert(title);
/*notificationEvents.forEach(function(eventName) { 
  notification[eventName] = function(event) {
    log.innerHTML = 'Event "' + event.type + '" triggered for notification "' + notification.tag + '"<br />' + log.innerHTML;
     
  };
});*/
}); 

}


        }
    }
    
    });
                
}
$('document').ready(function(){
callMe();
})

 if ( window.history.replaceState ) {
      window.history.replaceState( null, null, window.location.href );
}
     </script>

     <script type="text/javascript">
           $('document').ready(function(){
                  $('#filtersid').change(function(){
                        var option_chosen = this.value;
                  })
           })
     </script>
   </body>

