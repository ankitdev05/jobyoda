<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoda</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
   </head>
   <style>
      a:hover {
      text-decoration: none;
      }
      .tech i.fa.fa-ellipsis-h {
      float: right;
      }
      .progr span.progress-bar-tooltip {
      position: initial;
      color: #fff;
      }
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 11px;
      line-height: 0;
      padding: 10px;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .fall {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 23px 10px;
      }
      .progress-bar{
      background-color: #47b476;
      }
      .details {
      float: left;
      width: 100%;
      padding: 5px 17px;
      background-color: #47b476;
      color: #fff;
      margin: 15px;
      border-radius: 20px
      }
      .leftdetails {
      float: left;
      width: 100%;
      }
      .left {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px;
      }
      .detail span {
      display: block;
      float: right;
      margin: 12px 8px;  
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detail strong {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px 55px;
      font-weight: 700;
      font-size: 20px;
      }
      .detail, .detai {
      float: left;
      width: 100%;
      }
      .detai span {
      display: -webkit-box;
      width: 100%;
      font-weight: 600;
      margin: 0px;
      padding: 12px 0px;
      font-size: 20px;
      }
      .progras {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detai strong {
      float: right;
      font-size: 20px;
      }
      .detail p, .detai p {
      font-weight: 600;
      font-size: 16px;
      color: #000;
      }
      .detail .progress span {
      position: absolute;
      right: 0px;
      font-size: 13px;
      color: #fff;
      border: 0px solid#ddd;
      }
      .head {
      float: left;
      width: 100%;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .num {
      float: left;
      margin: 20px 0px;
      }
      .para {
      float: left;
      width: 100%;
      border: 1px solid#dddd;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .tech {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      margin: 10px 0px;
      }
      .search-container {
      float: left;
      }
      .search button {
      padding: 7px 4px;
      color: #fff;
      background-color: #47b476;
      border: none;
      width: 36%;
      }
      .upload a {
      color: #ffffff;
      text-decoration: none;
      background: #47b476;
      padding: 10px;
      border-radius: 50px;
      }
      .search input[type="text"] {
      padding: 9px 6px;
      width: 63%;
      background-color: #fff;
      border: 1px solid#ddd;
      }
      .details i.fa.fa-filter {
      float: right;
      margin: 5px 0px;
      padding: 0px;
      }
      .details p {
      float: left;
      margin: 0px;
      padding: 0px;
      color: #fff;
      }
      .head p {
      color: #fff;
      }
      .para p {
      color: #fff;
      }
      .upload {
      float: right;
      padding: 8px;
      margin: px;
      }
      border: 1px solid#ddd;
      padding: 10px;
      }
      .dot {
      float: right;
      }
      .progr {
      float: left;
      width: 100%;
      margin-bottom: 18px;
      }
      .progr .progress {
      position: absolute;
      left: 9%;
      height: 20px;
      -webkit-border-radius: 8px;
      -moz-border-radius: 8px;
      -ms-border-radius: 8px;
      -o-border-radius: 8px;
      border-radius: 8px;
      top: initial;
      width: 80%;
      }
      .progr .progress-label {
      margin: 12px 0px;
      }
      .progress-bar.progress-bar-primary {
      padding: 0px;
      }
      .texts p {
      position: absolute;
      top: 70%;
      left: 29%;
      }
      p.dropdown-toggle {
      font-size: 34px;
      margin: -30px;
      }
      .dot {
      float: right;
      }
      .texts {
      margin-top: 20px;
      }
      .text p {
      margin: 0px;
      padding: 0px;
      font-size: 13px;
      }
      .right .progress{
      width: 150px;
      height: 150px;
      line-height: 150px;
      background: none;
      margin: 0 auto;
      box-shadow: none;
      position: relative;
      }
      .right .progress:after{
      content: "";
      width: 100%;
      height: 100%;
      border-radius: 50%;
      border: 2px solid #fff;
      position: absolute;
      top: 0;
      left: 0;
      }
      .right .progress > span{
      width: 50%;
      height: 100%;
      overflow: hidden;
      position: absolute;
      top: 0;
      z-index: 1;
      }
      .progress .progress-left{
      left: 0;
      }
      .right .progress .progress-bar{
      width: 100%;
      height: 100%;
      background: none;
      border-width: 2px;
      border-style: solid;
      position: absolute;
      top: 0;
      }
      .right .progress .progress-left .progress-bar{
      left: 100%;
      border-top-right-radius: 80px;
      border-bottom-right-radius: 80px; 
      border-left: 0;
      -webkit-transform-origin: center left;
      transform-origin: center left;
      }
      .right .progress .progress-right{
      right: 0;
      }
      .right .progress .progress-right .progress-bar{
      left: -100%;
      border-top-left-radius: 80px;
      border-bottom-left-radius: 80px;
      border-right: 0;
      -webkit-transform-origin: center right;
      transform-origin: center right;
      animation: loading-1 1.8s linear forwards;
      }
      .right .progress .progress-value{
      width: 59%;
      height: 59%;
      border-radius: 50%;
      border: 2px solid #ebebeb;
      font-size: 28px;
      line-height: 82px;
      text-align: center;
      position: absolute;
      top: 7.5%;
      left: 7.5%;
      }
      .right .progress.blue .progress-bar{
      border-color: #049dff;
      }
      .right .progress.blue .progress-value{
      color: #47b476;
      }
      .right .progress.blue .progress-left .progress-bar{
      animation: loading-2 1.5s linear forwards 1.8s;
      }
      .progress.yellow .progress-bar{
      border-color: #fdba04;
      }
      .right .progress.yellow .progress-value{
      color: #fdba04;
      }
      .right .progress.yellow .progress-left .progress-bar{
      animation: loading-3 1s linear forwards 1.8s;
      }
      .right .progress.pink .progress-bar{
      border-color: #ed687c;
      }
      .right .progress.pink .progress-value{
      color: #ed687c;
      }
      .right .progress.pink .progress-left .progress-bar{
      animation: loading-4 0.4s linear forwards 1.8s;
      }
      .right .progress.green .progress-bar{
      border-color: #1abc9c;
      }
      .right .progress.green .progress-value{
      color: #1abc9c;
      }
      .right .progress.green .progress-left .progress-bar{
      animation: loading-5 1.2s linear forwards 1.8s;
      }
      @keyframes loading-1{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
      }
      }
      @keyframes loading-2{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(144deg);
      transform: rotate(144deg);
      }
      }
      @keyframes loading-3{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(90deg);
      transform: rotate(90deg);
      }
      }
      @keyframes loading-4{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(36deg);
      transform: rotate(36deg);
      }
      }
      @keyframes loading-5{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(126deg);
      transform: rotate(126deg);
      }
      }
      @media only screen and (max-width: 990px){
      .right .progress{ margin-bottom: 20px; }
      }
      .validError{color:#f00;}
      .insertMsg {
          text-align: center;
          color: #27aa60;
          font-size: 18px;
      }
      .form-control.locselect{padding:0px!important;}
      input[type="checkbox"], input[type="radio"] {
          z-index:0 !important;
      }
      
      
        
        .rightssoly {
            border: 1px solid #ddd;
            float: left;
            width: 100%;
            min-height: 173px;
            margin-bottom: 20px;
        }
                
                .rightssoly button {
            color: red;
            padding:10px;
        }

        a.remove i.fa.fa-trash {
color: #fff;
}
input.form-control {
    margin: 20px 0px;
}
a.remove{
padding: 10px 8px;
text-align:center;;
background: #26ae61;

border-radius: 100px;

width: 6%;
    margin: 10px 0px;
float: left;
 }
input.form-control.addinput {
    width: 80%!important;
    float: left;
    margin-right: 33px!important;
}
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  <div class="logo">
                     <a href="<?php echo base_url();?>recruiter/dashboard" title=""><img src="<?php echo base_url().'recruiterfiles/';?>images/jobyoda.png"></a>
                  </div>
                  <!-- Logo -->
                        <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="companyprofileforms fullwidthform updatemainfd">
                              <?php if($this->session->tempdata('inserted')) {?>
                                 <p class="insertMsg"><?php echo $this->session->tempdata('inserted'); ?></p>
                              <?php } ?>
                              <?php if($this->session->tempdata('postError')) {?>
                                 <p class="errorMsg"><?php echo $this->session->tempdata('postError'); ?></p>
                              <?php } ?>
                                 <form method="post" action="<?php echo base_url();?>recruiter/jobpost/jobpostInsert" enctype="multipart/form-data">
                                    <h6>Post Job</h6>
                                    <div class="filldetails">
                                        
                                        <div class="dividehalfd">
                                            <div class="row">
									<div class="col-md-6">
                                       <select  name="jobTitle" id="jobTitle">
                                            <option> Select Job Title </option>
                                        <?php
                                            if($jobTitle) {
                                                foreach($jobTitle as $job_Title) {
                                        ?>
                                                    <option value="<?php echo $job_Title['jobtitle']; ?>" <?php if($getJobs[0]['jobtitle'] == $job_Title['jobtitle']){ echo "selected";}?> > <?php echo $job_Title['jobtitle']; ?> </option>
                                        <?php
                                                }     
                                            }
                                        ?>
                                       </select>
                                       <!--<input type="text" class="form-control" name="jobTitle" placeholder="Job Title" value="<?php echo $getJobs[0]['jobtitle'];?>">-->
                                       <?php if(!empty($errors['jobTitle'])){echo "<span class='validError'>".$errors['jobTitle']."</span>";}?>
                                      </div>
                                      	<div class="col-md-6">
                                       <select  name="jobLoc" id="jobLoc">
                                            <option> Select Location </option>
                                        <?php
                                            if($addresses) {
                                                foreach($addresses as $address) {
                                        ?>
                                                    <option value="<?php echo $address['recruiter_id']; ?>" <?php if($getJobs[0]['company_id'] == $address['recruiter_id']){ echo "selected";}?> > <?php echo $address['address']; ?> </option>
                                        <?php
                                                }     
                                            }
                                        ?>
                                       </select>
                                       </div>
                                       
                                      </div>
                                      </div>
                                       <?php if(!empty($errors['jobLoc'])){echo "<p class='validError'>".$errors['jobLoc']."</p>";}?>
                                        <div class="dividehalfd">
									<div class="row">
									<div class="col-md-6">
                                       <input type="text" class="form-control" name="opening" placeholder="No. of Openings" value="<?php if(!empty($getJobs[0]['opening'])){ echo $getJobs[0]['opening']; }?>">
                                       <?php if(!empty($errors['opening'])){echo "<span class='validError'>".$errors['opening']."</span>";}?>
                                    </div>
                                    <div class="col-md-6">
                                       <input type="text" class="form-control" name="experience" placeholder="Experience Required" value="<?php if(!empty($getJobs[0]['experience'])){ echo $getJobs[0]['experience'];}?>">
                                       <?php if(!empty($errors['experience'])){echo "<span class='validError'>".$errors['experience']."</span>";}?>
                                    </div>
                                    </div>
                                    </div>
                                      <table id="myTable">
                                          <?php
                                            if($getJobexps) {
                                              foreach($getJobexps as $getJobexp) {
                                          ?>
                                                <tr>
                                                   <td>
                                                      <select name="expRange[]">
                                                         <option value="">Select Experience</option>
                                                    <?php
                                                        foreach($getExps as $getExp) {
                                                    ?>
                                                         <option value="<?php echo $getExp['exp']; ?>" <?php if($getJobexp['exp'] == $getExp['exp']){ echo "selected";} ?>> <?php echo $getExp['exp']; ?> </option>
                                                    <?php
                                                      }
                                                    ?>
                                                      </select>
                                                   </td>
                                                   <td><input type="text" name="expBasicSalary[]" class="form-control" value="<?php echo $getJobexp['basicsalary'];?>" placeholder="Salary per Month"/></td>
                                                </tr>
                                          <?php
                                              }
                                            } 
                                          ?>
                                       </table>

                                       <p onclick="myFunction()" class="addmre">Add More</p>
                                       <div class="dividehalfd">
    									<div class="row">
    									<div class="col-md-4">
                                       <select name="industry">
                                          <?php
                                               foreach($industryLists as $industryList) {
                                          ?>
                                                <option value="<?php echo $industryList['id'];?>" <?php if(!empty($getJobs[0]['industry'])){ if($getJobs[0]['industry'] == $industryList['id']) {echo "selected";}} ?>><?php echo $industryList['name'];?></option>
                                          <?php }?>
                                       </select>
                                        </div>
                                        <div class="col-md-4">
                                       <select name="channel">
                                          <option value=""> Select Channel </option>
                                          <?php
                                               foreach($channels as $channel) {
                                          ?>
                                                <option value="<?php echo $channel['id'];?>" <?php if(!empty($getJobs[0]['channel'])){ if($getJobs[0]['channel'] == $channel['id']) {echo "selected";}} ?>><?php echo $channel['name'];?></option>
                                          <?php }?>
                                       </select>
                                        </div>
                                    	<div class="col-md-4">
                                       <select name="lang">
                                          <option value=""> Select Language </option>
                                          <?php
                                                //echo $getJobs[0]['language'];die;
                                               foreach($langs as $lang) {
                                          ?>
                                                <option value="<?php echo $lang['id'];?>" <?php if(!empty($getJobs[0]['language'])){ if($getJobs[0]['language'] == $lang['id']) {echo "selected";}} ?>><?php echo $lang['name'];?></option>
                                          <?php }?>
                                       </select>
                                      </div>
                                       </div>
									   </div>
                                       <textarea class="form-control" name="jobDesc" placeholder="job Description" maxlength="200"><?php echo $getJobs[0]['jobDesc'];?></textarea>
                                       <?php if(!empty($errors['jobDesc'])){echo "<span class='validError'>".$errors['jobDesc']."</span>";}?>
                                        <div class="dividehalfd">
									<div class="row">
									
									<div class="col-md-4">
                                       <input type="text" class="form-control" name="skills" placeholder="Skills Required" value="<?php if(!empty($getJobs[0]['skills'])){ echo $getJobs[0]['skills'];}?>">
                                       <?php if(!empty($errors['skills'])){echo "<span class='validError'>".$errors['skills']."</span>";}?>
                                    </div>
                                    <div class="col-md-4">
                                       <input type="text" class="form-control" name="qualification" placeholder="Qualification Required" value="<?php if(!empty($getJobs[0]['qualification'])){ echo $getJobs[0]['qualification'];}?>">
                                       <?php if(!empty($errors['qualification'])){echo "<span class='validError'>".$errors['qualification']."</span>";}?>
                                    </div>
                                    <div class="col-md-4">
                                       <input placeholder="Job Expiry Date" class="form-control" value="<?php if(!empty($getJobs[0]['jobexpire'])){ echo $getJobs[0]['jobexpire'];}?>" name="jobExpire" type="text" onfocus="(this.type='date')"  id="date"> 
                                       <?php if(!empty($errors['jobExpire'])){echo "<span class='validError'>".$errors['jobExpire']."</span>";}?>
                                    </div>    
                                        <!--<div style="clear:both;"></div>-->
                                          <div class="col-md-12">
                                                <h3 style="font-size: 20px;  width: 86%;
    font-weight: 600;
    color: #000;">Site Images</h3>
                                          </div>      
                                          <div class="col-md-12">
                                          
                                          
                                          <?php if($jobImg!=''){foreach($jobImg as $siteimg){ 
                                                $siteimage = $siteimg['pic'] ;
                                          ?>
                                                <div class="col-md-2">
                                                      <div class="rightssoly">
                                                            <input type="hidden" value="<?php echo $siteimg['id']; ?>">
                                                            <button type="button" onClick="deleteImg('<?= $siteimg['id'] ?>')" style="border:0px;background: none;float:right;"> <i class="fa fa-trash"></i> </button>
                                                             <img src="<?php echo $siteimage;?>" style="width:100%;">
                                                      </div>
                                                     
                                                </div>
                                          <?php }}?>
                                          <?php if(count($jobImg)<3){ ?>
                                              <table id="myTable11">
                                              <tr>
                                                 
                                                 <td><input type="file" name="job_image[]" class="form-control"></td>
                                              </tr>
                                            </table>
                                            <p onClick="myFunction11()" class="addmre">Add More</p> 
                                            <?php  }?>
                                                    
                                          
                                          </div>
                                       <input type="hidden" name="jobId" value="<?php echo $getJobs[0]['id'];?>">
                                       <?php $userSession = $this->session->userdata('userSession'); ?>
                                       <input type="hidden" name="recruId" value="<?php echo $userSession['id']; ?>">
                                       <div class="newfiletrsgree">
                                  
                                 <div class="main-hda right">
                                     <!--<span>Please select all site-specific benefits</span>-->

                                    <h3>Top Picks</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li id="topicks1" class="<?php if($topPicks2){if(in_array(1, $topPicks2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="toppic1" name="toppicks[]" value="1" <?php if($topPicks2){if(in_array(1, $topPicks2)){ echo "checked"; }}?> >
                                          <i class="fas fa-mobile-alt"></i>
                                          <p> Joining<br>Bonus</p>
                                       </li>
                                       <li id="topicks2" class="<?php if($topPicks2){if(in_array(2, $topPicks2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="toppic2" name="toppicks[]" value="2" <?php if($topPicks2){if(in_array(2, $topPicks2)){ echo "checked"; }}?> >
                                          <i class="fas fa-utensils"></i>  
                                          <p> Free <br>Food</p>
                                       </li>
                                       <li id="topicks3" class="<?php if($topPicks2){if(in_array(3, $topPicks2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="toppic3" name="toppicks[]" value="3" <?php if($topPicks2){if(in_array(3, $topPicks2)){ echo "checked"; }}?>>
                                          <i class="fas fa-heartbeat"></i>
                                          <p>Day 1 HMO</p>
                                       </li>
                                       <li id="topicks4" class="<?php if($topPicks2){if(in_array(4, $topPicks2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="toppic4" name="toppicks[]" value="4" <?php if($topPicks2){if(in_array(4, $topPicks2)){ echo "checked"; }}?> >
                                          <i class="fas fa-heartbeat"></i>
                                          <p> Day 1 HMO<br> for Dependent</p>
                                       </li>
                                       <li id="topicks5" class="<?php if($topPicks2){if(in_array(5, $topPicks2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="toppic5" name="toppicks[]" value="5" <?php if($topPicks2){if(in_array(5, $topPicks2)){ echo "checked"; }}?>>
                                          <i class="fas fa-sun"></i>
                                          <p>Day Shift</p>
                                       </li>
                                       <li id="topicks6" class="<?php if($topPicks2){if(in_array(6, $topPicks2)){ echo 'selectedgreen'; }} ?>">
                                          <input type="checkbox" id="toppic6" name="toppicks[]" value="6" <?php if($topPicks2){if(in_array(6, $topPicks2)){ echo "checked"; }}?>>
                                          <i class="fas fa-money"></i> 
                                          <p> 14th Month Pay</p>
                                       </li>
                                    </ul>
                                 </div>
                                 
                              </div>
                              <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Allowances and Incentives</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li id="allowance1" class="<?php if($allowance2){if(in_array(1, $allowance2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="allowanc1" name="allowances[]" value="1" <?php if($allowance2){if(in_array(1, $allowance2)){ echo "checked"; }}?> >
                                          <i class="fas fa-mobile-alt"></i> 
                                          <p> Cell <br>Allowances</p>
                                       </li>
                                       <li id="allowance2" class="<?php if($allowance2){if(in_array(2, $allowance2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="allowanc2" name="allowances[]" value="2" <?php if($allowance2){if(in_array(2, $allowance2)){ echo "checked"; }}?>>
                                          <i class="fas fa-car"></i> 
                                          <p>Free <br>Parking</p>
                                       </li>
                                       <li id="allowance3" class="<?php if($allowance2){if(in_array(3, $allowance2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="allowanc3" name="allowances[]" value="3" <?php if($allowance2){if(in_array(3, $allowance2)){ echo "checked"; }}?>>
                                          <i class="fas fa-bus"></i> 
                                          <p> Free <br> Shuttle</p>
                                       </li>
                                       <li id="allowance4" class="<?php if($allowance2){if(in_array(4, $allowance2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="allowanc4" name="allowances[]" value="4" <?php if($allowance2){if(in_array(4, $allowance2)){ echo "checked"; }}?>>
                                          <i class="fas fa-chart-bar"></i> 
                                          <p> Annual <br> Performance Bonus</p>
                                       </li>
                                       <li id="allowance5" class="<?php if($allowance2){if(in_array(5, $allowance2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="allowanc5" name="allowances[]" value="5" <?php if($allowance2){if(in_array(5, $allowance2)){ echo "checked"; }}?>>
                                          <i class="fas fa-line-chart"></i> 
                                          <p> Retirements <br> Benifits</p>
                                       </li>
                                       <li id="allowance6" class="<?php if($allowance2){if(in_array(6, $allowance2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="allowanc6" name="allowances[]" value="6" <?php if($allowance2){if(in_array(6, $allowance2)){ echo "checked"; }}?>>
                                          <i class="fas fa-shuttle-van flip"></i> 
                                          <p> Transporter <br> Allowance</p>
                                       </li>
                                       <li id="allowance7" class="<?php if($allowance2){if(in_array(7, $allowance2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="allowanc7" name="allowances[]" value="7" <?php if($allowance2){if(in_array(7, $allowance2)){ echo "checked"; }}?>>
                                          <i class="fas fa-money"></i> 
                                          <p> Monthly Performance <br> Incentives</p>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Medical Benifits</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li id="medical1" class="<?php if($medical2){if(in_array(1, $medical2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="medi1" name="medical[]" value="1" <?php if($medical2){if(in_array(1, $medical2)){ echo "selectedgreen"; }}?>>
                                          <i class="fas fa-heartbeat"></i> 
                                          <p> Free HMO for<br>Dependents</p>
                                       </li>
                                       <li id="medical2" class="<?php if($medical2){if(in_array(2, $medical2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="medi2" name="medical[]" value="2" <?php if($medical2){if(in_array(2, $medical2)){ echo "selectedgreen"; }}?>>
                                          <i class="fas fa-medkit"></i>  
                                          <p> Critical Illness <br>Benefits</p>
                                       </li>
                                       <li id="medical3" class="<?php if($medical2){if(in_array(3, $medical2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="medi3" name="medical[]" value="3" <?php if($medical2){if(in_array(3, $medical2)){ echo "selectedgreen"; }}?>>
                                          <i class="fas fa-heart"></i>
                                          <p>Life <br>Insurance</p>
                                       </li>
                                       <li id="medical4" class="<?php if($medical2){if(in_array(4, $medical2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="medi4" name="medical[]" value="4" <?php if($medical2){if(in_array(4, $medical2)){ echo "selectedgreen"; }}?>>
                                          <i class="fas fa-plus-square"></i>
                                          <p> Maternity<br> Assistance</p>
                                       </li>
                                       <li id="medical5" class="<?php if($medical2){if(in_array(5, $medical2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="medi5" name="medical[]" value="5" <?php if($medical2){if(in_array(5, $medical2)){ echo "selectedgreen"; }}?>>
                                          <i class="fas fa-stethoscope"></i>
                                          <p>Medicine <br>Reimbursement</p>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Leaves</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li id="leaves1" class="<?php if($leaves2){if(in_array(1, $leaves2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="leave1"  name="leavs[]" value="1" <?php if($leaves2){if(in_array(1, $leaves2)){ echo "selectedgreen"; }}?>>
                                          <i class="fas fa-snowflake"></i>
                                          <p> Weekend Off</p>
                                       </li>
                                       <li id="leaves2" class="<?php if($leaves2){if(in_array(2, $leaves2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" id="leave2"  name="leavs[]" value="2" <?php if($leaves2){if(in_array(2, $leaves2)){ echo "selectedgreen"; }}?>>
                                          <i class="fas fa-snowflake"></i>
                                          <p>Holiday Off</p>
                                       </li>
                                    </ul>
                                 </div>
                                 
                              </div>
                              <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Work Shifts</h3>
                                 </div>
                                 <div class="filterchekers1">
                                    <ul>
                                       <li id="workshift1" class="work-class <?php if(!empty($workshift2)){ if($workshift2[0]=="1"){ echo 'selectedgreen';}} ?>">
                                          <input type="radio" id="works1" class="work-radio" name="shifts[]" value="1" >
                                          <i class="fas fa-star"></i> 
                                          <p> Mid Shift </p>
                                       </li>
                                       <li id="workshift2" class="work-class <?php if(!empty($workshift2)){ if($workshift2[0]=="2"){ echo 'selectedgreen';}} ?>">
                                          <input type="radio" id="works2" class="work-radio" name="shifts[]" value="2" >
                                          <i class="fas fa-moon-o"></i>
                                          <p> Night Shift </p>
                                       </li>
                                       <li id="workshift3" class="work-class <?php if(!empty($workshift2)){ if($workshift2[0]=="3"){ echo 'selectedgreen';}} ?>">
                                          <input type="radio" id="works3" class="work-radio" name="shifts[]" value="3" >
                                          <p class="txtonly">24/7</p>
                                          <p>24/7 </p>
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              
                                       <button type="submit">Update Job</button>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      </section>
      <?php include_once("modalpassword.php");?>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <script>
         function myFunction() {
           var table = document.getElementById("myTable");
           var rows = document.getElementById("myTable").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
           
           
           var row = table.insertRow(rows);
           var cell1 = row.insertCell(0);
           var cell2 = row.insertCell(1);
      <?php
        if($getExps) {
           $exp ="<select name='expRange[]'><option> Select Experience</option>";
           foreach($getExps as $getExp) {
                  $exp .= "<option value='".$getExp['exp']."'>".$getExp['exp']."</option>";
           }
           $exp .= "</select>";
        }

      ?>
           cell1.innerHTML = "<?php echo $exp;?>";
           cell2.innerHTML = "<input type='text' name='expBasicSalary[]' class='form-control' placeholder='Salary per Month'/>";
         }
         
         
      </script>
      <script>
        $(document).ready(function(){
            
        
          $('#jobLoc').change(function() {
             //alert('hi');
        var recruiter_id = $('#jobLoc').val();
        //alert(recruiter_id);
        $.ajax({
        type:"POST",
        url : "<?php echo base_url(); ?>/recruiter/recruiter/recruiter_location",
        data : {recruiter_id:recruiter_id},
        success : function(response1) {
            var response2 = JSON.parse(response1);
            
            if(response2['topPicks2'].length >= 1) {
                var topicks = [1,2,3,4,5,6];
                for($i=0; $i<response2['topPicks2'].length; $i++) {
                    var abc = topicks.indexOf(response2['topPicks2'][$i]);
                    if(abc) {
                        var customclass = "#topicks" + response2['topPicks2'][$i];
                        $(customclass).addClass('selectedgreen');
                        var cusid = "#toppic" + response2['topPicks2'][$i];
                            $(cusid).attr("checked", "checked");
                    }
                }
            }
            
            if(response2['allowance2'].length >= 1) {
                var allowance = [1,2,3,4,5,6,7];
                for($i=0; $i<response2['allowance2'].length; $i++) {
                    var abc = allowance.indexOf(response2['allowance2'][$i]);
                    if(abc) {
                        var customclass = "#allowance" + response2['allowance2'][$i];
                        $(customclass).addClass('selectedgreen');
                        var cusid = "#allowanc" + response2['allowance2'][$i];
                             $(cusid).attr("checked", "checked");
                    }
                }
            }
            
            if(response2['medical2'].length >= 1) {
                var medical = [1,2,3,4,5];
                for($i=0; $i<response2['medical2'].length; $i++) {
                    var abc = medical.indexOf(response2['medical2'][$i]);
                    if(abc) {
                        var customclass = "#medical" + response2['medical2'][$i];
                        $(customclass).addClass('selectedgreen');
                        var cusid = "#medi" + response2['medical2'][$i];
                            $(cusid).attr("checked", "checked");
                    }
                }
            }
            
            if(response2['leave2'].length >= 1) {
              var leave = [1,2];
              for($i=0; $i<response2['leave2'].length; $i++) {
                  var abc = leave.indexOf(response2['leave2'][$i]);
                  if(abc) {
                      var customclass = "#leaves" + response2['leave2'][$i];
                      $(customclass).addClass('selectedgreen');
                      var cusid = "#leave" + response2['leave2'][$i];
                      $(cusid).attr("checked", "checked");
                  }
              }
            }

            if(response2['workshift2'].length >= 1) {
                var workshift = [1,2,3];
                for($i=0; $i<response2['workshift2'].length; $i++) {
                    var abc = workshift.indexOf(response2['workshift2'][$i]);
                    if(abc) {
                        var customclass = "#workshift" + response2['workshift2'][$i];
                        $(customclass).addClass('selectedgreen');
                        var cusid = "#works" + response2['workshift2'][$i];
                        $(cusid).attr("checked", "checked");
                    }
                }
            }
            
        },
        error: function() {
            alert('Error occured');
        }
    });
    });
    });
      </script>
      <script>
        $(".filterchekers li").click(function(){
            $(this).toggleClass("selectedgreen");  
      });
        function clickfuncheck(id) {
            var cid =  "#"+id;
            if (document.getElementById(id).checked) {
                $(cid).prop('checked', true);
            } else {
                $(cid).prop('checked', false);
            }
        }
        
        
        $('.work-radio').change(function() {
            $(".work-class").removeClass("selectedgreen");
            if ($(this).is(':checked')){
                $(this).closest("li").addClass("selectedgreen");
              }
              else
                $(this).closest("li").removeClass("selectedgreen");
        });
        
        function deleteImg(id){
        $.ajax({
           'type':'POST',
           'data':'id='+id,
           'url' :"<?= base_url('recruiter/Jobpost/deleteimg')?>",
            'success':function(res){
                setTimeout(function(){// wait for 5 secs(2)
                       location.reload(); // then reload the page.(3)
                  }, 1000);
                //console.log(res);
            }           
           
        });
    }
   </script>
   <script>
     function myFunction11() {
       var count = '<?php echo count($jobImg); ?>';     
       var table = document.getElementById("myTable11");
       var rows = document.getElementById("myTable11").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
       
        if(rows<(3-count))
        {
           var row = table.insertRow(rows);
           var cell1 = row.insertCell(0);
              
           cell1.innerHTML = "<input type='file' name='job_image[]' class='form-control addinput' ><a href='javascript:void(0);' class='remove'><i class='fa fa-trash'></i></a> ";
        }
        else{
            $('.addmre').hide();
        }
       
       
     }
     $(document).on("click", "a.remove" , function() {
            $(this).parent().remove();
        });
  </script>
      <style>
         table#myTable {
         width: 100%;
         }
         table#myTable td {
         padding: 0 9px 0 0;
         }
         .addmre {
         margin: 0;
         background: #27aa60;
         float: left;
         color: #fff;
         padding: 3px 8px;
         margin-bottom: 20px;
         cursor: pointer;
         float: right;
         }
         table#myTable td:last-child {
         padding: 0;
         }
         
         table#myTable11 {
         width: 100%;
         }
         table#myTable11 td {
         padding: 0 9px 0 0;
         }
         
         table#myTable11 td:last-child {
         padding: 0;
         }
      </style>
   </body>
</html>

