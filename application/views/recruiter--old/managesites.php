<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/croppie.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
   </head>
   <style> 

      span.err{
            color: red;
      }
      a:hover {
      text-decoration: none;
      }
      .tech i.fa.fa-ellipsis-h {
      float: right;
      }
      .progr span.progress-bar-tooltip {
      position: initial;
      color: #fff;
      }
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 11px;
      line-height: 0;
      padding: 10px;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .fall {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 23px 10px;
      }
      .progress-bar{
      background-color: #47b476;
      }
      .details {
      float: left;
      width: 100%;
      padding: 5px 17px;
      background-color: #47b476;
      color: #fff;
      margin: 15px;
      border-radius: 20px
      }
      .leftdetails {
      float: left;
      width: 100%;
      }
      .left {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px;
      }
      .detail span {
      display: block;
      float: right;
      margin: 12px 8px;  
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detail strong {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px 55px;
      font-weight: 700;
      font-size: 20px;
      }
      .detail, .detai {
      float: left;
      width: 100%;
      }
      .detai span {
      display: -webkit-box;
      width: 100%;
      font-weight: 600;
      margin: 0px;
      padding: 12px 0px;
      font-size: 20px;
      }
      .progras {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detai strong {
      float: right;
      font-size: 20px;
      }
      .detail p, .detai p {
      font-weight: 600;
      font-size: 16px;
      color: #000;
      }
      .detail .progress span {
      position: absolute;
      right: 0px;
      font-size: 13px;
      color: #fff;
      border: 0px solid#ddd;
      }
      .head {
      float: left;
      width: 100%;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .num {
      float: left;
      margin: 20px 0px;
      }
      .para {
      float: left;
      width: 100%;
      border: 1px solid#dddd;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .tech {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      margin: 10px 0px;
      }
      .search-container {
      float: left;
      }
      .search button {
      padding: 7px 4px;
      color: #fff;
      background-color: #47b476;
      border: none;
      width: 36%;
      }
      .upload a {
      color: #ffffff;
      text-decoration: none;
      background: #47b476;
      padding: 10px;
      border-radius: 50px;
      }
      .search input[type="text"] {
      padding: 9px 6px;
      width: 63%;
      background-color: #fff;
      border: 1px solid#ddd;
      }
      .details i.fa.fa-filter {
      float: right;
      margin: 5px 0px;
      padding: 0px;
      }
      .details p {
      float: left;
      margin: 0px;
      padding: 0px;
      color: #fff;
      }
      .head p {
      color: #fff;
      }
      .para p {
      color: #fff;
      }
      .upload {
      float: right;
      padding: 8px;
      margin: px;
      }
      border: 1px solid#ddd;
      padding: 10px;
      }
      .dot {
      float: right;
      }
      .progr {
      float: left;
      width: 100%;
      margin-bottom: 18px;
      }
      .progr .progress {
      position: absolute;
      left: 9%;
      height: 20px;
      -webkit-border-radius: 8px;
      -moz-border-radius: 8px;
      -ms-border-radius: 8px;
      -o-border-radius: 8px;
      border-radius: 8px;
      top: initial;
      width: 80%;
      }
      .progr .progress-label {
      margin: 12px 0px;
      }
      .progress-bar.progress-bar-primary {
      padding: 0px;
      }
      .texts p {
      position: absolute;
      top: 70%;
      left: 29%;
      }
      p.dropdown-toggle {
      font-size: 34px;
      margin: -30px;
      }
      .dot {
      float: right;
      }
      .texts {
      margin-top: 20px;
      }
      .text p {
      margin: 0px;
      padding: 0px;
      font-size: 13px;
      }
      .right .progress{
      width: 150px;
      height: 150px;
      line-height: 150px;
      background: none;
      margin: 0 auto;
      box-shadow: none;
      position: relative;
      }
      .right .progress:after{
      content: "";
      width: 100%;
      height: 100%;
      border-radius: 50%;
      border: 2px solid #fff;
      position: absolute;
      top: 0;
      left: 0;
      }
      .right .progress > span{
      width: 50%;
      height: 100%;
      overflow: hidden;
      position: absolute;
      top: 0;
      z-index: 1;
      }
      .progress .progress-left{
      left: 0;
      }
      .right .progress .progress-bar{
      width: 100%;
      height: 100%;
      background: none;
      border-width: 2px;
      border-style: solid;
      position: absolute;
      top: 0;
      }
      .right .progress .progress-left .progress-bar{
      left: 100%;
      border-top-right-radius: 80px;
      border-bottom-right-radius: 80px; 
      border-left: 0;
      -webkit-transform-origin: center left;
      transform-origin: center left;
      }
      .right .progress .progress-right{
      right: 0;
      }
      .right .progress .progress-right .progress-bar{
      left: -100%;
      border-top-left-radius: 80px;
      border-bottom-left-radius: 80px;
      border-right: 0;
      -webkit-transform-origin: center right;
      transform-origin: center right;
      animation: loading-1 1.8s linear forwards;
      }
      .right .progress .progress-value{
      width: 59%;
      height: 59%;
      border-radius: 50%;
      border: 2px solid #ebebeb;
      font-size: 28px;
      line-height: 82px;
      text-align: center;
      position: absolute;
      top: 7.5%;
      left: 7.5%;
      }
      .right .progress.blue .progress-bar{
      border-color: #049dff;
      }
      .right .progress.blue .progress-value{
      color: #47b476;
      }
      .right .progress.blue .progress-left .progress-bar{
      animation: loading-2 1.5s linear forwards 1.8s;
      }
      .progress.yellow .progress-bar{
      border-color: #fdba04;
      }
      .right .progress.yellow .progress-value{
      color: #fdba04;
      }
      .right .progress.yellow .progress-left .progress-bar{
      animation: loading-3 1s linear forwards 1.8s;
      }
      .right .progress.pink .progress-bar{
      border-color: #ed687c;
      }
      .right .progress.pink .progress-value{
      color: #ed687c;
      }
      .right .progress.pink .progress-left .progress-bar{
      animation: loading-4 0.4s linear forwards 1.8s;
      }
      .right .progress.green .progress-bar{
      border-color: #1abc9c;
      }
      .right .progress.green .progress-value{
      color: #1abc9c;
      }
      .right .progress.green .progress-left .progress-bar{
      animation: loading-5 1.2s linear forwards 1.8s;
      }
      @keyframes loading-1{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
      }
      }
      @keyframes loading-2{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(144deg);
      transform: rotate(144deg);
      }
      }
      @keyframes loading-3{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(90deg);
      transform: rotate(90deg);
      }
      }
      @keyframes loading-4{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(36deg);
      transform: rotate(36deg);
      }
      }
      @keyframes loading-5{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(126deg);
      transform: rotate(126deg);
      }
      }
      @media only screen and (max-width: 990px){
      .right .progress{ margin-bottom: 20px; }
      }
      .filterchekers .checkedColor {background: #27aa60;color: #fff;cursor: pointer;}
     
      .checked_value{
          width: 100px;
           height: 52px;
      }
      input[type="checkbox"], input[type="radio"] {
          z-index: 0!important;
      }
	  
	  .psthbs .companyprofileforms.fullwidthform .filldetails{
		      width: 100%;
	  }
	  .filldetails.flxdtls {
    display: flex;
}

.filldetails.flxdtls .stepcountfrms {
    margin: 0 14px;
}
     button.updts {
    float: none;
    margin: 0 auto;
    width: 12%;
} 
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <!-- Logo -->
                    <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10">
                        <div class="row">
                           <div class="col-md-12 psthbs"> 
                              <div class="companyprofileforms fullwidthform managecompanybch mgsirt">
							   <div class="myhdainside">
                                      <h6 style="text-align: center;">Manage Company Profile</h6>
                                    </div>
                                 <form method="post" action="<?php echo base_url();?>recruiter/recruiter/companyupdate" enctype="multipart/form-data">
                                    <div class="mailayer">
									<div class="camico">
									<i class="fas fa-camera"></i>
                                       
									   </div>
								<div class="uploadareas">
									
									   <input type="file" name="profilePic" id="imgInp" accept="image/x-png,image/jpg,image/jpeg">
									   <div class="myimgprofd" style="width:100%;">
                                      <!--  <input type="hidden" name="cropimg" id="cropimg" value=""> -->
                                       <?php if(@getimagesize($companyDetails[0]['companyPic'])){ ?>
                                       
                                       <img id="blah" src="<?php echo $companyDetails[0]['companyPic'] ?>">
                                       <?php } else{ ?>
                                       <img id="blah" src="<?php echo base_url();?>recruiterfiles/images/camera1.png">
                                       <?php }?>
                                      </div>
									  </div>
                                    </div>
									
                                    <span class="imgmsg">*Image dimension should be within 1376X588 & size upto 5 MB.</span>                                       
                                    <div class="filldetails flxdtls">
									<div class="stepcountfrms">
									  <div class="headsteps-gt">
                                             <h5>Company Information</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
										  
										<input type="text" class="form-control" name="cname" placeholder="Company Name" value="<?php if(!empty($companie[0]['cname'])){echo $companie[0]['cname'];} ?>">
										
										<input type="text" id="txtPlaces" class="form-control" name="address" placeholder="Address" value="<?php if(!empty($companyDetails[0]['address'])){echo $companyDetails[0]['address'];} ?>">
									
									   </div>
									   
									   
									   <div class="stepcountfrms">
									  <div class="headsteps-gt">
                                             <h5>Company Overview</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
										  
										 <textarea name="compDesc" placeholder="Company Description" maxlength="1000"><?php if(isset($companyDetails[0]['companyDesc'])){echo $companyDetails[0]['companyDesc'];} ?></textarea>

									   </div>
									   
									   </div> 
                                      <div class="filldetails flxdtls"> 									   
									    <div class="stepcountfrms">
									  <div class="headsteps-gt">
                                             <h5>Recruitment Leader Information</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
										  
										 <input type="hidden" name="rid" value="<?php if(!empty($companie[0]['id'])){ echo $companie[0]['id']; } ?>">    
                                       <input type="text" class="form-control" name="fname" placeholder="First Name" value="<?php if(!empty($companie[0]['fname'])){echo $companie[0]['fname'];} ?>">
                                       
                                       <input type="text" class="form-control" name="lname" placeholder="Last Name" value="<?php if(!empty($companie[0]['lname'])){echo $companie[0]['lname'];} ?>"> 
										 
                                       <input type="text" class="form-control" name="email" placeholder="Email id" value="<?php if(!empty($companie[0]['email'])){echo $companie[0]['email'];} ?>">
                                       <span class="err"><?php if(!empty($errors['email'])){ echo $errors['email']; } ?></span>
                                       <div class="divhalfu">
                                       <select name="industry">
                                          <option value=""> Select Industry </option>
                                          <?php
                                               foreach($industryLists as $industryList) {
                                          ?>
                                                <option <?php if($industryList['id']==$companyDetails[0]['industry']){ echo "selected"; }?> value="<?php echo $industryList['id'];?>"><?php echo $industryList['name'];?></option>
                                          <?php }?>
                                       </select>
                                       
                                       <select name="phonecode" class="form-control">
                                          <option value=""> Select Phone Code</option>
                                        <?php
                                          foreach($phonecodes as $phonecode) {
                                        ?>
                                            <option <?php if($phonecode['phonecode']==$companyDetails[0]['phonecode']){ echo "selected"; }?> value="<?php echo $phonecode['phonecode'];?>"> <?php echo $phonecode['name'];?> - <?php echo $phonecode['phonecode'];?> </option>
                                        <?php
                                          }
                                        ?>
                                       </select>

                                       <input type="text" class="form-control" name="phone" placeholder="Phone Number" value="<?php if(isset($companyDetails[0]['phone'])){echo $companyDetails[0]['phone'];} ?>">
                                       
                                       

                                      </div>
                                       
                                       <!--<input type="text" class="form-control" name="rating" placeholder="Glass Door Rating" value="<?php //if(isset($companyDetails[0]['rating'])){if($companyDetails[0]['rating'] == 0){}else{echo $companyDetails[0]['rating'];}} ?>">
                                       
                                       <input type="text" class="form-control" name="gross_salary" placeholder="Gross Salary" value="<?php //if(isset($companyDetails[0]['gross_salary'])){if($companyDetails[0]['gross_salary'] == 0){}else{echo $companyDetails[0]['gross_salary'];}} ?>">
                                       -->
                                       <!--<input type="text" class="form-control" name="basic_salary" placeholder="Basic Salary" value="<?php //if(isset($companyDetails[0]['basic_salary'])){if($companyDetails[0]['basic_salary'] == 0){}else{echo $companyDetails[0]['basic_salary'];}} ?>">-->
                                       
                                       <!--<input type="text" class="form-control" name="work_off" placeholder="Work Off" value="<?php //if(isset($companyDetails[0]['work_off'])){echo $companyDetails[0]['work_off'];} ?>">
                                       -->
                                       <!--<input type="text" class="form-control" name="leave" placeholder="Annual leaves Count" value="<?php //if(isset($companyDetails[0]['annual_leave'])){if($companyDetails[0]['annual_leave'] == 0){}else{echo $companyDetails[0]['annual_leave'];}} ?>">
                                       
                                       <input type="text" class="form-control" name="job_type" placeholder="Job Type" value="< ?php //if(isset($companyDetails[0]['job_type'])){echo $companyDetails[0]['job_type'];} ?>">
                                       -->
									   
									   </div>
									   
									   
									   
									   </div>
                                  
                                    <div>
                                         
                                    </div>
                            
                              <!--<?php 
                               echo "<pre>";
                               print_r($topPicks2);
                              
                              ?>-->
							  
							  <div class="filldetails flxdtls">
							 
								
									<div class="stepcountfrms">
									 <div class="headsteps-gt">
                                             <h5>Please select all company-wide benefits</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
                              <?php  ?>            
                              <div class="newfiletrsgree">
                                  <h2></h2>
                                 <div class="main-hda right">
                                    <h6>Top Picks</h6>
                                 </div>
                                 <div class="filterchekers">
                                     <!--<ul>
                                         <li>
                                     <div class="setsboxsone">
                                      <div class="checkflowrt">
                                         <div class="custom-control custom-radio">
                                            <input type="checkbox" class="checked_value" value="1" id="customRadio20" name="toppic[]" <?php //if($topPicks2){if(in_array('1',$topPicks2)){echo "checked";}}?>>
                                            <label class="custom-control-label" for="customRadio20"><img src="<?php //echo base_url().'webfiles/';?>img/car.png">Automotive</label>
                                         </div>
                                      </div></li>
                                      <li>
                                      <div class="checkflowrt">
                                         <div class="custom-control custom-radio">
                                            <input type="checkbox" class="custom-control-input" value="2" id="customRadio21" name="toppic[]" <?php //if($topPicks2){if(in_array('2',$topPicks2)){echo "checked";}}?>>
                                            <label class="custom-control-label" for="customRadio21"><img src="<?php //echo base_url().'webfiles/';?>img/sun.png">Technology</label>
                                         </div>
                                      </div>
                                   </div></li>
                                   </ul>-->
                                    <ul>
                                       <!-- <li class="<?php if($topPicks2){if(in_array(1, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic1" type="checkbox" name="toppicks[]" value="1" <?php if($topPicks2){ if(in_array(1, $topPicks2)){ echo "checked";}}?>>
                                          <p class="txtonly">Bonus</p>
                                          <p> Joining<br>Bonus</p>
                                       </li> -->
                                       
                                       <li class="<?php if($topPicks2){if(in_array(2, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic2" type="checkbox" name="toppicks[]" value="2" <?php if($topPicks2){ if(in_array(2, $topPicks2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-food.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-food-1.png" class="hvrsicos">
                                          <p> Free <br>Food</p>
                                       </li>

                                       <li class="<?php if($topPicks2){if(in_array(3, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic3" type="checkbox" name="toppicks[]" value="3" <?php if($topPicks2){ if(in_array(3, $topPicks2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-white.png" class="hvrsicos">
                                          <p>Day 1 HMO</p>
                                       </li>

                                       <li class="<?php if($topPicks2){if(in_array(4, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic4" type="checkbox" name="toppicks[]" value="4" <?php if($topPicks2){ if(in_array(4, $topPicks2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-for-depended.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-for-depended-1.png" class="hvrsicos">
                                          <p> Day 1 HMO<br> for Dependent</p>
                                       </li>

                                       <!-- <li class="<?php if($topPicks2){if(in_array(5, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic5" type="checkbox" name="toppicks[]" value="5" <?php if($topPicks2){ if(in_array(5, $topPicks2)){ echo "checked";}}?>>
                                          <i class="fas fa-sun"></i>
                                          <p>Day Shift</p>
                                       </li> -->

                                       <li class="<?php if($topPicks2){if(in_array(6, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic6" type="checkbox" name="toppicks[]" value="6" <?php if($topPicks2){ if(in_array(6, $topPicks2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/14-month-pay.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/14-month-pay-1.png" class="hvrsicos">
                                          <p> 14th Month Pay</p>
                                       </li>
                                    </ul>
                                 </div>
                                 
                              </div>
                              <?php ?>
                              <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h6>Allowances and Incentives</h6>
                                 </div>
                                 <div class="filterchekers">
                                    <ul id="filterchekers">

                                       <li class="tapsct <?php if($allowance2){if(in_array(1, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct1" data-value="1">
                                          <input class="checked_value" id="allow1" type="checkbox" name="allowances[]" value="1" <?php if($allowance2){ if(in_array(1, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/cell-phone-allowance.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/cell-phone-allowance-1.png" class="hvrsicos">
                                          <p> Cellphone <br>Allowance</p>
                                       </li>

                                       <li class="tapsct <?php if($allowance2){if(in_array(2, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct2" data-value="2">
                                          <input class="checked_value" id="allow2" type="checkbox" name="allowances[]" value="2" <?php if($allowance2){ if(in_array(2, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-parking.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-parking-1.png" class="hvrsicos">
                                          <p>Free <br>Parking</p>
                                       </li> 

                                       <li class="tapsct <?php if($allowance2){if(in_array(3, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct3" data-value="3">
                                          <input class="checked_value" id="allow3" type="checkbox" name="allowances[]" value="3" <?php if($allowance2){ if(in_array(3, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-shuttle.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-shuttle-1.png" class="hvrsicos">
                                          <p> Free <br> Shuttle</p>
                                       </li>

                                       <li class="tapsct <?php if($allowance2){if(in_array(4, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct4" data-value="4">
                                          <input class="checked_value" id="allow4" type="checkbox" name="allowances[]" value="4" <?php if($allowance2){ if(in_array(4, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/annual--performance-bonus.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/annual--performance-bonus-1.png" class="hvrsicos">
                                          <p> Annual <br> Performance Bonus</p>
                                       </li>

                                       <li class="tapsct <?php if($allowance2){if(in_array(5, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct5" data-value="5">
                                          <input class="checked_value" id="allow5" type="checkbox" name="allowances[]" value="5" <?php if($allowance2){ if(in_array(5, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/reteirment-benifits.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/reteirments-benefits-1.png" class="hvrsicos">
                                          <p> Retirements <br> Benefits</p>
                                       </li>

                                       <li class="tapsct <?php if($allowance2){if(in_array(6, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct6" data-value="6">
                                          <input class="checked_value" id="allow6" type="checkbox" name="allowances[]" value="6" <?php if($allowance2){ if(in_array(6, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/transportation.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/transportation-1.png" class="hvrsicos">
                                          <p> Transport <br> Allowance</p>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li class="tapsct <?php if($allowance2){if(in_array(7, $allowance2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="allow7" type="checkbox" name="allowances[]" value="7" <?php if($allowance2){ if(in_array(7, $allowance2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/monthly-performance-incentive.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/monthly-performance-incentive-1.png" class="hvrsicos">
                                          <p> Monthly Performance <br> Incentives</p>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h6>Medical Benefits</h6>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li class="<?php if($medical2){if(in_array(1, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" id="medical1" value="1" <?php if($medical2){ if(in_array(1, $medical2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-hmo-for-dependents.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-hmo-for-dependents-1.png" class="hvrsicos">
                                          <p> Free HMO for<br>Dependents</p>
                                       </li>

                                       <li class="<?php if($medical2){if(in_array(2, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" id="medical2" value="2" <?php if($medical2){ if(in_array(2, $medical2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/critical-illness-benefits.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/critical-illness-benefits1.png" class="hvrsicos">
                                          <p> Critical Illness <br>Benefits</p>
                                       </li>

                                       <li class="<?php if($medical2){if(in_array(3, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" id="medical3" value="3" <?php if($medical2){ if(in_array(3, $medical2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/life-insurence.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/life-insurence-1.png" class="hvrsicos">
                                          <p>Life <br>Insurance</p>
                                       </li>

                                       <li class="<?php if($medical2){if(in_array(4, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" value="4" id="medical4" <?php if($medical2){ if(in_array(4, $medical2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/maternity-assistance.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/maternity-assistance-1.png" class="hvrsicos">
                                          <p> Maternity<br> Assistance</p>
                                       </li>

                                       <li class="<?php if($medical2){if(in_array(5, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" value="5" id="medical5" <?php if($medical2){ if(in_array(5, $medical2)){ echo "checked";}}?>>
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/medicine-reimbursemer.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/medicine-reimbursemer-1.png" class="hvrsicos">
                                          <p>Medicine <br>Reimbursement</p>
                                       </li>
                                    </ul>
                                 </div>
								 
								  <button class="updts" type="submit">Save</button>
								  
                              </div>
                             <!--  <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Leaves</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li id="leaves1" class="<?php if($leave2){if(in_array(1, $leave2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" class="checked_value" id="leave1"  name="leavs[]" value="1" <?php if($leave2){if(in_array(1, $leave2)){ echo "checked"; }}?>>
                                          <i class="fas fa-snowflake"></i>
                                          <p> Weekend Off</p>
                                       </li>
                                       <li id="leaves2" class="<?php if($leave2){if(in_array(2, $leave2)){ echo 'selectedgreen'; }}?>">
                                          <input type="checkbox" class="checked_value" id="leave2"  name="leavs[]" value="2" <?php if($leave2){if(in_array(2, $leave2)){ echo "checked"; }}?>>
                                          <i class="fas fa-snowflake"></i>
                                          <p>Holiday Off</p>
                                -->        </li>
                                    </ul>
                                 </div>
                                 
                              </div>
                              <div class="newfiletrsgree">
                                 <!-- <div class="main-hda right">
                                    <h3>Work Shifts</h3>
                                 </div> -->
                                 <!-- <div class="filterchekers1">
                                    <ul><?php //print_r($workshift2);?>
                                       <li id="workshift1" class="work-class <?php if(!empty($workshift2)){ if($workshift2[0]=="1"){ echo 'selectedgreen';}} ?>">
                                          <input class="work-radio" type="radio" id="works1" name="shifts[]" value="1">
                                          <i class="fas fa-star"></i> 
                                          <p> Mid Shift </p>
                                       </li>

                                       <li id="workshift2" class="work-class <?php if(!empty($workshift2)){ if($workshift2[0]=="2"){ echo 'selectedgreen';}} ?>">
                                          <input class="work-radio" id="works2" type="radio" name="shifts[]" value="2">
                                          <i class="fas fa-moon-o"></i> 
                                          <p> Night Shift </p>
                                       </li>
                                       <li id="workshift3" class="work-class <?php if(!empty($workshift2)){ if($workshift2[0]=="3"){ echo 'selectedgreen';}} ?>">
                                          <input type="radio" id="works3" class="work-radio" name="shifts[]" value="3" >
                                          <p class="txtonly">24/7</p>
                                          <p>24/7 </p>
                                       </li>

                                       
                                    </ul>
                                 </div> -->
                                 </form>
								   </div>
                              </div>
							   </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      <div id="uploadimageModal" class="modal" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            
            </div>
            <div class="modal-body">
            <div class="row">
               <div class="col-md-12 text-center">
                    <div id="image_demo" style=" margin-top:30px"></div>
               </div>
            </div>
            </div>
            <div class="modal-footer">
            <button class="btn btn-success crop_image">Crop & Upload Image</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
      </div>
    </div>
</div>
      </section>
      <?php include_once("footer.php");?>
      <?php include_once("modalpassword.php");?>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'recruiterfiles/';?>js/croppie.js"></script>
   </body>

<script>
  $(function () { 
    $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
  });  
   
  $(".progress-bar").each(function(){
    each_bar_width = $(this).attr('aria-valuenow');
    $(this).width(each_bar_width + '%');
  });
         
</script>
   
   <script>
    $(".filterchekers li").click(function(){
      $(this).toggleClass("selectedgreen");  
    });
   </script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&sensor=false&libraries=places&callback=initMap"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.A;
            var longitude = place.geometry.location.F;
            var mesg = "Address: " + address;
            mesg += "\nLatitude: " + latitude;
            mesg += "\nLongitude: " + longitude;
        });
    });
    
    function readURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();
    
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
    
        reader.readAsDataURL(input.files[0]);
      }
    }
    
    $("#imgInp").change(function() {
      readURL(this);
    });
    
    $('.work-radio').change(function() {
            $(".work-class").removeClass("selectedgreen");
            if ($(this).is(':checked')){
                $(this).closest("li").addClass("selectedgreen");
              }
              else
                $(this).closest("li").removeClass("selectedgreen");
        });
</script>
<script type="text/javascript">
      var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\.~`="
      var check = function(string){
          for(i = 0; i < specialChars.length;i++){
              if(string.indexOf(specialChars[i]) > -1){
                  return true
              }
          }
          return false;
      }


      $("#txtPlaces").change(function(){
            var textplace = $(this).val();

            if(check($('#txtPlaces').val()) == true){
                  alert("Special characters like !,@,#,$,%,^,&,*,(,),+,= not required");
              }
      });

</script>

<script>  
$(document).ready(function(){

   /*$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:280,
      height:280,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });*/

  /*$('#imgInp').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });*/

  /*$('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        url:"<?php echo base_url(); ?>recruiter/recruiter/upload",
        type: "POST",
        data:{"image": response},
        success:function(data)
        {
         //alert(data);
          $('#uploadimageModal').modal('hide');
          $('#blah').attr('src',data);
          $('#cropimg').val(data);
        }
      });
    })
  });*/

});  
</script>

