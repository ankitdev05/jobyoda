<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoda</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
   </head>
   <style>
      .rap{
      float: left;
      width: 100%;
      padding: 50px 10px;
      border: 1px solid rgba(51, 51, 51, 0.08);
      box-shadow: 0px 8px 16px -4px rgba(128, 128, 128, 0.22);
      /* margin-top: 70px; */
      height: 316px;
      }
      .rap .account-popup .cfield{
      margin-bottom: 35px
      }
   </style>
   <body style="background: #f5f5f5;">
      <div class="page-loading" style="display: none;">
         <img src="<?php echo base_url().'recruiterfiles/';?>images/loader.gif" alt="">
         <span>Skip Loader</span>
      </div>
      <div class="theme-layout" id="scrollup">
         <div class="responsive-header">
            <div class="responsive-menubar">
               <div class="res-logo"><a href="index.html" title=""><img src="http://placehold.it/178x40" alt=""></a></div>
               <div class="menu-resaction">
                  <div class="res-openmenu">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/icon.png" alt=""> Menu
                  </div>
                  <div class="res-closemenu">
                     <img src="<?php echo base_url().'recruiterfiles/';?>images/icon2.png" alt=""> Close
                  </div>
               </div>
            </div>
         </div>

         <section>
            <div class="block remove-bottomms bgatype">
               <div class="container">
                  <div class="row">
                     <div class="col-lg-12">
                        <div class="account-popup-area signin-popup-box static loginfrms">
                           <div class="jobyodaformlogo">
                            <a href="<?php echo base_url();?>recruiter/recruiter">
                              <img src="<?php echo base_url().'recruiterfiles/';?>images/Final.png">  
                            </a>
                           </div>
                           <div class="account-popup">
                              <div class="rap">

                                 <h4 style="float: left; margin: 0 0 15px 0;">Enter Verification Code</h4>
                                <?php
                                  if(isset($forgotsuccess)) {
                                ?>
                                   <p style="text-align:center; float: left; width: 100%; color: #26ae61; ">
                                   Verification Code is sent to your registered mail id</p>
                                 <?php
                                }
                                 ?>
                                 <?php
                                  if(isset($verifyerror)) {
                                ?>
                                   <p style="text-align:center; float: left; width: 100%; color:#f00;">
                                   <?php echo $verifyerror; ?></p>
                                 <?php
                                }
                                 ?>
                                 <form method="post" action="<?php echo base_url();?>recruiter/recruiter/verifysignupemail">
                                    <div class="cfield">
                                       <input type="text" name="codeverify" placeholder="4 Digit Verification Code Here">
                                       <i class="la la-envelope-o"></i>
                                    </div>
                                    <div class="fullwdt">
                                      <input type="hidden" name="ids" value="<?php if($userId){ echo $userId;}?>">
                                       <button type="submit" class="loginlink">Verify</button> 
                                  </form>
                                  <form method="post" action="<?php echo base_url();?>recruiter/recruiter/resendMail">
                                      <input type="hidden" name="ids" value="<?php if($userId){ echo $userId;}?>">
                                       <button type="submit" style="margin: 21px 12px; color: #fff; text-decoration: underline;"> Resend</button>
                                       <!--<button type="submit">Login</button> -->
                                    </div>
                                 </form>
                              </div>
                              <div class="sign">
                                 <p>Already Have An Account Yet?<span><a href="<?php echo base_url();?>recruiter/recruiter/"><strong>Login</strong></a></span></P>
                              </div>
                           </div>
                        </div>
                        <!-- LOGIN POPUP -->
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      
      <!-- SIGNUP POPUP -->
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/maps2.js" type="text/javascript"></script>
   </body>
</html>

