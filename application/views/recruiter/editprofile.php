<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
   </head>
   <style>
span.err{color: #b70f2c;}
      .filldetails input.form-control {    float: left;    width: 30%;  }
      a:hover {
      text-decoration: none;
      }.filldetails button {    width: 20%; }
      .tech i.fa.fa-ellipsis-h {
      float: right;
      }
      .progr span.progress-bar-tooltip {
      position: initial;
      color: #fff;
      }
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 11px;
      line-height: 0;
      padding: 10px;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
	  .chang {    float: left;    width: 100%;    padding: 0px 0px;}.chang p {    float: left;    margin: 7px 0px;   color: #000;    font-weight: 600;}
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }.filterchekers a {  color: #47b476;    border-radius: 10px;    border: 1px solid#47b476;}
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }.bb button {    text-align: center;    float: none;    margin: 0 auto; }.bb{ float: left; width:100%; text-align: center;     margin-bottom: 25px;}.right h3 {    font-size: 20px;    color: #47b476;}
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .fall {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 23px 10px;
      }
      .progress-bar{
      background-color: #47b476;
      }
      .details {
      float: left;
      width: 100%;
      padding: 5px 17px;
      background-color: #47b476;
      color: #fff;
      margin: 15px;
      border-radius: 20px
      }
      .leftdetails {
      float: left;
      width: 100%;
      }
      .left {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px;
      }
      .detail span {
      display: block;
      float: right;
      margin: 12px 8px;  
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detail strong {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px 55px;
      font-weight: 700;
      font-size: 20px;
      }
      .detail, .detai {
      float: left;
      width: 100%;
      }
      .detai span {
      display: -webkit-box;
      width: 100%;
      font-weight: 600;
      margin: 0px;
      padding: 12px 0px;
      font-size: 20px;
      }
      .progras {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      }.filterchekers {float: left;width: 100%;padding:60px 0px;}
      .filterchekers ul li {text-align: center;width: 29%;}
      .detai strong {
      float: right;
      font-size: 20px;
      }
      .detail p, .detai p {
      font-weight: 600;
      font-size: 16px;
      color: #000;
      }
      .detail .progress span {
      position: absolute;
      right: 0px;
      font-size: 13px;
      color: #fff;
      border: 0px solid#ddd;
      }
      .head {
      float: left;
      width: 100%;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .num {
      float: left;
      margin: 20px 0px;
      }
      .para {
      float: left;
      width: 100%;
      border: 1px solid#dddd;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .tech {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      margin: 10px 0px;
      }
      .search-container {
      float: left;
      }
      .search button {
      padding: 7px 4px;
      color: #fff;
      background-color: #47b476;
      border: none;
      width: 36%;
      }
      .upload a {
      color: #ffffff;
      text-decoration: none;
      background: #47b476;
      padding: 10px;
      border-radius: 50px;
      }
      .search input[type="text"] {
      padding: 9px 6px;
      width: 63%;
      background-color: #fff;
      border: 1px solid#ddd;
      }
      .details i.fa.fa-filter {
      float: right;
      margin: 5px 0px;
      padding: 0px;
      }
      .details p {
      float: left;
      margin: 0px;
      padding: 0px;
      color: #fff;
      }
      .head p {
      color: #fff;
      }
      .para p {
      color: #fff;
      }
      .upload {
      float: right;
      padding: 8px;
      margin: px;
      }
      border: 1px solid#ddd;
      padding: 10px;
      }
      .dot {
      float: right;
      }
      .progr {
      float: left;
      width: 100%;
      margin-bottom: 18px;
      }.right {    padding: 10px 0;    margin: 20px 0 0 0;    border-bottom: 1px solid #ddd;}
      .progr .progress {
      position: absolute;
      left: 9%;
      height: 20px;
      -webkit-border-radius: 8px;
      -moz-border-radius: 8px;
      -ms-border-radius: 8px;
      -o-border-radius: 8px;
      border-radius: 8px;
      top: initial;
      width: 80%;
      }
      .progr .progress-label {
      margin: 12px 0px;
      }
      .progress-bar.progress-bar-primary {
      padding: 0px;
      }
      .texts p {
      position: absolute;
      top: 70%;
      left: 29%;
      }
      p.dropdown-toggle {
      font-size: 34px;
      margin: -30px;
      }
      .dot {
      float: right;
      }
      .texts {
      margin-top: 20px;
      }
      .text p {
      margin: 0px;
      padding: 0px;
      font-size: 13px;
      }
      .right .progress{
      width: 150px;
      height: 150px;
      line-height: 150px;
      background: none;
      margin: 0 auto;
      box-shadow: none;
      position: relative;
      }
      .right .progress:after{
      content: "";
      width: 100%;
      height: 100%;
      border-radius: 50%;
      border: 2px solid #fff;
      position: absolute;
      top: 0;
      left: 0;
      }.companyprofileforms h6 {    color: #26ae61;    margin: 40px 0 0 0;    font-weight: 500;    text-align: left;    margin-bottom: 20px;    font-size: 20px;}
      .right .progress > span{
      width: 50%;
      height: 100%;
      overflow: hidden;
      position: absolute;
      top: 0;
      z-index: 1;
      }
      .progress .progress-left{
      left: 0;
      }
      .right .progress .progress-bar{
      width: 100%;
      height: 100%;
      background: none;
      border-width: 2px;
      border-style: solid;
      position: absolute;
      top: 0;
      }
      .right .progress .progress-left .progress-bar{
      left: 100%;
      border-top-right-radius: 80px;
      border-bottom-right-radius: 80px; 
      border-left: 0;
      -webkit-transform-origin: center left;
      transform-origin: center left;
      }
      .right .progress .progress-right{
      right: 0;
      }
      .right .progress .progress-right .progress-bar{
      left: -100%;
      border-top-left-radius: 80px;
      border-bottom-left-radius: 80px;
      border-right: 0;
      -webkit-transform-origin: center right;
      transform-origin: center right;
      animation: loading-1 1.8s linear forwards;
      }
      .right .progress .progress-value{
      width: 59%;
      height: 59%;
      border-radius: 50%;
      border: 2px solid #ebebeb;
      font-size: 28px;
      line-height: 82px;
      text-align: center;
      position: absolute;
      top: 7.5%;
      left: 7.5%;
      }
      .right .progress.blue .progress-bar{
      border-color: #049dff;
      }
      .right .progress.blue .progress-value{
      color: #47b476;
      }
      .right .progress.blue .progress-left .progress-bar{
      animation: loading-2 1.5s linear forwards 1.8s;
      }
      .progress.yellow .progress-bar{
      border-color: #fdba04;
      }
      .right .progress.yellow .progress-value{
      color: #fdba04;
      }
      .right .progress.yellow .progress-left .progress-bar{
      animation: loading-3 1s linear forwards 1.8s;
      }
      .right .progress.pink .progress-bar{
      border-color: #ed687c;
      }
      .right .progress.pink .progress-value{
      color: #ed687c;
      }
      .right .progress.pink .progress-left .progress-bar{
      animation: loading-4 0.4s linear forwards 1.8s;
      }
      .right .progress.green .progress-bar{
      border-color: #1abc9c;
      }
      .right .progress.green .progress-value{
      color: #1abc9c;
      }
      .right .progress.green .progress-left .progress-bar{
      animation: loading-5 1.2s linear forwards 1.8s;
      }
      @keyframes loading-1{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
      }
      }
      @keyframes loading-2{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(144deg);
      transform: rotate(144deg);
      }
      }
      @keyframes loading-3{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(90deg);
      transform: rotate(90deg);
      }
      }
      @keyframes loading-4{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(36deg);
      transform: rotate(36deg);
      }
      }
      @keyframes loading-5{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(126deg);
      transform: rotate(126deg);
      }
      }@media (max-width: 767px){.filldetails input.form-control {    float: left;    width: 100%;}
      .main-hda.right input.form-control {    width: 50%;  }
      .chang p {    float: left;    margin: 7px 13px;    color: #000;    font-weight: 600;}
      .filterchekers ul li {    text-align: center;    width: 100%;}}
      @media only screen and (max-width: 990px){
      .right .progress{ margin-bottom: 20px; }
      }
      .uploadareas input[type="file"] {
            width: 55%;
            height: 124px;
       }
       .filldetails input.form-control {
    width: 100%!important;
    margin: 10px 0px;
}
.filldetails label {
    margin: 10px 0px;
}
       .chang input.form-control {
    width: 100%!important;
}
.chang label {
    width: 100%!important;
    margin: 10px 0px;
}
 }
.whats input[type="checkbox"] {
    opacity: 1;
    float: right;
    position: relative;
    z-index: 0;
}

.app input[type="checkbox"], input[type="radio"], .app2 input[type="checkbox"], input[type="radio"] {

       position: absolute;


    top: 6px;

    width: 100%;
}



.managerecsite .filldetails .app label, .managerecsite .filldetails .app2 label{
    width: 22%;
    float: left;
    margin: 0;
	padding-left: 26px;

}

.app, .app2 {
    display: flex;
    width: 100%;
    position: relative;
}

.app {
    margin: 20px 0 0;
}

.app2 {
    margin-bottom: 30px;
}

.custom-control-label::before{
    content:"" !important; 
}

.custom-checkbox .custom-control-input:checked~.custom-control-label::before{
       content:"";     
       background:#fbaf31;
       outline:none;
	       box-shadow: none;
}

input[type="checkbox"] + label::after{
    content:"";
    outline:none;
}

div#msgModal i.fa.fa-check {
    padding: 29px 27px;
    background: #26ae61;
    color: #fff;
    border-radius: 100%;
    font-size: 30px;
}

div#msgModal {
    text-align: center;
}
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <!-- Logo -->
                  <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 recruprogfd MainWrapper">
                        <div class="companyprofileforms managerecsite">
                              <div class="col-md-12">
                                
                                 <form method="post" action="<?php echo base_url();?>recruiter/recruiter/profileupdate" enctype="multipart/form-data">
                                     <div class="row">
                                        
                                        <div class="col-md-8 psthbs">
										 
                                           <div class="filldetails">
										   <h6>Recruiter Profile</h6>
										   <div class="chang">
                                            <label>Name</label>
                                              <input type="text" class="form-control" name="name" placeholder="Name" value="<?php if(!empty($errors['name'])){}else{echo $companie[0]['fname'].' '.$companie[0]['lname'];} ?>">
                                              <?php if(!empty($errors['name'])){ echo "<span class='err'>".$errors['name']."</span>";}?>
                                            </div>
                                             <div class="chang">											
                                              <label>Email Id</label>
                                              <input type="text" class="form-control" name="email" placeholder="Email id" value="<?php if(!empty($errors['email'])){}else{echo $companie[0]['email'];}?>" readonly>
                                              <?php if(!empty($errors['email'])){ echo "<span class='err'>".$errors['email']."</span>";}?>
                                              <label>Country Code</label>                          
                                              <select name="phonecode" id="phonecode">
                                                <option value=""> Select Country Code</option>
                                                <?php
                                                   foreach($phonecodes as $phonecode) {

                                                   ?>
                                                <option value="<?php echo $phonecode["phonecode"];?>" <?php if(!empty($companyDetails[0]['phonecode'])) { if($companyDetails[0]['phonecode'] == $phonecode["phonecode"]){ echo "selected";}} ?>  > <?php echo $phonecode["name"];?> - <?php echo $phonecode["phonecode"];?> </option>
                                                <?php
                                                   }
                                                   ?>
                                             </select>
                                              <!-- <?php if(!empty($errors['phonecode'])){ echo "<span class='err'>".$errors['phonecode']."</span>";}?> -->
                                              <label>Mobile No.</label>
                                                                        
                                              <input type="text" class="form-control" name="phone" placeholder="Phone Number" value="<?php if(!empty($errors['phone'])){}else{echo $companyDetails[0]['phone'];}?>">
                                              <?php if(!empty($errors['phone'])){ echo "<span class='err'>".$errors['phone']."</span>";}?>

                                              <label>Landline No.</label>
                                                                        
                                              <input type="number" class="form-control" name="landline" minlength="6" placeholder="Landline Number" value="<?php if(!empty($errors['landline'])){}else{echo $companyDetails[0]['landline'];}?>">
                                              <?php if(!empty($errors['landline'])){ echo "<span class='err'>".$errors['landline']."</span>";}?>
                                              </div>
                                              
                                                              
                                            <div class="filldetails whats">
                                              <h6>Your Preferred Communication Channel</h6>
                                               <div class="app">
											   
											  <?php $comm = explode(',', $companyDetails[0]['comm_channel']); ?> 
											   <div class="custom-control custom-checkbox mr-sm-2">
        <input type="checkbox" name="comm_channel[]" value="1" <?php if(!empty($companyDetails[0]['comm_channel']) && in_array('1', $comm)){ echo "checked" ;} ?> class="custom-control-input" id="customControlAutosizing">
        <label class="custom-control-label" for="customControlAutosizing">Whatsapp</label>
      </div>
	  
                                               
   
                                           
                                                 </div>
                                                 <div class="app2">
                                                <div class="custom-control custom-checkbox mr-sm-2">
        <input type="checkbox" name="comm_channel[]" value="2"  <?php if(!empty($companyDetails[0]['comm_channel']) && in_array('2', $comm)){ echo "checked" ;} ?> class="custom-control-input" id="customControlAutosizing2">
        <label class="custom-control-label" for="customControlAutosizing2">Viber</label>
      </div>
                                               </div>
                                            </div>
                                            
                                              <div class="bb">
                                                <button type="submit">Save</button> 
                                              </div>
                                           </div>
                                        </div>
                                                            
                                                            <div class="col-md-4 mobspc">
															<div class="psthbs">
															 <div class="mailayer">
														<div class="camico">
									<i class="fas fa-camera"></i>
                                        
									   </div>	 
                                           <div class="uploadareas">
                                              <input type="file" name="profilePic" id="imgInp" accept="image/x-png,image/jpg,image/jpeg">
                                              <?php
                                                if(!empty($companyDetails[0]['companyPic'])) {
                                              ?>
                                                    <img id="blah" src="<?php echo $companyDetails[0]['companyPic'];?>">
                                              <?php
                                                } else{
                                              ?>
                                                    <img id="blah" src="<?php echo base_url().'recruiterfiles/';?>images/camera1.png">
                                              <?php
                                                }
                                              ?>
                                           </div>
										   </div>
										   
										   <div class="filterchekers recruprofileflt">
                                 <ul>
                                    <li><a href="<?php echo base_url();?>recruiter/jobpost/manageJobView">Jobs Posted</a></li>
                                    <!--<li><a href="<?php //echo base_url();?>recruiter/candidate/manageCandidates?status=7">Hired Candidates  </a></li>-->
                                    <li><a href="<?php echo base_url();?>recruiter/candidate/manageCandidates">Manage Candidates </a></li>
                                 </ul>
                              </div>
							  
                                        </div>
                                           </div>                 
                                     </div>
                                 </form>
                              </div>
                           </form>
                           <div class="newfiletrsgree">
                              <div class="main-hda">

                                 <div class="col-md-8 psthbs">
                                    <form method="post" action="<?php echo base_url();?>recruiter/recruiter/changepassword">
                                       
                                          
                                           <div class="">
<!-- <div class="col-md-12 text-center">
<p style="color: #165a07;    font-size: 18px;margin-bottom:10px;">
<?php if($this->session->tempdata('item')){echo $this->session->tempdata('item');}?>
</p>
</div> -->
                                                               <div class="filldetails">
															    <h6>Change Password</h6>
                                              <div class="chang">
                                                 <label>New Password</label>
                                                 <input type="password" class="form-control" name="password" placeholder="Password">
                                                 <?php if(!empty($errors['password'])){ echo "<span class='err'>".$errors['password']."</span>";}?>
                                              </div>
                                                                    
                                                                    <div class="chang">
                                                  <label>Confirm Password</label>
                                                 <input type="password" class="form-control" name="confirmPassword" placeholder="Password">
                                                 <?php if(!empty($errors['confirmPassword'])){ echo "<span class='err'>".$errors['confirmPassword']."</span>";}?>
                                              </div>
                                              <div class="bb">
                                                <button type="submit">Save</button>
                                            </div>
                                           </div>
                                          
                                           
                                                                  </div>
                                        
                                    </form>
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
         <?php include_once('footer.php');?>
      </div>
      <!-- Modal -->
        <div class="modal fade" id="msgModal" role="dialog">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              
              <div class="modal-body">
                <i class="fa fa-check" aria-hidden="true"></i>
                <p class="success_msg"><?php if($this->session->tempdata('inserted')){ echo $this->session->tempdata('inserted'); } ?></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
            
          </div>
      <div class="modal fade" id="exampleModalCenter10" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="formmidaress modpassfull">
                     <div class="filldetails">
                        <form>
                           <div class="addupdatecent">
                              <p style="color: #165a07;    font-size: 18px;margin-bottom:10px;">
                                    <?php if($this->session->tempdata('item')){echo $this->session->tempdata('item');}?>
                              </p>
                              <p></p>
                              <div class="statsusdd">
                                 
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <script>
      $(function () { 
        $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
      });  
      
      // $( window ).scroll(function() {   
       // if($( window ).scrollTop() > 10){  // scroll down abit and get the action   
        $(".progress-bar").each(function(){
          each_bar_width = $(this).attr('aria-valuenow');
          $(this).width(each_bar_width + '%');
        });
            function readURL(input) {

                  if (input.files && input.files[0]) {
                    var reader = new FileReader();
                
                    reader.onload = function(e) {
                      $('#blah').attr('src', e.target.result);
                    }
                
                    reader.readAsDataURL(input.files[0]);
                  }
                }
                
                $("#imgInp").change(function() {
                  readURL(this);
                }); 
   </script>
   </body>
</html>
<?php
         if($this->session->tempdata('inserted')!='') {
             echo($this->session->tempdata('inserted')) ;
             /*if($this->session->tempdata('validationError')){$this->session->unset_tempdata('validationError');}*/
      ?>
            <script type="text/javascript">
                $(window).on('load',function(){
                    $('#msgModal').modal('show');
                });
                if ( window.history.replaceState ) {
                    window.history.replaceState( null, null, window.location.href );
                    //window.location.href = "<?php echo base_url();?>recruiter/recruiter/index";
                }
            </script>
      <?php
         }
     ?>
<?php if($this->session->tempdata('item')!= null) {
    
    if($this->session->tempdata('item')){$this->session->unset_tempdata('item');}
?>
      <script type="text/javascript">
        $(window).on('load',function(){
           $('#exampleModalCenter10').modal('show');
        });
        if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
        }
      </script>
<?php
  }
?>