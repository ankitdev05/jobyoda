<div class="footerrecruiter">
  <div class="container">
    <div class="addressar">
    <!-- 
      <p> <strong>Office Address:</strong> Level 10-1 Fort Legend Tower, 3rd Avenue Bonifacio Global City 31st Street Fort Bonifacio TAGUIG CITY, FOURTH DISTRICT, NCR, Philippines, 1634</p> 
    -->
      <p><strong>Office Address:</strong>
        Level 10-1 Fort Legend Tower, 3rd Avenue Bonifacio Global City 31st Street Fort Bonifacio Taguig City, Fourth District, NCR, Philippines, 1634
      </p>
    </div>
  
    <div class="alinflow">
      <p><strong>Email -</strong> <a href="mailto:help@jobyoda.com">help@jobyoda.com</a></p>
      <p><strong>Phone No -</strong> <a href="tel:+63 9178721630">+63 9178721630</a></p>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- <script type="text/javascript">
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 170) {
      $(".Sidenavbar").addClass("FixedSidenav"); 
      $(".MainWrapper").addClass("Fixeddashboard"); 
    } else {
      $(".Sidenavbar").removeClass("FixedSidenav");
      $(".MainWrapper").removeClass("Fixeddashboard");
    }
  });
</script> -->

<!-- <script type="text/javascript">
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
      $("header.stick-top").addClass("hdafxd");  
    } else {
      $("header.stick-top").removeClass("hdafxd"); 
    }
  });
</script> -->




<script>
  $.fn.jQuerySimpleCounter = function( options ) {
    var settings = $.extend({
        start:  0,
        end:    100,
        easing: 'swing',
        duration: 400,
        complete: ''
    }, options );

    var thisElement = $(this);

    $({count: settings.start}).animate({count: settings.end}, {
    duration: settings.duration,
    easing: settings.easing,
    step: function() {
      var mathCount = Math.ceil(this.count);
      thisElement.text(mathCount);
    },
    complete: settings.complete
  });
};

<?php
    $userData11 = $this->session->userdata('userSession11');
?>

$('#number1').jQuerySimpleCounter({end: <?php echo $userData11['onedata']; ?>,duration: 3000});
$('#number2').jQuerySimpleCounter({end: <?php echo $userData11['twodata']; ?>,duration: 3000});

$('#number3').jQuerySimpleCounter({end: 50000,duration: 4000});
$('#number4').jQuerySimpleCounter({end: 1000,duration: 2500});

$('.about-me-img').hover(function(){
  $('.authorWindowWrapper').stop().fadeIn('fast').find('p').addClass('trans');
}, function(){
  $('.authorWindowWrapper').stop().fadeOut('fast').find('p').removeClass('trans');
});
</script>