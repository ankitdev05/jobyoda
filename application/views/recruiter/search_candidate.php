

<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoda</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="http://mobuloustech.com/jobyoda/recruiterfiles/images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
   </head>
   <style>
      a:hover {
      text-decoration: none;
      }
      .tech i.fa.fa-ellipsis-h {
      float: right;
      }
      .progr span.progress-bar-tooltip {
      position: initial;
      color: #fff;
      }
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 11px;
      line-height: 0;
      padding: 10px;
      }
      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .fall {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 23px 10px;
      }
      .progress-bar{
      background-color: #47b476;
      }
      .details {
      float: left;
      width: 100%;
      padding: 5px 17px;
      background-color: #47b476;
      color: #fff;
      margin: 15px;
      border-radius: 20px
      }
      .leftdetails {
      float: left;
      width: 100%;
      }
      .left {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px;
      }
      .detail span {
      display: block;
      float: right;
      margin: 12px 8px;  
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detail strong {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px 55px;
      font-weight: 700;
      font-size: 20px;
      }
      .detail, .detai {
      float: left;
      width: 100%;
      }
      .detai span {
      display: -webkit-box;
      width: 100%;
      font-weight: 600;
      margin: 0px;
      padding: 12px 0px;
      font-size: 20px;
      }
      .progras {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detai strong {
      float: right;
      font-size: 20px;
      }
      .detail p, .detai p {
      font-weight: 600;
      font-size: 16px;
      color: #000;
      }
      .detail .progress span {
      position: absolute;
      right: 0px;
      font-size: 13px;
      color: #fff;
      border: 0px solid#ddd;
      }
      .head {
      float: left;
      width: 100%;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .num {
      float: left;
      margin: 20px 0px;
      }
      .para {
      float: left;
      width: 100%;
      border: 1px solid#dddd;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .tech {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      margin: 10px 0px;
      }
      .search-container {
      float: left;
      }
      .search button {
      padding: 7px 4px;
      color: #fff;
      background-color: #47b476;
      border: none;
      width: 36%;
      }
      .upload a {
      color: #ffffff;
      text-decoration: none;
      background: #47b476;
      padding: 10px;
      border-radius: 50px;
      }
      .search input[type="text"] {
      padding: 9px 6px;
      width: 63%;
      background-color: #fff;
      border: 1px solid#ddd;
      }
      .details i.fa.fa-filter {
      float: right;
      margin: 5px 0px;
      padding: 0px;
      }
      .details p {
      float: left;
      margin: 0px;
      padding: 0px;
      color: #fff;
      }
      .head p {
      color: #fff;
      }
      .para p {
      color: #fff;
      }
      .upload {
      float: right;
      padding: 8px;
      margin: px;
      }
      border: 1px solid#ddd;
      padding: 10px;
      }
      .dot {
      float: right;
      }
      .progr {
      float: left;
      width: 100%;
      margin-bottom: 18px;
      }
      .progr .progress {
      position: absolute;
      left: 9%;
      height: 20px;
      -webkit-border-radius: 8px;
      -moz-border-radius: 8px;
      -ms-border-radius: 8px;
      -o-border-radius: 8px;
      border-radius: 8px;
      top: initial;
      width: 80%;
      }
      .progr .progress-label {
      margin: 12px 0px;
      }
      .progress-bar.progress-bar-primary {
      padding: 0px;
      }
      .texts p {
      position: absolute;
      top: 70%;
      left: 29%;
      }
      p.dropdown-toggle {
      font-size: 34px;
      margin: -30px;
      }
      .dot {
      float: right;
      }
      .texts {
      margin-top: 20px;
      }
      .text p {
      margin: 0px;
      padding: 0px;
      font-size: 13px;
      }
      .right .progress{
      width: 150px;
      height: 150px;
      line-height: 150px;
      background: none;
      margin: 0 auto;
      box-shadow: none;
      position: relative;
      }
      .right .progress:after{
      content: "";
      width: 100%;
      height: 100%;
      border-radius: 50%;
      border: 2px solid #fff;
      position: absolute;
      top: 0;
      left: 0;
      }
      .right .progress > span{
      width: 50%;
      height: 100%;
      overflow: hidden;
      position: absolute;
      top: 0;
      z-index: 1;
      }
      .progress .progress-left{
      left: 0;
      }
      .right .progress .progress-bar{
      width: 100%;
      height: 100%;
      background: none;
      border-width: 2px;
      border-style: solid;
      position: absolute;
      top: 0;
      }
      .right .progress .progress-left .progress-bar{
      left: 100%;
      border-top-right-radius: 80px;
      border-bottom-right-radius: 80px; 
      border-left: 0;
      -webkit-transform-origin: center left;
      transform-origin: center left;
      }
      .right .progress .progress-right{
      right: 0;
      }
      .right .progress .progress-right .progress-bar{
      left: -100%;
      border-top-left-radius: 80px;
      border-bottom-left-radius: 80px;
      border-right: 0;
      -webkit-transform-origin: center right;
      transform-origin: center right;
      animation: loading-1 1.8s linear forwards;
      }
      .right .progress .progress-value{
      width: 59%;
      height: 59%;
      border-radius: 50%;
      border: 2px solid #ebebeb;
      font-size: 28px;
      line-height: 82px;
      text-align: center;
      position: absolute;
      top: 7.5%;
      left: 7.5%;
      }
      .right .progress.blue .progress-bar{
      border-color: #049dff;
      }
      .right .progress.blue .progress-value{
      color: #47b476;
      }
      .right .progress.blue .progress-left .progress-bar{
      animation: loading-2 1.5s linear forwards 1.8s;
      }
      .progress.yellow .progress-bar{
      border-color: #fdba04;
      }
      .right .progress.yellow .progress-value{
      color: #fdba04;
      }
      .right .progress.yellow .progress-left .progress-bar{
      animation: loading-3 1s linear forwards 1.8s;
      }
      .right .progress.pink .progress-bar{
      border-color: #ed687c;
      }
      .right .progress.pink .progress-value{
      color: #ed687c;
      }
      .right .progress.pink .progress-left .progress-bar{
      animation: loading-4 0.4s linear forwards 1.8s;
      }
      .right .progress.green .progress-bar{
      border-color: #1abc9c;
      }
      .right .progress.green .progress-value{
      color: #1abc9c;
      }
      .right .progress.green .progress-left .progress-bar{
      animation: loading-5 1.2s linear forwards 1.8s;
      }
      @keyframes loading-1{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
      }
      }
      @keyframes loading-2{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(144deg);
      transform: rotate(144deg);
      }
      }
      @keyframes loading-3{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(90deg);
      transform: rotate(90deg);
      }
      }
      @keyframes loading-4{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(36deg);
      transform: rotate(36deg);
      }
      }
      @keyframes loading-5{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(126deg);
      transform: rotate(126deg);
      }
      }
      @media only screen and (max-width: 990px){
      .right .progress{ margin-bottom: 20px; }
      }
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 psthbs MainWrapper">
                        <div class="row">
                           <div class="col-md-12 col-lg-9 managecandi">
                              <div class="padding-left">
                                 <div class="srchresult">
                                    <!--<h4>Ios Developer (found 23 Result)</h4>-->
                                 </div>
                                 <div class="emply-resume-sec">
                                    <?php
                                       if($candidatesApplied1) {
                                         foreach($candidatesApplied1 as $applied) {
                                       ?> 
                                    <div class="emply-resume-list">
                                       <div class="emply-resume-thumb">
                                          <?php
                                             if(empty($applied['profilePic'])) {
                                             ?>
                                          <img src="<?php echo base_url().'recruiterfiles/';?>images/man.png" alt="">
                                          <?php
                                             } else {
                                             ?>
                                          <img src="<?php echo $applied['profilePic']; ?>" alt="">
                                          <?php
                                             }
                                             ?>
                                       </div>
                                       <div class="emply-resume-info">
                                          <h3><a href="<?php echo base_url()?>/recruiter/candidate/singleCandidates?id=<?php echo base64_encode($applied['id'])?>" title=""><?php echo $applied['name']; ?></a></h3>
                                          <span><i class="fa fa-envelope" aria-hidden="true"></i><?php echo $applied['email']; ?></span>
                                          <span><i class="fa fa-phone" aria-hidden="true"></i><?php echo $applied['phone']; ?></span>
                                          <p><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo $applied['location']; ?></p>
                                          <p><i class="fa fa-picture-o" aria-hidden="true"></i><?php if(!empty($applied['resume'])){?><a href="<?php if(!empty($applied['resume'])){ echo $applied['resume']; }?>" target="_blank">Attached resume</a> <?php }?></p>
                                          <!--<p><?php echo $applied['jobDesc']; ?> </p>-->
                                       </div>
                                       <div class="shortlists">
                                          <form>
                                             <div class="shoropnt">
                                                <a href="<?php echo base_url()?>/recruiter/candidate/singleCandidates?id=<?php echo base64_encode($applied['id'])?>" title="">View Profile</a>
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                    <?php }}else{?>
                                     <center> <h4>No Data Found</h4></center>
                                    <!-- Emply List -->
                                    <?php }?>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-3  filtersnavs">
						   <div class="filrgthda">
							  <h3>Filters</h3>
							  </div>
							  
                              <form method="post" action="<?php echo base_url();?>recruiter/filtercandidate">
                                 <div class="widget aplreset">
                                    <button type="submit">Apply</button>
                                    <input type="hidden" name="keyword" value="<?php echo $keyword;?>">
                                    <button type="reset" class="resetall">Reset All</button>
                                 </div>
                                  <div class="widget">
                                    <h3 class="sb-title open">Job Level</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox ">
                                          <select name="level">
                                             <option value=""> Select </option>
                                             <?php
                                                foreach($levels as $level) {
                                              ?>
                                             <option value="<?php echo $level['id'];?>" <?php if(!empty($filterdata)){if(!empty($filterdata['level'])){if($filterdata['level'] == $level['id']){echo "selected";}}} ?>> <?php echo $level['level'];?> </option>
                                             <?php
                                                }
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="widget">
                                    <h3 class="sb-title open">Job Category</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox ">
                                          <select name="category" id="category">
                                                      <option value=""> Select Category </option>
                                                      <?php
                                                         if($category) {
                                                             foreach($category as $categorys) {
                                                         ?>
                                                      <option value="<?php echo $categorys['id']; ?>" <?php if(!empty($filterdata['category'])){ if($filterdata['category'] == $categorys['id']) {echo "selected";}} ?>> <?php echo $categorys['category']; ?> </option>
                                                      <?php
                                                         }
                                                         }
                                                         ?>
                                                   </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="widget">
                                    <h3 class="sb-title open">Job Subcategory</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox ">
                                          <select name="subcategory" id="subcategory">
                                             <option value=""> Select </option>
                                             
                                             
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="widget">
                                    <h3 class="sb-title open">Education</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox">
                                          <select name="education">
                                             <option value=""> Select Education Level </option>
                                                      <option value="No Requirement" <?php if(!empty($filterdata['education'])){$filterdata['education'] == 'No Requirement' ? ' selected="selected"' : '';}?>>No Requirement</option>
                                                      <option value="High School Graduate" <?php if(!empty($filterdata['education'])){ $filterdata['education'] == 'High School Graduate' ? ' selected="selected"' : '';}?>>High School Graduate</option>
                                                      <option value="Vocational" <?php if(!empty($filterdata['education'])){ $filterdata['education'] == 'Vocational' ? ' selected="selected"' : '';}?>>Vocational</option>
                                                      <option value="Undergraduate" <?php if(!empty($filterdata['education'])){ $filterdata['education'] == 'Undergraduate' ? ' selected="selected"' : '';}?>>Undergraduate</option>
                                                      <option value="Associate Degree" <?php if(!empty($filterdata['education'])){ $filterdata['education'] == 'Associate Degree' ? ' selected="selected"' : '';}?>>Associate Degree</option>
                                                      <option value="College Graduate" <?php if(!empty($filterdata['education'])){ $filterdata['education'] == 'College Graduate' ? ' selected="selected"' : '';}?>>College Graduate</option>
                                                      <option value="Post Graduate" <?php if(!empty($filterdata['education'])){ $filterdata['education'] == 'Post Graduate' ? ' selected="selected"' : '';}?>>Post Graduate</option>
                                             
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="widget">
                                    <h3 class="sb-title open">Experince</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox">
                                          <select name="exp">
                                             <option value=""> Select </option>
                                             <option value="All Tenure" <?php if(!empty($filterdata)){if(!empty($filterdata['exp'])){if($filterdata['exp'] == "All Tenure"){echo "selected";}}}?>> All Tenure </option>
                                             <option value="less than 6 months" <?php if(!empty($filterdata)){if(!empty($filterdata['exp'])){if($filterdata['exp'] == "less than 6 months"){echo "selected";}}}?>> less than 6 months </option>
                                             <option value="6mo to 1 yr" <?php if(!empty($filterdata)){if(!empty($filterdata['exp'])){if($filterdata['exp'] == "6mo to 1 yr"){echo "selected";}}}?>> 6mo to 1 yr </option>
                                             <option value="1 yr to 2 yr" <?php if(!empty($filterdata)){if(!empty($filterdata['exp'])){if($filterdata['exp'] == "1 yr to 2 yr"){echo "selected";}}}?>> 1 yr to 2 yr </option>
                                             <option value="2yr to 3 yr" <?php if(!empty($filterdata)){if(!empty($filterdata['exp'])){if($filterdata['exp'] == "2yr to 3 yr"){echo "selected";}}}?>> 2yr to 3 yr </option>
                                             <option value="3yr and up" <?php if(!empty($filterdata)){if(!empty($filterdata['exp'])){if($filterdata['exp'] == "3yr and up"){echo "selected";}}}?>>3yr and up </option>
                                             
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="widget">
                                    <h3 class="sb-title open">Language</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox">
                                          <select name="lang">
                                             <option value=""> Select </option>
                                             <?php
                                                foreach($languages as $language) {
                                              ?>
                                             <option value="<?php echo $language['id'];?>" <?php if(!empty($filterdata)){if(!empty($filterdata['lang'])){if($filterdata['lang'] == $language['id']){echo "selected";}}} ?>> <?php echo $language['name'];?> </option>
                                             <?php
                                                }
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="widget">
                                    <h3 class="sb-title open">Clients Supported</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox">
                                          <select name="clients">
                                             <option value=""> Select </option>
                                             <?php
                                                foreach($clients as $client) {
                                                ?>
                                             <option value="<?php echo $client['clients'];?>" <?php if(!empty($filterdata)){if(!empty($filterdata['clients'])){if($filterdata['clients'] == $client['clients']){echo "selected";}}}?>> <?php echo $client['clients'];?> </option>
                                             <?php
                                                }
                                                ?>
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <div class="widget">
                                    <h3 class="sb-title open">Industry Expertise</h3>
                                    <div class="specialism_widget">
                                       <div class="simple-checkbox ">
                                          <select name="industry">
                                             <option value=""> Select </option>
                                             <option value="81" <?php if(!empty($filterdata)){if($filterdata['industry']){if($filterdata['industry'] == "81"){echo "selected";}}} ?>> BPO - IT </option>
                                             <option value="83" <?php if(!empty($filterdata)){if($filterdata['industry']){if($filterdata['industry'] == "83"){echo "selected";}}} ?>> Restaurants </option>
                                             <option value="82" <?php if(!empty($filterdata)){if($filterdata['industry']){if($filterdata['industry'] == "82"){echo "selected";}}} ?>> Hotels </option>
                                             
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      </section>
      <?php include_once('footer.php'); ?>
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
         <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="formmidaress modpassfull">
                     <div class="filldetails">
                        <form>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" placeholder="Change Password">
                              <img src="images/keypass.png">
                           </div>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" placeholder="New Password">
                              <img src="images/keypass.png">
                           </div>
                           <div class="forminputspswd">
                              <input type="password" class="form-control" placeholder="Confirm New Password">
                              <img src="images/keypass.png">
                           </div>
                           <button type="submit" class="srchbtns">Change</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <script type="text/javascript">
            $("#category").change(function(){
             //get category value
             var cat_val = $("#category").val();
             //alert(cat_val);
             // put your ajax url here to fetch subcategory
             var url             =   '<?php echo base_url(); ?>/recruiter/Jobpost/fetchSubcategory';
             // call subcategory ajax here 
             $.ajax({
                            type:"POST",
                            url:url,
                            data:{
                                cat_val : cat_val
                            },
         
                            success:function(data)
         
                             {
                              
                           $("#subcategory").html(data);
                        
                             }
                         });
         });
      </script>
   </body>
</html>

