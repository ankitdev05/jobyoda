<?php $userSession = $this->session->userdata('userSession');?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>JobYoDA</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="description" content="">
      <meta name="keywords" content="">
      <meta name="author" content="CreativeLayers">
      <!-- Styles -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap-grid.css" />
      <link rel="icon" href="<?php echo base_url().'recruiterfiles/';?>images/fav.png" type="image/png" sizes="16x16">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/icons.css">
      <link rel="stylesheet" href="<?php echo base_url().'recruiterfiles/';?>css/animate.min.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/all.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/fontawesome.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/responsive.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/chosen.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/colors/colors.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/bootstrap.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/style.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/croppie.css" />
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'recruiterfiles/';?>css/jquery.timepicker.min.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
   </head>
   <style>
/*.modal-dialog {
    width: 30%!important;
}*/
      a:hover {
      text-decoration: none;
      }
      .tech i.fa.fa-ellipsis-h {
      float: right;
      }
      .progr span.progress-bar-tooltip {
      position: initial;
      color: #fff;
      }
      .manage-jobs-sec {
      float: none;
      width: 100%;
      margin: 0 auto;
      }

      .app {display: block;width: 70%;float: left;}

.app2 {
    float: left;
    display: block;
    width: 70%;
}

.whats span {
    margin: 0px;
    padding: 7px 0px;
    width: 100%;
    float: left;
}

.whats input[type="checkbox"] {
    opacity: 1;
    float: right;
    position: relative;
    z-index: 0;
}

.whats label {
    float: left;
}
      .link a {
      background: #26ae61;
      color: #fff;
      word-break: keep-all;
      font-size: 11px;
      line-height: 0;
      padding: 10px;
      }


      .link {
      float: left;
      width: 100px;
      }
      .btn-extars{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .tree_widget-sec > ul > li.inner-child.active > a {
      color: #26ae61;
      }
      .tree_widget-sec > ul > li.inner-child.active > a i {
      color: #26ae61;}
      .contact-edit .srch-lctn:hover {
      background: #26ae61;
      color: #ffffff;
      border-color: #26ae61;
      }
      .contact-edit .srch-lctn {
      color: #26ae61;
      border: 2px solid #26ae61;
      border: 2px solid #26ae61;
      }
      .contact-edit > form button{
      border: 2px solid #26ae61;}
      .profile-form-edit > form button:hover, .contact-edit > form button:hover {
      background: #26ae61;
      color: #ffffff;
      }
      .step.active i {
      background: #26ae61;
      border-color: #26ae61;
      color: #ffffff;
      }
      .step i{
      color: #26ae61;}
      .step.active span {
      color: #000;
      }
      .menu-sec nav > ul > li.menu-item-has-children > a::before{
      display:none;}
      .inner-header {
      float: left;
      width: 100%;
      position: relative;
      padding-top: 60px; padding-bottom: 15px;
      z-index: 0;
      }
      .manage-jobs-sec > table thead tr td{
      color: #26ae61;}
      .extra-job-info > span i {
      float: left;
      font-size: 30px;
      color: #26ae61;
      width: 30px;
      margin-right: 12px;
      }
      .action_job > li span {
      background: #26ae61;}
      .action_job > li span::before{
      background: #26ae61;}
      .action_job > li {
      float: left;
      margin: 0;
      position: relative;
      width: 15px;
      }
      .manage-jobs-sec > h3 {
      padding-left: 0px; 
      margin-top: 40px;
      text-align: center;
      }
      .fall {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 23px 10px;
      }
      .progress-bar{
      background-color: #47b476;
      }
      .details {
      float: left;
      width: 100%;
      padding: 5px 17px;
      background-color: #47b476;
      color: #fff;
      margin: 15px;
      border-radius: 20px
      }
      .leftdetails {
      float: left;
      width: 100%;
      }
      .left {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px;
      }
      .detail span {
      display: block;
      float: right;
      margin: 12px 8px;  
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detail strong {
      float: left;
      width: 100%;
      margin: 0px;
      padding: 0px 55px;
      font-weight: 700;
      font-size: 20px;
      }
      .detail, .detai {
      float: left;
      width: 100%;
      }
      .detai span {
      display: -webkit-box;
      width: 100%;
      font-weight: 600;
      margin: 0px;
      padding: 12px 0px;
      font-size: 20px;
      }
      .progras {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      }
      .detai strong {
      float: right;
      font-size: 20px;
      }
      .detail p, .detai p {
      font-weight: 600;
      font-size: 16px;
      color: #000;
      }
      .detail .progress span {
      position: absolute;
      right: 0px;
      font-size: 13px;
      color: #fff;
      border: 0px solid#ddd;
      }
      .head {
      float: left;
      width: 100%;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .num {
      float: left;
      margin: 20px 0px;
      }
      .para {
      float: left;
      width: 100%;
      border: 1px solid#dddd;
      padding: 10px;
      background-color: #47b476;
      color: #fff;
      margin-top: 10px;
      margin-bottom: 10px;
      }
      .tech {
      float: left;
      width: 100%;
      border: 1px solid#ddd;
      padding: 10px;
      margin: 10px 0px;
      }
      .search-container {
      float: left;
      }
      .search button {
      padding: 7px 4px;
      color: #fff;
      background-color: #47b476;
      border: none;
      width: 36%;
      }
      .upload a {
      color: #ffffff;
      text-decoration: none;
      background: #47b476;
      padding: 10px;
      border-radius: 50px;
      }
      .search input[type="text"] {
      padding: 9px 6px;
      width: 63%;
      background-color: #fff;
      border: 1px solid#ddd;
      }
      .details i.fa.fa-filter {
      float: right;
      margin: 5px 0px;
      padding: 0px;
      }
      .details p {
      float: left;
      margin: 0px;
      padding: 0px;
      color: #fff;
      }
      .head p {
      color: #fff;
      }
      .para p {
      color: #fff;
      }
      .upload {
      float: right;
      padding: 8px;
      margin: px;
      }
      border: 1px solid#ddd;
      padding: 10px;
      }
      .dot {
      float: right;
      }
      .progr {
      float: left;
      width: 100%;
      margin-bottom: 18px;
      }
      .progr .progress {
      position: absolute;
      left: 9%;
      height: 20px;
      -webkit-border-radius: 8px;
      -moz-border-radius: 8px;
      -ms-border-radius: 8px;
      -o-border-radius: 8px;
      border-radius: 8px;
      top: initial;
      width: 80%;
      }
      .progr .progress-label {
      margin: 12px 0px;
      }
      .progress-bar.progress-bar-primary {
      padding: 0px;
      }
      .texts p {
      position: absolute;
      top: 70%;
      left: 29%;
      }
      p.dropdown-toggle {
      font-size: 34px;
      margin: -30px;
      }
      .dot {
      float: right;
      }
      .texts {
      margin-top: 20px;
      }
      .text p {
      margin: 0px;
      padding: 0px;
      font-size: 13px;
      }
      .right .progress{
      width: 150px;
      height: 150px;
      line-height: 150px;
      background: none;
      margin: 0 auto;
      box-shadow: none;
      position: relative;
      }
      .right .progress:after{
      content: "";
      width: 100%;
      height: 100%;
      border-radius: 50%;
      border: 2px solid #fff;
      position: absolute;
      top: 0;
      left: 0;
      }
      .right .progress > span{
      width: 50%;
      height: 100%;
      overflow: hidden;
      position: absolute;
      top: 0;
      z-index: 1;
      }
      .progress .progress-left{
      left: 0;
      }
      .right .progress .progress-bar{
      width: 100%;
      height: 100%;
      background: none;
      border-width: 2px;
      border-style: solid;
      position: absolute;
      top: 0;
      }
      .right .progress .progress-left .progress-bar{
      left: 100%;
      border-top-right-radius: 80px;
      border-bottom-right-radius: 80px; 
      border-left: 0;
      -webkit-transform-origin: center left;
      transform-origin: center left;
      }
      .right .progress .progress-right{
      right: 0;
      }
      .right .progress .progress-right .progress-bar{
      left: -100%;
      border-top-left-radius: 80px;
      border-bottom-left-radius: 80px;
      border-right: 0;
      -webkit-transform-origin: center right;
      transform-origin: center right;
      animation: loading-1 1.8s linear forwards;
      }
      .right .progress .progress-value{
      width: 59%;
      height: 59%;
      border-radius: 50%;
      border: 2px solid #ebebeb;
      font-size: 28px;
      line-height: 82px;
      text-align: center;
      position: absolute;
      top: 7.5%;
      left: 7.5%;
      }
      .right .progress.blue .progress-bar{
      border-color: #049dff;
      }
      .right .progress.blue .progress-value{
      color: #47b476;
      }
      .right .progress.blue .progress-left .progress-bar{
      animation: loading-2 1.5s linear forwards 1.8s;
      }
      .progress.yellow .progress-bar{
      border-color: #fdba04;
      }
      .right .progress.yellow .progress-value{
      color: #fdba04;
      }
      .right .progress.yellow .progress-left .progress-bar{
      animation: loading-3 1s linear forwards 1.8s;
      }
      .right .progress.pink .progress-bar{
      border-color: #ed687c;
      }
      .right .progress.pink .progress-value{
      color: #ed687c;
      }
      .right .progress.pink .progress-left .progress-bar{
      animation: loading-4 0.4s linear forwards 1.8s;
      }
      .right .progress.green .progress-bar{
      border-color: #1abc9c;
      }
      .right .progress.green .progress-value{
      color: #1abc9c;
      }
      .right .progress.green .progress-left .progress-bar{
      animation: loading-5 1.2s linear forwards 1.8s;
      }


      @keyframes loading-1{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(180deg);
      transform: rotate(180deg);
      }
      }
      @keyframes loading-2{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(144deg);
      transform: rotate(144deg);
      }
      }
      @keyframes loading-3{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(90deg);
      transform: rotate(90deg);
      }
      }
      @keyframes loading-4{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(36deg);
      transform: rotate(36deg);
      }
      }
      @keyframes loading-5{
      0%{
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
      }
      100%{
      -webkit-transform: rotate(126deg);
      transform: rotate(126deg);
      }
      }
      @media only screen and (max-width: 990px){
      .right .progress{ margin-bottom: 20px; }
      }
      .filterchekers input[type="checkbox"]{
                position: absolute;
                opacity: 0;
                z-index: auto;
                margin: 0;
                width: 90px;
                height: 55px;
      }
      .filldetails select.form-control {
          outline:none;
      border-radius:0px;
      }
      .validErr{
          color:red;
      }
      
      table#myTable {
         width: 100%;
         }
         table#myTable td {
         padding: 0 9px 0 0;
         }
      .addmre {
         margin: 0;
         background: #27aa60;
         float: left;
         color: #fff;
         padding: 3px 8px;
         margin-bottom: 20px;
         cursor: pointer;
         float: right;
         }
         
         .rightssoly {
    border: 1px solid #ddd;
    float: left;
    width: 100%;
    min-height: 173px;
    margin-bottom: 20px;
}
        
        .rightssoly button {
    color: red;
    padding:10px;
}
  .filterchekers1 input[type="radio"]{
                position: absolute;
                opacity: 0;
                z-index: auto;
                margin: 0;
                width: 100%;
                height: 55px;
      }    
      a.remove i.fa.fa-trash {
color: #fff;
}
input.form-control {
    margin: 20px 0px;
}
a.remove{
padding: 10px 8px;
text-align:center;;
background: #26ae61;

border-radius: 100px;

width: 5%;

float: left;
 }
input.form-control.addinput {
    width: 80%;
    float: left;
    margin-right: 33px;
}
.savingbutton{padding: 17px;
    width: 300px;    margin-top: 40px;}
  
  .psthbs .companyprofileforms.fullwidthform .filldetails.flxdtls {
    width: 100%;
    display: flex;
}

.filldetails.flxdtls .stepcountfrms {
    margin: 0 12px;
}

.dvdhft {
    display: flex;
}

.companyprofileforms.fullwidthform .filldetails .dvdhft input:first-child {
    margin: 10px 18px 10px 0px;
}

.companyprofileforms.fullwidthform .filldetails .dvdhft input:last-child {
    margin: 10px 15px 10px 0px;
}

.filldetails .form-control:focus {
    box-shadow:none;
}

.companyprofileforms.fullwidthform .filldetails .dvdhft.slinp input:last-child{
      margin: 10px 0px 14px 0px;
}

.GalleryBox{
    height: 110px;
    border: 1px solid #000;
    position: relative;
    margin: 0 0 15px
}
.GalleryBox img{
    width: 100%;
    height: 100%;
}
.GalleryBox button{
    border: 0px;
    background: none;
    float:right;
    position: absolute;
    top: -15px;
    right: -15px;
    margin: 0;
    width: 30px !important;
    height: 30px !important;
    background-color: #c31111;
    border-radius: 50%;
    padding: 0 !important;
    font-size: 13px !important;
}
#getlengthtext {
        position: relative!important;
        float: right!important;
        bottom: 10px!important;
        color: #27ae62!important;
    }
   </style>
   <body>
      <div class="theme-layout" id="scrollup">
         <header class="stick-top nohdascr">
            <div class="menu-sec">
               <div class="container-fluid dasboardareas">
                  
                  <!-- Logo -->
                    <?php include_once('headermenu.php');?>
                  <!-- Menus -->
               </div>
            </div>
         </header>
         <section>
            <div class="block no-padding">
               <div class="container-fluid dasboardareas">
                  <div class="row">
                     <aside class="col-md-3 col-lg-2 column border-right Sidenavbar">
                        <div class="widget">
                           <div class="menuinside">
                              <?php include_once('sidebar.php'); ?>
                           </div>
                        </div>
                     </aside>
                     <div class="col-md-9 col-lg-10 MainWrapper">
                        <div class="row">
                           <div class="col-md-12 psthbs">
              
                              <div class="companyprofileforms fullwidthform managecompanybch mgsirt">
                
                 <div class="myhdainside">
                                      <h6 style="text-align: center;">Edit Site Details</h6>
                                    </div>
                  
                                 <form method="post" action="<?php echo base_url();?>recruiter/recruiter/siteupdate" enctype="multipart/form-data">
                 <div class="mailayer">
                  <div class="camico">
                  <i class="fas fa-camera"></i>
                                        
                     </div>
                                    <div class="uploadareas">
                                      <input type="hidden" name="rid" value="<?php if(!empty($_GET['view'])){ echo base64_decode($_GET['view']); }?>">
                                       <input type="file" name="profilePic" id="imgInp" accept="image/x-png,image/jpeg,image/jpg">
                                       <!-- <input type="hidden" name="cropimg" id="cropimg" value=""> -->
                                       <?php if($companyDetails[0]['companyPic']){ ?>
                                            <img id="blah" src="<?php echo $companyDetails[0]['companyPic'] ?>" >
                                       <?php } else{?>
                                             <img id="blah" src="<?php echo base_url() ?>recruiterfiles/images/camera1.png" >
                                             <?php }?>
                                    </div>
                  </div>
                   <div class="MandatoryBox">
                                      <p><sup>*</sup> Mandatory site logo (Image size within 500X280 - 600X340)</p>

                                      <p id="invalidfileizeee" style="color:red;"> <?php if(!empty($errors['profilePic'])){ echo "<span class='err'>".$errors['profilePic']."</span>";}?> </p>
                                    </div>
                                   
                                    <div>
                                        <?php if(!empty($errors)){
                                        //var_dump($errors);
                                        }?>
                                    </div>
                                    <div class="filldetails flxdtls">
                                     <div class="stepcountfrms">
                                           <div class="headsteps-gt">
                                             <h5>Site Information</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
                      
                                               <input type="text" class="form-control" name="site_name" value="<?php if($companie[0]['cname']){echo $companie[0]['cname'];} ?>" placeholder="Site Name">
                                                <?php if(!empty($errors['site_name'])){ ?><span class="validErr"><?= $errors['site_name'];?></span><?php }?>
                                         
                                           <select name="region" class="form-control">
                                                  <option value=""> Select Region</option>
                                                <?php
                                                  foreach($regions as $region) {
                                                ?>
                                                    <option value="<?php echo $region['id'];?>" <?php if($region['id']==$companyDetails[0]['region']){ echo "selected"; }?>> <?php echo $region['region'];?>  </option>
                                                <?php
                                                  }
                                                ?>
                                               </select>
                                           
                                                <input type="text" id="txtPlaces" class="form-control" name="address" value="<?php if(!empty($companyDetails[0]['address'])){echo $companyDetails[0]['address'];} ?>" placeholder="Address">
                                                <?php if(!empty($errors['address'])){ ?><span class="validErr"><?= $errors['address'];?></span><?php }?>
                                        
                                            
                                             
                                           </div>
                       
                       
                       <div class="stepcountfrms">
                                          <div class="headsteps-gt">
                                             <h5>Recruitment Hours of Operation</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
                                               
                                            
                                            <div class="dvdhft">
                                            
                                               <select name="dayfrom" class="form-control">
                                                    <option value=""> Select Day From</option>
                                                    <option value="1" <?php if(!empty($companyDetails[0]['dayfrom'])){ if($companyDetails[0]['dayfrom'] == 1){echo "selected"; }}?>> Monday  </option>
                                                    <option value="2" <?php if(!empty($companyDetails[0]['dayfrom'])){ if($companyDetails[0]['dayfrom'] == 2){echo "selected"; }}?>> Tuesday  </option>
                                                    <option value="3" <?php if(!empty($companyDetails[0]['dayfrom'])){ if($companyDetails[0]['dayfrom'] == 3){echo "selected"; }}?>> Wednesday  </option>
                                                    <option value="4" <?php if(!empty($companyDetails[0]['dayfrom'])){ if($companyDetails[0]['dayfrom'] == 4){echo "selected"; }}?>> Thursday  </option>
                                                    <option value="5" <?php if(!empty($companyDetails[0]['dayfrom'])){ if($companyDetails[0]['dayfrom'] == 5){echo "selected"; }}?>> Friday  </option>
                                                    <option value="6" <?php if(!empty($companyDetails[0]['dayfrom'])){ if($companyDetails[0]['dayfrom'] == 6){echo "selected"; }}?>> Saturday  </option>
                                                    <option value="7" <?php if(!empty($companyDetails[0]['dayfrom'])){ if($companyDetails[0]['dayfrom'] == 7){echo "selected"; }}?>> Sunday  </option>
                                               </select>
                                          
                                           
                                               <select name="dayto" class="form-control">
                                                  <option value=""> Select Day To</option>
                                                    <option value="1" <?php if(!empty($companyDetails[0]['dayto'])){ if($companyDetails[0]['dayto'] == 1){echo "selected"; }}?>> Monday  </option>
                                                    <option value="2" <?php if(!empty($companyDetails[0]['dayto'])){ if($companyDetails[0]['dayto'] == 2){echo "selected"; }}?>> Tuesday  </option>
                                                    <option value="3" <?php if(!empty($companyDetails[0]['dayto'])){ if($companyDetails[0]['dayto'] == 3){echo "selected"; }}?>> Wednesday  </option>
                                                    <option value="4" <?php if(!empty($companyDetails[0]['dayto'])){ if($companyDetails[0]['dayto'] == 4){echo "selected"; }}?>> Thursday  </option>
                                                    <option value="5" <?php if(!empty($companyDetails[0]['dayto'])){ if($companyDetails[0]['dayto'] == 5){echo "selected"; }}?>> Friday  </option>
                                                    <option value="6" <?php if(!empty($companyDetails[0]['dayto'])){ if($companyDetails[0]['dayto'] == 6){echo "selected"; }}?>> Saturday  </option>
                                                    <option value="7" <?php if(!empty($companyDetails[0]['dayto'])){ if($companyDetails[0]['dayto'] == 7){echo "selected"; }}?>> Sunday  </option>
                                               </select>
                                       
                                            </div>
                                           
                        <div class="dvdhft">
                            <input type="text" class="form-control" name="from_time" id="from_time" value="<?php if(!empty($companyDetails[0]['from_time'])){ echo $companyDetails[0]['from_time']; }?>" placeholder="From Time">
                            <?php if(!empty($errors['from_time'])){ ?><span class="validErr"><?= $errors['from_time'];?></span><?php }?>
                        
                            <input type="text" class="form-control" name="to_time" id="to_time" value="<?php if(!empty($companyDetails[0]['to_time'])){ echo $companyDetails[0]['to_time']; }?>" placeholder="To Time">
                            <?php if(!empty($errors['to_time'])){ ?><span class="validErr"><?= $errors['to_time'];?></span><?php }?>
                        </div>
                      
                    </div>
                      
                                            </div>
                      
                    
                      
                      <!-- <div class="filldetails flxdtls">
                                     <div class="stepcountfrms"> 
                                            <div class="headsteps-gt">
                                             <h5>Site Contact Info</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
                      
                       <div class="dvdhft slinp">
                       <select name="phonecode" class="form-control">
                                                  <option value=""> Select Phone Code</option>
                                                <?php
                                                  foreach($phonecodes as $phonecode) {
                                                ?>
                                                    <option value="<?php echo $phonecode['phonecode'];?>" <?php if($phonecode['phonecode']==$companyDetails[0]['phonecode']){ echo "selected"; }?>> <?php echo $phonecode['name'];?> - <?php echo $phonecode['phonecode'];?> </option>
                                                <?php
                                                  }
                                                ?>
                                               </select>
                         
                       <input type="text" class="form-control" name="phone" value="<?php if(!empty($companyDetails[0]['phone'])){echo $companyDetails[0]['phone'];} ?>" placeholder="Phone Number">
                                                <?php if(!empty($errors['phone'])){ ?><span class="validErr"><?= $errors['phone'];?></span><?php }?>
                        
                        
                         
                                             </div>   
                                           
                                              <div class="dvdhft slinp"> 
                                               <select name="rphonecode" class="form-control">
                                                  <option value=""> Select Phone Code</option>
                                                <?php
                                                  foreach($phonecodes as $phonecode) {
                                                ?>
                                                    <option value="<?php echo $phonecode['phonecode'];?>" <?php if($phonecode['phonecode']==$companyDetails[0]['rphonecode']){ echo "selected"; }?>> <?php echo $phonecode['name'];?> - <?php echo $phonecode['phonecode'];?> </option>
                                                <?php
                                                  }
                                                ?>
                                               </select>
                                            
                                            
                                                <input type="text" class="form-control" name="recruiter_contact" id="recruiter_contact" value="<?php if(!empty($companyDetails[0]['recruiter_contact'])){ echo $companyDetails[0]['recruiter_contact']; }?>" placeholder="Recruiter Contact No">
                                                <?php if(!empty($errors['recruiter_contact'])){ ?><span class="validErr"><?= $errors['recruiter_contact'];?></span><?php }?>
                                           </div>
                       
                       
                                            
                                           
                                                <input type="text" class="form-control" name="recruiter_email" id="recruiter_email" value="<?php if(!empty($companyDetails[0]['recruiter_email'])){ echo $companyDetails[0]['recruiter_email']; }?>" placeholder="Recruiter Email">
                                                <?php if(!empty($errors['recruiter_email'])){ ?><span class="validErr"><?= $errors['recruiter_email'];?></span><?php }?>
                                           
                                            
                                          </div> 
                      
                       <div class="stepcountfrms">
                      <div class="headsteps-gt">
                                             <h5>Your Preferred Communication Channel</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
                      
                                            <div class="col-md-9 col-xs-12 whats">
                                               
                                               <div class="app">
                                               <label>Whatsapp</label>
   
                                            <input type="checkbox" name="comm_channel[]" class="comm" value="1" <?php if(!empty($companyDetails[0]['comm_channel']) && $companyDetails[0]['comm_channel']=='1'){ echo "checked" ;} ?>>
                                                 </div>
                                                 <div class="app2">
                                               <label>Viber</label>
                                               <input type="checkbox" name="comm_channel[]" class="comm" value="2" <?php if(!empty($companyDetails[0]['comm_channel']) && $companyDetails[0]['comm_channel']=='2'){ echo "checked" ;} ?>>
                                               </div>
                                            </div>
                                            </div>
                      
                      </div> -->
                                            <!-- <div class="col-md-3 col-xs-12">
                                                <select name="industry">
                                                   <option value=""> Select Industry </option>
                                                   <?php
                                                        foreach($industryLists as $industryList) {
                                                   ?>
                                                         <option <?php if($industryList['id']==$companyDetails[0]['industry']){ echo "selected"; }?> value="<?php echo $industryList['id'];?>"><?php echo $industryList['name'];?></option>
                                                   <?php }?>
                                                </select>
                                            </div> -->
                      
                       <div class="filldetails flxdtls">
                       <div class="stepcountfrms"> 
                        <div class="headsteps-gt">
                                             <h5>Site Overview</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
                      
                            <textarea id="lengthtext" name="compDesc" placeholder="Job Description" maxlength="1000"><?php if(!empty($companyDetails[0]['companyDesc'])){ echo $companyDetails[0]['companyDesc']; }?></textarea>
                            <span style="color:#27ae62;font-size: 11px;bottom: 14px;position:relative;">Maximum characters limit: 1000 </span>
                      <span id="getlengthtext">  </span>
                      </div>
                      </div>
                                            
                                   
                                   
                                    

                              </div>
                              <div style="clear:both;"></div>
                
                           <div class="filldetails">
                              <div class="stepcountfrms"> 
                                 <div class="headsteps-gt">
                                    <h5>Site Gallery</h5>
                                    <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                 </div>
                                 <div class="row"> 
                                    <?php if(!empty($companyImg)){ foreach($companyImg as $siteimg){ 
                                       $siteimage = $siteimg['pic'] ;
                                    ?>
                                       <div class="col-md-2">
                                          <div class="GalleryBox">
                                             <input type="hidden" value="<?php echo $siteimg['id']; ?>" required>
                                             <button type="button" onClick="deleteImg('<?= $siteimg['id'] ?>')"> <i class="fa fa-trash"></i> </button>
                                             <img src="<?php echo $siteimage;?>" style="width:100%;">
                                          </div>                                          
                                       </div>
                                    <?php }}?>                                        
                                 </div>

                                 <?php if(count($companyImg)<5){ ?>
                                 <table id="myTable">
                                          <tr>
                                             
                                             <td class="addtd"><input type="file" id="jobfile" name="site_image[]" class="form-control siteimg" ><BR>
                                             <span style="font-size: 13px;"><span style="color:red;">*</span> Image dimension should be within 400X300 - 450X350</span><BR>
                                             <span style="color:red;font-size:14px;" id="invalidfileize"></span><BR>
                                             </td>
                                          </tr>
                                        </table>
                                        <p onClick="myFunction()" class="addmre">Add More</p>
                                  <?php } ?>
                                    <!-- <?php //if(!empty($companyImg)){ if(count($companyImg)<5){ ?>
                                        <table id="myTable">
                                        <tr>
                                           
                                           <td><input type="file" name="site_image11[]" class="form-control" ></td>
                                        </tr>
                                      </table>
                                      <p  class="addmre">Add More</p> 
                                      <?php  
                                          //}} else{
                                       ?>
                                          <table id="myTable">
                                        <tr>
                                           
                                           <td><input type="file" name="site_image11[]" class="form-control" ></td>
                                        </tr>
                                      </table>
                                      <p onClick="myFunction()" class="addmre">Add More</p> 
                                       <?php
                                          //}
                                      ?> -->
                                 </div>
                              </div>
                  
                 <div class="filldetails">
                <div class="stepcountfrms"> 
                <div class="headsteps-gt">
                                             <h5>Site-Specific Benefits</h5>
                                             <img src="<?php echo base_url(); ?>recruiterfiles/images/fav.png">
                                          </div>
                      
                              <div class="newfiletrsgree">
                                  
                                 <div class="main-hda right">
                                    <h3>Top Picks</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <!-- <li class="<?php ///if($topPicks2){if(in_array(1, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic1" type="checkbox" name="toppicks[]" value="1" <?php //if($topPicks2){ if(in_array(1, $topPicks2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <p class="txtonly">Bonus</p>
                                          <p> Joining<br>Bonus</p>
                                       </li> -->
                                       
                                       <li class="<?php if($topPicks2){if(in_array(2, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic2" type="checkbox" name="toppicks[]" value="2" <?php if($topPicks2){ if(in_array(2, $topPicks2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-food.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-food-1.png" class="hvrsicos">
                                          <p> Free <br>Food</p>
                                       </li>

                                       <li class="<?php if($topPicks2){if(in_array(3, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic3" type="checkbox" name="toppicks[]" value="3" <?php if($topPicks2){ if(in_array(3, $topPicks2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                         <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-white.png" class="hvrsicos">
                                          <p>Day 1 HMO</p>
                                       </li>

                                       <li class="<?php if($topPicks2){if(in_array(4, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic4" type="checkbox" name="toppicks[]" value="4" <?php if($topPicks2){ if(in_array(4, $topPicks2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-for-depended.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/day-1-hmo-for-depended-1.png" class="hvrsicos">
                                          <p> Day 1 HMO<br> for Dependent</p>
                                       </li>

                                       <!-- <li class="<?php //if($topPicks2){if(in_array(5, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic5" type="checkbox" name="toppicks[]" value="5" <?php //if($topPicks2){ if(in_array(5, $topPicks2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <i class="fas fa-sun"></i>
                                          <p>Day Shift</p>
                                       </li> -->

                                       <li class="<?php if($topPicks2){if(in_array(6, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic6" type="checkbox" name="toppicks[]" value="6" <?php if($topPicks2){ if(in_array(6, $topPicks2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/14-month-pay.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/14-month-pay-1.png" class="hvrsicos">
                                          <p> 14th Month Pay</p>
                                       </li>

                                       <li class="<?php if($topPicks2){if(in_array(7, $topPicks2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="toppic7" type="checkbox" name="toppicks[]" value="7" <?php if($topPicks2){ if(in_array(7, $topPicks2)){ echo "checked";}}?>>
                                          
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/wfh.png" class="normicos">  
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/wfhun.png" class="hvrsicos">
                                          <p> Work From Home </p>
                                       </li>

                                    </ul>
                                 </div>
                                 
                              </div>
                              <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Allowances and Incentives</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li class="tapsct <?php if($allowance2){if(in_array(1, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct1" data-value="1">
                                          <input class="checked_value" id="allow1" type="checkbox" name="allowances[]" value="1" <?php if($allowance2){ if(in_array(1, $allowance2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/cell-phone-allowance.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/cell-phone-allowance-1.png" class="hvrsicos"> 
                                          <p> Cellphone <br>Allowance</p>
                                       </li>

                                       <li class="tapsct <?php if($allowance2){if(in_array(2, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct2" data-value="2">
                                          <input class="checked_value" id="allow2" type="checkbox" name="allowances[]" value="2" <?php if($allowance2){ if(in_array(2, $allowance2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-parking.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-parking-1.png" class="hvrsicos">  
                                          <p>Free <br>Parking</p>
                                       </li>

                                       <li class="tapsct <?php if($allowance2){if(in_array(3, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct3" data-value="3">
                                          <input class="checked_value" id="allow3" type="checkbox" name="allowances[]" value="3" <?php if($allowance2){ if(in_array(3, $allowance2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-shuttle.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-shuttle-1.png" class="hvrsicos">  
                                          <p> Free <br> Shuttle</p>
                                       </li>

                                       <li class="tapsct <?php if($allowance2){if(in_array(4, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct4" data-value="4">
                                          <input class="checked_value" id="allow4" type="checkbox" name="allowances[]" value="4" <?php if($allowance2){ if(in_array(4, $allowance2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/annual--performance-bonus.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/annual--performance-bonus-1.png" class="hvrsicos">  
                                          <p> Annual <br> Performance Bonus</p>
                                       </li>

                                       <li class="tapsct <?php if($allowance2){if(in_array(5, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct5" data-value="5">
                                          <input class="checked_value" id="allow5" type="checkbox" name="allowances[]" value="5" <?php if($allowance2){ if(in_array(5, $allowance2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/reteirment-benifits.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/reteirments-benefits-1.png" class="hvrsicos">  
                                          <p> Retirements <br> Benefits</p>
                                       </li>

                                       <li class="tapsct <?php if($allowance2){if(in_array(6, $allowance2)){ echo "selectedgreen"; }}?>" id="tapsct6" data-value="6">
                                          <input class="checked_value" id="allow6" type="checkbox" name="allowances[]" value="6" <?php if($allowance2){ if(in_array(6, $allowance2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/transportation.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/transportation-1.png" class="hvrsicos">  
                                          <p> Transport <br> Allowance</p>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li class="tapsct <?php if($allowance2){if(in_array(7, $allowance2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="allow7" type="checkbox" name="allowances[]" value="7" <?php if($allowance2){ if(in_array(7, $allowance2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/monthly-performance-incentive.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/monthly-performance-incentive-1.png" class="hvrsicos"> 
                                          <p> Monthly Performance <br> Incentives</p>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Medical Benifits</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li class="<?php if($medical2){if(in_array(1, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" id="medical1" value="1" <?php if($medical2){ if(in_array(1, $medical2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-hmo-for-dependents.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/free-hmo-for-dependents-1.png" class="hvrsicos">  
                                          <p> Free HMO for<br>Dependents</p>
                                       </li>

                                       <li class="<?php if($medical2){if(in_array(2, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" id="medical2" value="2" <?php if($medical2){ if(in_array(2, $medical2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/critical-illness-benefits.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/critical-illness-benefits1.png" class="hvrsicos">
                                          <p> Critical Illness <br>Benefits</p>
                                       </li>

                                       <li class="<?php if($medical2){if(in_array(3, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" id="medical3" value="3" <?php if($medical2){ if(in_array(3, $medical2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/life-insurence.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/life-insurence-1.png" class="hvrsicos">
                                          <p>Life <br>Insurance</p>
                                       </li>

                                       <li class="<?php if($medical2){if(in_array(4, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" value="4" id="medical4" <?php if($medical2){ if(in_array(4, $medical2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/maternity-assistance.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/maternity-assistance-1.png" class="hvrsicos">
                                          <p> Maternity<br> Assistance</p>
                                       </li>

                                       <li class="<?php if($medical2){if(in_array(5, $medical2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" type="checkbox" name="medical[]" value="5" id="medical5" <?php if($medical2){ if(in_array(5, $medical2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                           <img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/medicine-reimbursemer.png" class="normicos">  
<img src="<?php echo base_url(); ?>recruiterfiles/images/toppics/medicine-reimbursemer-1.png" class="hvrsicos">
                                          <p>Medicine <br>Reimbursement</p>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              
                              <!-- <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Leaves</h3>
                                 </div>
                                 <div class="filterchekers">
                                    <ul>
                                       <li class="<?php if($leave2){if(in_array(1, $leave2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="leaves1" type="checkbox" name="leavs[]" value="1" <?php if($leave2){ if(in_array(1, $leave2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <i class="fas fa-snowflake"></i>
                                          <p> Weekend Off</p>
                                       </li>

                                       <li class="<?php if($leave2){if(in_array(2, $leave2)){ echo "selectedgreen"; }}?>">
                                          <input class="checked_value" id="leaves2" type="checkbox" name="leavs[]" value="2" <?php if($leave2){ if(in_array(2, $leave2)){ echo "checked";}}?> onClick="clickfuncheck(this.id)">
                                          <i class="fas fa-snowflake"></i>
                                          <p>Holiday Off</p>
                                       </li>
                                    </ul>
                                 </div>
                              </div> -->
                              <!-- <div class="newfiletrsgree">
                                 <div class="main-hda right">
                                    <h3>Work Shifts</h3>
                                 </div>
                                 <div class="filterchekers1">
                                    <ul>
                                        <?php //echo "<pre>";
                                        //print_r($workshift2);?>
                                       <li class="work-class <?php if(!empty($workshift2)){ if($workshift2[0]=="1"){ echo 'selectedgreen';}} ?>">
                                          <input class="work-radio" id="shifts1" type="radio" name="shifts[]" value="1" >
                                          <i class="fas fa-star"></i> 
                                          <p> Mid Shift </p>
                                       </li>

                                       <li class="work-class <?php if(!empty($workshift2)){ if($workshift2[0]=="2"){ echo 'selectedgreen';}} ?>">
                                          <input class="work-radio" id="shifts2" type="radio" name="shifts[]" value="2" >
                                          <i class="fas fa-moon-o"></i> 
                                          <p> Night Shift </p>
                                       </li>

                                       <li class="work-class <?php if(!empty($workshift2)){ if($workshift2[0]=="3"){ echo 'selectedgreen';}} ?>">
                                          <input class="work-radio" id="shifts3" type="radio" name="shifts[]" value="3">
                                          <p class="txtonly">24/7</p>
                                          <p>24/7 </p>
                                       </li>

                                       
                                    </ul>
                                 </div>
                                 
                              </div> -->
                
                
                              <div class="PostjobButton">
                                    <button type="submit" class="savingbutton">Save</button>
                              </div>
                
                </div>
                </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </div>
      <div id="uploadimageModal" class="modal bd-example-modal-lg" role="dialog">
   <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            
            </div>
            <div class="modal-body">
            <div class="row">
               <div class="col-md-12 text-center">
                    <div id="image_demo" style="margin-top:30px"></div>
               </div>
               <!-- <div class="col-md-4" style="padding-top:30px;">
                  
                    
               </div> -->
            </div>
            </div>
            <div class="modal-footer">
            <button class="btn btn-success crop_image">Crop & Upload Image</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
      </div>
    </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="benefitModal" role="dialog">
    <div class="modal-dialog rechout">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-body">
         <img src="<?php echo base_url(); ?>recruiterfiles/images/smallload.gif">
          <p class="success_msg">Please reach out to your group admin if you wish to remove this benefit</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
      </section>
<?php include_once("footer.php");?>
<?php include_once("modalpassword.php");?>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/modernizr.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/script.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/wow.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/slick.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/parallax.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/select-chosen.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/jquery.scrollbar.min.js" type="text/javascript"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'recruiterfiles/';?>js/recruiteryoda.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'recruiterfiles/';?>js/croppie.js"></script>
      <script type="text/javascript" src="<?php echo base_url().'recruiterfiles/';?>js/jquery.timepicker.min.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&sensor=false&libraries=places&callback=initMap"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.A;
            var longitude = place.geometry.location.F;
            var mesg = "Address: " + address;
            mesg += "\nLatitude: " + latitude;
            mesg += "\nLongitude: " + longitude;
        });
    });
    </script>

    <?php if($userSession['label']==='0'){ ?>
      <script>
      //alert('hi');
      $(".filterchekers li").click(function(){
        $(this).toggleClass("selectedgreen");  
      });
      </script>
    <?php }else{ ?>
      <script>
      //alert('hello');
      $(".filterchekers li").click(function(){
        $('#benefitModal').modal("show");  
      });
      </script>
    <?php } ?> 
   
   <script>
       $('#from_time').timepicker({ 'timeFormat': 'H:i' });
       $('#to_time').timepicker({ 'timeFormat': 'H:i' });
   </script>

   <script>

    function clickfuncheck(id) {
            var cid =  "#"+id;
            if (document.getElementById(id).checked) {
                $(cid).prop('checked', true);
            } else {
                $(cid).prop('checked', false);
            }
        }
   </script>

<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&sensor=false&libraries=places&callback=initMap"></script> -->
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
        google.maps.event.addListener(places, 'place_changed', function () {
            var place = places.getPlace();
            var address = place.formatted_address;
            var latitude = place.geometry.location.A;
            var longitude = place.geometry.location.F;
            var mesg = "Address: " + address;
            mesg += "\nLatitude: " + latitude;
            mesg += "\nLongitude: " + longitude;
        });
    });
    function readURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();
    
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
    
        reader.readAsDataURL(input.files[0]);
      }
    }
    
    $("#imgInp").change(function() {

        readURL(this);

        var fileUpload = document.getElementById("imgInp");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
        if (regex.test(fileUpload.value.toLowerCase())) {

           if (typeof (fileUpload.files) != "undefined") {

              //Initiate the FileReader object.
              var reader = new FileReader();
              //Read the contents of Image File.
              reader.readAsDataURL(fileUpload.files[0]);
              reader.onload = function (e) {
                  //Initiate the JavaScript Image object.
                  var image = new Image();
                  //Set the Base64 string return from FileReader as source.
                  image.src = e.target.result;
                  //Validate the File Height and Width.
                  image.onload = function () {
                      var height = parseInt(this.height);
                      var width = parseInt(this.width);
                      
                      if (width <= 600 && height <= 340) {

                        if(width >= 500 && height >= 280) {
                            $("#invalidfileizeee").html("");
                        
                        } else {
                            $("#invalidfileizeee").html("Image size must be within 500X280 - 600X340");
                            $('#imgInp').val('');
                            $('#blah').attr('src', "<?php echo base_url();?>recruiterfiles/images/camera1.png");
                        }
                      } else {
                          $("#invalidfileizeee").html("Image size must be within 500X280 - 600X340");
                          $('#imgInp').val('');
                          $('#blah').attr('src', "<?php echo base_url();?>recruiterfiles/images/camera1.png");
                      }
                  };
              }
           } else {
              $("#invalidfileizeee").html("Please select a valid Image file.");
              $('#imgInp').val('');
              $('#blah').attr('src', "<?php echo base_url();?>recruiterfiles/images/camera1.png");
           }
        } else {
           $("#invalidfileizeee").html("Please select a valid Image file.");
           $('#imgInp').val('');
           $('#blah').attr('src', "<?php echo base_url();?>recruiterfiles/images/camera1.png");
        }
    });
    
    function deleteImg(id){
        $.ajax({
           'type':'POST',
           'data':'id='+id,
           'url' :"<?= base_url('recruiter/recruiter/deleteimg')?>",
            'success':function(res){
                //console.log(res);
                setTimeout(function(){// wait for 5 secs(2)
                       location.reload(); // then reload the page.(3)
                  }, 1000); 
            }           
           
        });
    }
</script>

<script>
     /*function myFunction() {
       var count = '<?php echo count($companyImg); ?>';
       //alert(count);
       var table = document.getElementById("myTable");
       var rows = document.getElementById("myTable").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
       var limit = <?php count($companyImg); ?> - 4;
        if(rows<(5-count))
        {
           var row = table.insertRow(rows);
           var cell1 = row.insertCell(0);
              
           cell1.innerHTML = "<input type='file' name='site_image11[]' class='form-control addinput' ><a href='javascript:void(0);' class='remove'><i class='fa fa-trash'></i></a>";
        }
        else{
            $('.addmre').hide();
        }
       
       
     }
*/
// $(document).ready(function(){
//   $(".addmre").click(function(){
//     $(".enteitm").append("<li><input type='file' onchange='functionimg"+rows+"()' id='jobfile"+rows+"' name='job_image[]' class='form-control addinput'><BR><span style='color:red;font-size:14px;' id='invalidfileize"+rows+"'></span><BR></li>");
//   });
// });
     $(document).on("click", "a.remove" , function() {
            $(this).parent().remove();
        });
     
     $('.work-radio').change(function() {
        $(".work-class").removeClass("selectedgreen");
        if ($(this).is(':checked')){
            $(this).closest("li").addClass("selectedgreen");
          }
          else
            $(this).closest("li").removeClass("selectedgreen");
    });
  </script>


<script>
         function myFunction() {
          // var limit=5;
          var count = '<?php echo count($companyImg); ?>';
           var table = document.getElementById("myTable");
           var rows = document.getElementById("myTable").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
            if(rows<(5-count))
            {
               var row = table.insertRow(rows);
               var cell1 = row.insertCell(0);
                  
               cell1.innerHTML = "<input type='file' onchange='functionimg"+rows+"()' id='jobfile"+rows+"' name='site_image[]' class='form-control addinput'><BR><span style='color:red;font-size:14px;' id='invalidfileize"+rows+"'></span><BR>";


            }
            else{
                $('.addmre').hide();
            }
           
           
         }

         $(document).on("click", "a.remove" , function() {
            $(this).parent().remove();
        });
      </script>



<script>  
$(document).ready(function(){

   /*$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:280,
      height:280,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#imgInp').on('change', function(){
   var a=(this.files[0].size);
    if(a > 1000000) {
        alert('Image size should be less than 1 MB');
        //retrun false;
    }else{
      var reader = new FileReader();
      reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    }
    
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
      $.ajax({
        url:"<?php echo base_url(); ?>recruiter/recruiter/upload",
        type: "POST",
        data:{"image": response},
        success:function(data)
        {
         //alert(data);
          $('#uploadimageModal').modal('hide');
          $('#blah').attr('src',data);
          $('#cropimg').val(data);
        }
      });
    })
  });*/

});  
</script>

<script type="text/javascript">
      $(document).ready(function() {
            var lengthText = $('#lengthtext').val();
            var calcLength = parseInt(1000) - parseInt(lengthText.length);
            var textcount = "Remaining characters : " + calcLength;
            $('#getlengthtext').html(textcount);

            $('#lengthtext').on('keyup', function() {
                  var lengthText = $(this).val();
                  var calcLength = parseInt(1000) - parseInt(lengthText.length);
                  var textcount = "Remaining characters : " + calcLength;
                  $('#getlengthtext').html(textcount);
            });

      });
</script>

<script type="text/javascript"> 
   $(document).ready(function(){
         $('#jobfile').change(function () {
            var fileUpload = document.getElementById("jobfile");
            
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
            if (regex.test(fileUpload.value.toLowerCase())) {

               if (typeof (fileUpload.files) != "undefined") {

                  //Initiate the FileReader object.
                  var reader = new FileReader();
                  //Read the contents of Image File.
                  reader.readAsDataURL(fileUpload.files[0]);
                  reader.onload = function (e) {
                      //Initiate the JavaScript Image object.
                      var image = new Image();
       
                      //Set the Base64 string return from FileReader as source.
                      image.src = e.target.result;
                             
                      //Validate the File Height and Width.
                      image.onload = function () {
                          var height = parseInt(this.height);
                          var width = parseInt(this.width);

                          if (width>=400 && height>=300) {

                              if(width<=450 && height<= 350) {
                                 $("#invalidfileize").html("");

                              } else {
                                 $("#invalidfileize").html("Image size must be within 400X300 - 450X350");
                                 $('#jobfile').val('');
                              }
                              
                          } else {
                              $("#invalidfileize").html("Image size must be within 400X300 - 450X350");
                              $('#jobfile').val('');
                          }
                      };
       
                  }
               } else {
                  $('#jobfile').val('');
               }

            } else {
               $("#invalidfileize").html("Please select a valid Image file.");
               $('#jobfile').val('');
            }
         });
   });

   function functionimg1() {


         //$('#jobfile1').change(function () {
            var fileUpload = document.getElementById("jobfile1");

            console.log(fileUpload);
            
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
            if (regex.test(fileUpload.value.toLowerCase())) {

               if (typeof (fileUpload.files) != "undefined") {

                  //Initiate the FileReader object.
                  var reader = new FileReader();
                  //Read the contents of Image File.
                  reader.readAsDataURL(fileUpload.files[0]);
                  reader.onload = function (e) {
                      //Initiate the JavaScript Image object.
                      var image = new Image();
       
                      //Set the Base64 string return from FileReader as source.
                      image.src = e.target.result;
                             
                      //Validate the File Height and Width.
                      image.onload = function () {
                          var height = parseInt(this.height);
                          var width = parseInt(this.width);

                          if (width>=400 && height>=300) {

                              if(width<=450 && height<= 350) {
                                 $("#invalidfileize1").html("");

                              } else {
                                 $("#invalidfileize1").html("Image size must be within 400X300 - 450X350");
                                 $('#jobfile1').val('');
                              }
                              
                          } else {
                              $("#invalidfileize1").html("Image size must be within 400X300 - 450X350");
                              $('#jobfile1').val('');
                          }
                      };
       
                  }
               } else {
                  $('#jobfile1').val('');
               }

            } else {
               $("#invalidfileize1").html("Please select a valid Image file.");
               $('#jobfile1').val('');
            }
         //});
   }

   function functionimg2() {
         //$('#jobfile2').change(function () {
            var fileUpload = document.getElementById("jobfile2");
            
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
            if (regex.test(fileUpload.value.toLowerCase())) {

               if (typeof (fileUpload.files) != "undefined") {

                  //Initiate the FileReader object.
                  var reader = new FileReader();
                  //Read the contents of Image File.
                  reader.readAsDataURL(fileUpload.files[0]);
                  reader.onload = function (e) {
                      //Initiate the JavaScript Image object.
                      var image = new Image();
       
                      //Set the Base64 string return from FileReader as source.
                      image.src = e.target.result;
                             
                      //Validate the File Height and Width.
                      image.onload = function () {
                          var height = parseInt(this.height);
                          var width = parseInt(this.width);

                          if (width>=400 && height>=300) {

                              if(width<=450 && height<= 350) {
                                 $("#invalidfileize2").html("");

                              } else {
                                 $("#invalidfileize2").html("Image size must be within 400X300 - 450X350");
                                 $('#jobfile2').val('');
                              }
                              
                          } else {
                              $("#invalidfileize2").html("Image size must be within 400X300 - 450X350");
                              $('#jobfile2').val('');
                          }
                      };
       
                  }
               } else {
                  $('#jobfile2').val('');
               }

            } else {
               $("#invalidfileize2").html("Please select a valid Image file.");
               $('#jobfile2').val('');
            }
         //});
   }

   function functionimg3() {
         //$('#jobfile2').change(function () {
            var fileUpload = document.getElementById("jobfile3");
            
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
            if (regex.test(fileUpload.value.toLowerCase())) {

               if (typeof (fileUpload.files) != "undefined") {

                  //Initiate the FileReader object.
                  var reader = new FileReader();
                  //Read the contents of Image File.
                  reader.readAsDataURL(fileUpload.files[0]);
                  reader.onload = function (e) {
                      //Initiate the JavaScript Image object.
                      var image = new Image();
       
                      //Set the Base64 string return from FileReader as source.
                      image.src = e.target.result;
                             
                      //Validate the File Height and Width.
                      image.onload = function () {
                          var height = parseInt(this.height);
                          var width = parseInt(this.width);

                          if (width>=400 && height>=300) {

                              if(width<=450 && height<= 350) {
                                 $("#invalidfileize3").html("");

                              } else {
                                 $("#invalidfileize3").html("Image size must be within 400X300 - 450X350");
                                 $('#jobfile3').val('');
                              }
                              
                          } else {
                              $("#invalidfileize3").html("Image size must be within 400X300 - 450X350");
                              $('#jobfile3').val('');
                          }
                      };
       
                  }
               } else {
                  $('#jobfile3').val('');
               }

            } else {
               $("#invalidfileize3").html("Please select a valid Image file.");
               $('#jobfile3').val('');
            }
         //});
   }

   function functionimg4() {
         //$('#jobfile2').change(function () {
            var fileUpload = document.getElementById("jobfile4");
            
            var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.gif)$");
            if (regex.test(fileUpload.value.toLowerCase())) {

               if (typeof (fileUpload.files) != "undefined") {

                  //Initiate the FileReader object.
                  var reader = new FileReader();
                  //Read the contents of Image File.
                  reader.readAsDataURL(fileUpload.files[0]);
                  reader.onload = function (e) {
                      //Initiate the JavaScript Image object.
                      var image = new Image();
       
                      //Set the Base64 string return from FileReader as source.
                      image.src = e.target.result;
                             
                      //Validate the File Height and Width.
                      image.onload = function () {
                          var height = parseInt(this.height);
                          var width = parseInt(this.width);

                          if (width>=400 && height>=300) {

                              if(width<=450 && height<= 350) {
                                 $("#invalidfileize4").html("");

                              } else {
                                 $("#invalidfileize4").html("Image size must be within 400X300 - 450X350");
                                 $('#jobfile4').val('');
                              }
                              
                          } else {
                              $("#invalidfileize4").html("Image size must be within 400X300 - 450X350");
                              $('#jobfile4').val('');
                          }
                      };
       
                  }
               } else {
                  $('#jobfile4').val('');
               }

            } else {
               $("#invalidfileize4").html("Please select a valid Image file.");
               $('#jobfile4').val('');
            }
         //});
   }
</script>
   </body>
</html>

