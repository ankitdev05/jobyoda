<?php include('header.php'); ?>


               <div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon s-6 icon-person-box"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Statement Generation</div>
                                 <!-- <div class="">Total: <?php if($Lists){echo count($Lists); }?></div> -->
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        <!-- <div class="col-auto">
                           <a href="<?php echo base_url();?>administrator/recruiter/hirecost" class="btn btn-light">ADD Hire Cost</a>
                        </div> -->
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements reculsting hirecd"> 
                                             <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text" style="width: 40px;"> 
                                                         <div class="table-header">
                                                            <span class="column-title">SNo</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Company</span>
                                                         </div>
                                                      </th>
                                                      <!-- <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Hire Cost</span>
                                                         </div>
                                                      </th> -->
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Billing Cut-Off</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text" style="width: 80px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Add/Edit Cost Per Hire</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">View Candidates</span>
                                                         </div>
                                                      </th>
                                                      
                                                      

                                                      <th class="secondary-text" style="width: 80px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Generate Invoice</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text" style="width: 100px;">
                                                         <div class="table-header">
                                                            <span class="column-title">View All Invoice</span>
                                                         </div>
                                                      </th>
                            
                                                     
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <?php
                                                      $x1 =1;
                                                      foreach($Lists as $List) {
                                                      ?>
                                                   <tr>
                                                      <td style="width: 10px;"><?php echo $x1;?></td>
                                                      <td style="width: 50px;"><?php echo $List['cname'];?></td>
                                                      <!-- <td style="width: 80px;"><?php if(!empty($List['cost'])){ echo $List['cost'];}?></td> -->
                                                      <td style="width: 60px;"><?php if(!empty($List['billing_date'])){ echo $List['effective_date'];}?></td>
                                                      
                                                      <td style="width: 80px; text-align: center;">
                                                        <?php if(empty($List['cost'])){?>
                                                          <a style="margin:0 0px;" title="Add Cost" href="<?php echo base_url();?>administrator/recruiter/hirecost?id=<?php echo base64_encode($List['id']); ?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-plus-outline"></i></a>   
                                                        <?php }else{?>
                                                           <a style="margin:0 0px;" title="Edit" href="<?php echo base_url();?>administrator/recruiter/hirecost?id=<?php echo base64_encode($List['id']);?>" class="btn btn-icon usricos" aria-label="Product details"><i class="icon icon-pencil s-4"></i></a>
                                                        <?php }?>                                
                             
                                                      </td>
                                                      <td style="width: 80px; text-align: center;"">
                                                       <?php if($List['haveList']){ ?>
                                                        <?php if(!empty($countt)){ echo $countt; }else{ echo "0"; } ?> 
                                                        <a style="margin:0 0px;" href="<?php echo base_url(); ?>administrator/recruiter/hiredListing?id=<?php echo base64_encode($List['id']); ?>"><i class="icon s-4 icon-eye"></i></a>
                                                       <?php }else{?>
                                                       <?php echo '0'; ?>
                                                       <a title="No Candidate Hired" style="margin:0 0px;" href="#"><i class="icon s-4 icon-eye"></i></a>
                                                       <?php }?>
                                                      </td>
                                                      <td style="width: 80px;">
                                                      <?php if($List['haveList']){ ?>
                                                         <a style="margin:0 0px;" href="#" data-toggle="modal" id="<?php echo $List['id'];?>" onclick="getrid1(this.id)" data-target="#myModal2" class="btn btn-block btn-secondary fuse-ripple-ready" aria-label="Product details">Invoice</a>
                                                         <?php }else{?>
                                                         <a title="No Candidate Hired" style="margin:0 0px;" href="#" data-toggle="modal" id="<?php echo $List['id'];?>"  class="btn btn-block btn-secondary fuse-ripple-ready" aria-label="Product details">Invoice</a>
                                                         <?php }?>
                                                      </td> 
                                                      <td style="width: 80px;">
                                                      <?php if($List['iclist']){ ?>
                                                        <a href="<?php echo base_url(); ?>administrator/Jobpost/invoice?id=<?php echo base64_encode($List['id']); ?>">All invoice</a>
                                                      <?php }else{?>
                                                      <a href="#" title="No Invoice Available">All invoice</a>
                                                      <?php } ?>  
                                                      </td>
                                                      
                                                   </tr>
                                                   <?php
                                                      $x1++;
                                                      }
                                                      ?>
                                                </tbody>
                                             </table>
                                             <script type="text/javascript">
                                                $('#sample-data-table').DataTable();
                                             </script>
                                          </div>
                                       </div>
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>

<div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Recruiter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this recruiter?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createLink">Delete</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="blockModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Block Recruiter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to block this recruiter?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createbLink">Block</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="activeModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Active Recruiter</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure to active this recruiter?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createaLink">Active</a>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="myModal2" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="hired_list">
            <input type="hidden" name="recruiter_id" id="recruiter_id" value="">
            
            
        </div>
    </div>
</div>

<!-- <div class="modal" id="invoiceModal" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Generate Invoice</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="invoice_body">
                
               
            </div>
            <div class="modal-footer">
                <a href="#"  class="btn btn-secondary"  id="createLink1">Send Invoice</a>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                
            </div>
        </div>
    </div>
</div> -->


<script type="text/javascript">
    function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/recruiter/deleteRecruiter?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
    }

    function getbid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/recruiter/recruiterBlock?id="+rid;
        var aa = document.getElementById('createbLink');
        aa.setAttribute("href", link);
    }
    function getaid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/recruiter/recruiterActive?id="+rid;
        var aa = document.getElementById('createaLink');
        aa.setAttribute("href", link);
    }

    function getrid1(id) {
        var rid = id;
          var url =   '<?php echo base_url(); ?>/administrator/recruiter/fetchhired';
       
          $.ajax({
                   type:"POST",
                   url:url,
                   data:{
                       rid : rid
                   },

                   success:function(data)

                    {
                     //alert(data);
                     
                  $("#hired_list").html(data);
                  $('#recruiter_id').val(rid);
                  
                    }
                });
    }

    function generate_invoice(){
      //var amount = $('#amount').val();
      var recruiter_id = $('#recruiter_id').val();
      var url =   '<?php echo base_url(); ?>/administrator/recruiter/fetchhired1';
       var link = "<?php echo base_url();?>administrator/recruiter/pdfdetails?rid="+recruiter_id;
        var aa = document.getElementById('createiLink');
        window.open(link, "_blank");  
          $('#myModal2').modal('hide');
      
    }

    /*function generate_pdf(){
      var amount = $('#amount').val();
      var recruiter_id = $('#recruiter_id').val();
      var link = "<?php echo base_url();?>administrator/recruiter/pdfdetails?rid="+recruiter_id+"&amount="+amount;
        var aa = document.getElementById('createLink1');
        aa.setAttribute("href", link);
      
    }*/

   
</script>
   </body>
</html>

