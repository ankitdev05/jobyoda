<?php include('header.php'); ?>                <div class="content custom-scrollbar">

                    <div id="e-commerce-products" class="page-layout carded full-width">

                        <div class="top-bg bg-primary"></div>

                        <!-- CONTENT -->
                        <div class="page-content-wrapper">

                            <!-- HEADER -->
                            <div class="page-header light-fg row no-gutters align-items-center justify-content-between">

                                <!-- APP TITLE -->
                                <div class="col-12 col-sm">

                                    <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                                        <div class="logo-icon mr-3 mt-1">
                                            <i class="icon-cube-outline s-6"></i>
                                        </div>
                                        <div class="logo-text">
                                            <div class="h4">Admin Listing</div>
                                            <div class="">Total: <?php if($adminLists){echo count($adminLists); }?></div>
                                        </div>
                                    </div>

                                </div>
                                <!-- / APP TITLE -->

                                <!-- SEARCH -->
                                <!--<div class="col search-wrapper px-2">

                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-icon">
                                                <i class="icon icon-magnify"></i>
                                            </button>
                                        </span>
                                        <input id="products-search-input" type="text" class="form-control" placeholder="Search" aria-label="Search" />
                                    </div>

                                </div>-->
                                <!-- / SEARCH -->

                                <div class="col-auto">
                                    <a href="<?php echo base_url();?>administrator/admin/signup" class="btn btn-light">ADD NEW</a>
                                </div>

                            </div>
                            <!-- / HEADER -->

                            <div class="page-content-card">
                                <table id="e-commerce-products-table" class="table dataTable">
                                    <thead>
                                        <tr>
                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">ID</span>
                                                </div>
                                            </th>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Name</span>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Username</span>
                                                </div>
                                            </th>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Email</span>
                                                </div>
                                            </th>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Active</span>
                                                </div>
                                            </th>

                                            <th>
                                                <div class="table-header">
                                                    <span class="column-title">Actions</span>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                      $x1 =1;
                                      foreach($adminLists as $adminList) {
                                    ?>
                                        <tr>
                                            <td><?php echo $x1;?></td>
                                            <td><?php echo $adminList['name'];?></td>
                                            <td><?php echo $adminList['username'];?></td>
                                            <td><?php echo $adminList['email'];?></td>
                                            <td>true</td>
                                            <td>
                                                <a href="<?php echo base_url();?>administrator/admin/adminUpdate?id=<?php echo $adminList['id'];?>" class="btn btn-icon" aria-label="Product details"><i class="icon icon-pencil s-4"></i></a>
                                               
                                            </td>
                                        </tr>
                                    <?php
                                      $x1++;
                                      }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- / CONTENT -->
                    </div>

                    <script type="text/javascript" src="<?php echo base_url().'adminfiles/';?>assets/js/apps/e-commerce/products/products.js"></script>

                </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
                <div class="list-group" class="date">

                    <div class="list-group-item subheader">TODAY</div>

                    <div class="list-group-item two-line">

                        <div class="text-muted">

                            <div class="h1"> Friday</div>

                            <div class="h2 row no-gutters align-items-start">
                                <span> 5</span>
                                <span class="h6">th</span>
                                <span> May</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Events</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Group Meeting</h3>
                            <p>In 32 Minutes, Room 1B</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Public Beta Release</h3>
                            <p>11:00 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Dinner with David</h3>
                            <p>17:30 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Q&amp;A Session</h3>
                            <p>20:30 PM</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Notes</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Best songs to listen while working</h3>
                            <p>Last edit: May 8th, 2015</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Useful subreddits</h3>
                            <p>Last edit: January 12th, 2015</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Quick Settings</div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Notifications</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Cloud Sync</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Retro Thrusters</h3>
                        </div>

                        <div class="secondary-container">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
           <!-- <a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
                <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
        </nav>
    </main>
</body>

</html>