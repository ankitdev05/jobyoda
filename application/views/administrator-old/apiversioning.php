<?php include('header.php'); 
//print_r($jobtitleLists);
?>

<style type="text/css">
  /* The container */
.form-group.mb-4.chkrecbd .container {
  display: inline;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 16px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default checkbox */
.form-group.mb-4.chkrecbd .container input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.form-group.mb-4.chkrecbd .checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 20px;
  width: 20px;
  background-color: #ffffff;
}

/* On mouse-over, add a grey background color */
.form-group.mb-4.chkrecbd .container:hover input ~ .checkmark {
  background-color: #ffffff;
}

/* When the checkbox is checked, add a blue background */
.form-group.mb-4.chkrecbd .container input:checked ~ .checkmark {
  background-color: #46a56f;
}

/* Create the checkmark/indicator (hidden when not checked) */
.form-group.mb-4.chkrecbd .checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.form-group.mb-4.chkrecbd .container input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.form-group.mb-4.chkrecbd .container .checkmark:after {
  left: 9px;
  top: 5px;
  width: 5px;
  height: 10px;
  border: solid white;
  border-width: 0 3px 3px 0;
  -webkit-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  transform: rotate(45deg);
}
</style>


<div class="content custom-scrollbar">
<div class="doc data-table-doc page-layout simple full-width">


 <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon s-6 icon-text-shadow"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Update App Version</div>
                                 
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
						</div>
						
						<div class="page-content p-6">
					 <div class="content container">
                           <div class="row">
                              <div class="col-12"> 
					 
					 <div class="example">
					 
					  <div class="source-preview-wrapper">
                                       <div class="preview">
									    <div class="preview-elements">
										
                    <div id="registers">

                        <div class="form-wrapper">

                            
                    <div class="mainjob editablepartdgf">
                            <form name="registerForm" action="<?php echo base_url();?>administrator/JobSeeker/versionUpdate" method="post" novalidate>
                        <div class="job">
                                <div class="form-group mb-4">
                                    <input type="text" name="api_version" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" required="required" value="<?php if(!empty($appversion[0]['api_version'])){ echo $appversion[0]['api_version']; }?>" />
                                    <label>App Version</label>
                                    
                                </div>
                                <div class="form-group mb-4 chkrecbd">
                                    <label>Recommend Update</label>
                                   <label class="container">Yes
                                    <input type="radio" name="recommend" value="1" <?php if($appversion[0]['recommend_update']=='1'){ echo "checked"; } ?>>
                                    <span class="checkmark"></span>
                                  </label>
                                    <label class="container">No
                                      <input type="radio" name="recommend" value="0" <?php if($appversion[0]['recommend_update']=='0'){ echo "checked"; } ?>>
                                      <span class="checkmark"></span>
                                    </label>
                                    
                                    
                                </div>
                                <div class="form-group mb-4 chkrecbd">
                                    <label>Force Update</label>
                                    <label class="container">Yes
                                    <input type="radio" name="force" value="1" <?php if($appversion[0]['force_update']=='1'){ echo "checked"; } ?>>
                                    <span class="checkmark"></span>
                                  </label>
                                    <label class="container">No
                                      <input type="radio" name="force" value="0" <?php if($appversion[0]['force_update']=='0'){ echo "checked"; } ?>>
                                      <span class="checkmark"></span>
                                    </label>
                                    
                                    
                                </div>
                                <div class="form-group mb-4">
                                    <label>Description</label>
                                    <textarea name="message" class="form-control" id="content" aria-describedby="nameHelp" required="required" ><?php if(!empty($appversion[0]['message'])){ echo urldecode($appversion[0]['message']); }?></textarea>
                                    
                                    
                                </div>
						</div>		
                           <div class="jobbtn">     
                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                    Update
                                </button>
							</div>	
							</div>
                       
                            </form>
                        </div>
                    </div>
					
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					
                     </div>
                </div>
            </div>
            

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
        </nav>
    </main>

    <script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
      CKEDITOR.replace('content');
    </script>
</body>

</html>