<?php include('header.php');?>
                <div class="content custom-scrollbar">

                    <div id="register" class="p-8">

                        <div class="form-wrapper md-elevation-8 p-8">

                            <div class="title mt-4 mb-8">Update Admin</div>

                            <form name="registerForm" action="<?php echo base_url();?>administrator/admin/updateAdmin" method="post" novalidate>

                                <div class="form-group mb-4">
                                    <input type="text" name="name" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" value="<?php echo $getAdmin[0]['name'];?>" />
                                    <label for="registerFormInputName">Name</label>
                                    <?php if(!empty($errors['name'])){echo "<span class='validError'>".$errors['name']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" name="username" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" value="<?php echo $getAdmin[0]['username'];?>"/>
                                    <label for="registerFormInputName">Username</label>
                                     <?php if(!empty($errors['username'])){echo "<span class='validError'>".$errors['username']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" name="email" class="form-control" id="registerFormInputEmail" aria-describedby="emailHelp" value="<?php echo $getAdmin[0]['email'];?>" />
                                    <label for="registerFormInputEmail">Email address</label>
                                    <?php if(!empty($errors['email'])){echo "<span class='validError'>".$errors['email']."</span>";}?>
                                </div>

                                <!--<div class="form-group mb-4">
                                    <input type="text" name="password" class="form-control" id="registerFormInputPassword" value="<?php echo $this->encryption->decrypt($getAdmin[0]['password']);?>" />
                                    <label for="registerFormInputPassword">Password</label>
                                     <?php if(!empty($errors['password'])){echo "<span class='validError'>".$errors['password']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <input type="text" name="confirmPassword" class="form-control" id="registerFormInputPasswordConfirm" value="<?php echo $getAdmin[0]['password'];?>" />
                                    <label for="registerFormInputPasswordConfirm">Password (Confirm)</label>
                                    <?php if(!empty($errors['confirmPassword'])){echo "<span class='validError'>".$errors['confirmPassword']."</span>";}?>
                                </div>-->

                                <input type="hidden" name="rid" value="<?php echo $getAdmin[0]['id'];?>">
                                
                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                    Update Admin
                                </button>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
                <div class="list-group" class="date">

                    <div class="list-group-item subheader">TODAY</div>

                    <div class="list-group-item two-line">

                        <div class="text-muted">

                            <div class="h1"> Friday</div>

                            <div class="h2 row no-gutters align-items-start">
                                <span> 5</span>
                                <span class="h6">th</span>
                                <span> May</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Events</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Group Meeting</h3>
                            <p>In 32 Minutes, Room 1B</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Public Beta Release</h3>
                            <p>11:00 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Dinner with David</h3>
                            <p>17:30 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Q&amp;A Session</h3>
                            <p>20:30 PM</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Notes</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Best songs to listen while working</h3>
                            <p>Last edit: May 8th, 2015</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Useful subreddits</h3>
                            <p>Last edit: January 12th, 2015</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Quick Settings</div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Notifications</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Cloud Sync</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Retro Thrusters</h3>
                        </div>

                        <div class="secondary-container">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
                <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
        </nav>
    </main>
</body>

</html>