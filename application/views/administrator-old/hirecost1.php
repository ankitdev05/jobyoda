<?php include('header.php'); ?>
<style type="text/css">
  .frmdvt input {
    padding: 0;
    height: 17px;
}
</style>
<div class="content custom-scrollbar">

                    <div id="register" class="p-8">

                        <div class="form-wrapper md-elevation-8 p-8">

                            <div class="title mt-4 mb-8">Hire Cost</div>

                            <form name="registerForm" action="<?php echo base_url();?>administrator/recruiter/hirecostInsert" method="post" novalidate>

                            <div class="dvdcostper">
                            <div class="frmdvt">
                            <div class="leftsd">
                            <lable>Effective Date</lable>
                            </div>

                                <div class="form-group mb-4">
                                <!--  <label>Effective Date</label> -->

                                    <input type="text" autocomplete="off" name="effect_date" id="effect_date" class="form-control" value="<?php if(!empty($Lists[0]['effective_date'])){ echo $Lists[0]['effective_date']; } ?>">
                                   
                                    <span style="color: red;"><?php if(!empty($errors['effect_date'])){ echo $errors['effect_date']; } ?></span>
                                    
                                </div>
                            </div>


                              <div class="frmdvt">
                            <div class="leftsd">
                           <label>Billing cut-off date</label>
                            </div>

                                <div class="form-group mb-4">
                                   
                                    <!-- <label>Billing cut-off date</label> -->
                                    <input type="text" autocomplete="off" name="billing_date" id="billing_date" class="form-control" value="<?php if(!empty($Lists[0]['billing_date'])){ echo $Lists[0]['billing_date']; } ?>">
                                  
                                    <span style="color: red;"><?php if(!empty($errors['billing_date'])){ echo $errors['billing_date']; } ?></span>
                                    
                                </div>
                                </div>

                            
                            <h4>Total Monthly Guaranteed Salary</h4>

                               <div class="frmdvt">
                            <div class="leftsd">
                            <lable><= 50000</lable>
                            </div>
                                <div class="form-group mb-4">
                                    <input type="hidden" name="company_name" value="<?php echo base64_decode($_GET['id']); ?>">
                                    <input type="number" name="cost" class="form-control" value="<?php if(!empty($Lists[0]['cost'])){ echo $Lists[0]['cost']; } ?>">
                                    
                                    <span style="color: red;"><?php if(!empty($errors['cost'])){ echo $errors['cost']; } ?></span>
                                    
                                </div>
                            </div>

                              <div class="frmdvt">
                            <div class="leftsd">
                           <label>> 50000 (in %)</label>
                            </div>

                                <div class="form-group mb-4">
                                 <!-- <label>Percentage</label> -->
                                    <input type="number" name="percentage" class="form-control" value="<?php if(!empty($Lists[0]['percentage'])){ echo $Lists[0]['percentage']; } ?>" min='0' max='100'>
                                   
                                    <span style="color: red;"><?php if(!empty($errors['percentage'])){ echo $errors['percentage']; } ?></span>
                                    
                                </div>
                                </div>

                             </div>

                          <div class="hedbth">

                           <h4 class="fst">Special Profiles</h4>
                              <h4 class="scd">Cost Per Hire</h4>

                           </div>
                                <div class="form-group mb-4 multipleopyodf">
                                <?php //print_r($special); ?>
                                    <table id="myTable">
                                       <tbody>
                                       <?php if(!empty($special)){ foreach ($special as $spcl) {
                                         
                                       ?>
                                          <tr>
                                             <td>
                                                <select name="expRange[]" required="">
                                                   <option value="">Select Special Profile</option>
                                                   <?php
                                                       if($subcategory) {
                                                           foreach($subcategory as $subcat) {

                                                       ?>
                                                        <option <?php if($spcl['special_category']==$subcat['subcategory']){ echo "selected" ; } ?>  value="<?php echo $subcat['subcategory']; ?>" > <?php echo $subcat['subcategory']; ?> </option>
                                                      <?php
                                                         }
                                                         }

                                                       ?>
                                                </select>
                                             </td>
                                             <td><input placeholder="Enter Salary"  name="expBasicSalary[]" type="number" min="1" class="form-control" placeholder="Enter Salary" required="" value="<?php if(!empty($spcl['price'])){ echo $spcl['price'] ;} ?>" ></td>
                                          </tr>
                                          <?php }}else{?>
                                          <tr>
                                             <td>
                                                <select name="expRange[]" required="">
                                                   <option value="">Select Special Profile</option>
                                                   <?php
                                                       if($subcategory) {
                                                           foreach($subcategory as $subcat) {

                                                       ?>
                                                        <option  value="<?php echo $subcat['subcategory']; ?>" > <?php echo $subcat['subcategory']; ?> </option>
                                                      <?php
                                                         }
                                                         }

                                                       ?>
                                                </select>
                                             </td>
                                             <td><input placeholder="Cost Per Hire" name="expBasicSalary[]" type="number" min="1" class="form-control" placeholder="Enter Salary" required=""  ></td>
                                          </tr>
                                          <?php }?>
                                       </tbody>
                                    </table>

                                    <p onclick="myFunction()" class="addmre">Add More</p>
                                    <!-- <select name="special_profile[]" id="multiselect" multiple="multiple" class="form-control">
                                    <?php $profile= explode(',', $Lists[0]['special_profile']); ?>                  
                                    <?php
                                       if($subcategory) {
                                           foreach($subcategory as $subcat) {
                                       ?>
                                    <option <?php if(!empty($profile) && in_array($subcat['id'],$profile)){ echo "selected" ;} ?>  value="<?php echo $subcat['id']; ?>" > <?php echo $subcat['subcategory']; ?> </option>
                                    <?php
                                       }
                                       }

                                       ?>
                                      
                                 </select> -->
                                    
                                </div>
                                

                                <div class="form-group mb-4">
                                  
                                </div>

                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                    Submit
                                </button>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
                <div class="list-group" class="date">

                    <div class="list-group-item subheader">TODAY</div>

                    <div class="list-group-item two-line">

                        <div class="text-muted">

                            <div class="h1"> Friday</div>

                            <div class="h2 row no-gutters align-items-start">
                                <span> 5</span>
                                <span class="h6">th</span>
                                <span> May</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Events</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Group Meeting</h3>
                            <p>In 32 Minutes, Room 1B</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Public Beta Release</h3>
                            <p>11:00 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Dinner with David</h3>
                            <p>17:30 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Q&amp;A Session</h3>
                            <p>20:30 PM</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Notes</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Best songs to listen while working</h3>
                            <p>Last edit: May 8th, 2015</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Useful subreddits</h3>
                            <p>Last edit: January 12th, 2015</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Quick Settings</div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Notifications</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Cloud Sync</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Retro Thrusters</h3>
                        </div>

                        <div class="secondary-container">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
                <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
        </nav>
    </main>
   
<script>
         function myFunction() {
         
           var table = document.getElementById("myTable");
           var rows = document.getElementById("myTable").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
           
           var row = table.insertRow(rows);
           var cell1 = row.insertCell(0);
           var cell2 = row.insertCell(1);
              
            <?php
                if($subcategory) {
                   $exp ="<select name='expRange[]'><option> Select Special Profile</option>";
                   foreach($subcategory as $subcat) {
                          $exp .= "<option value='".$subcat['subcategory']."'>".$subcat['subcategory']."</option>";
                   }
                   $exp .= "</select>";
                }

              ?>
           cell1.innerHTML = "<?php echo $exp;?>";
           cell2.innerHTML = "<input type='text' name='expBasicSalary[]' class='form-control' placeholder='Enter Salary' /><a href='javascript:void(0);' class='remove'><i class='fa fa-trash'></i></a>";
         }
         $(document).on("click", "a.remove" , function() {
            $(this).parent().parent().remove();
        });
      </script>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> 
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script type="text/javascript">
         $(document).ready(function(){  
            $.datepicker.setDefaults({  
                 dateFormat: 'yy-mm-dd'   
            });  
            $(function(){  
                 $("#effect_date").datepicker({ minDate: +1}); 
                 $("#billing_date").datepicker({ minDate: +1}); 
            });
           });
      </script>
      <style type="text/css">
        select {
    background: transparent;
    border: none;
    border-bottom: 1px solid #a29d9d;
    padding: 5px 0px 5px 0px;
    margin-right: 10px;
    color: #655e5e;
}

p.addmre {
    background: #46a56f;
    display: initial;
    color: #fff;
    padding: 4px 7px;
    margin-top: 11px;
    float: left;
    font-size: 14px;
    box-shadow: #524d4d 1px 2px 3px 0px;
    cursor: pointer;
}

p.addmre:hover {
 background: #524d4d;
}

.form-group.mb-4 {
    float: left;
    width: 100%;
}

.frmdvt {
    display: flex;
}

.frmdvt .leftsd {
    width: 95%;
    float: left;
}

.frmdvt .form-group.mb-4 {
    padding: 0;
}

#register .form-wrapper{
      max-width: 61.4rem;
}

table#myTable {
    width: 86%;
}

.hedbth .scd {
    float: left;
}

.hedbth h4 {
    width: 48%;
        border-bottom: 1px solid #ddd;
    padding-bottom: 10px;
}

.hedbth .fst {
    float: left;
}

.hedbth {
    float: left;
    width: 100%;
}


.form-group.mb-4.multipleopyodf table tr td {
    position: relative;
}

.form-group.mb-4.multipleopyodf table tr td a.remove {
    position: absolute;
    right: -41px;
    top: 10px;
    width: 13px;
    cursor: pointer;
}

.form-group.mb-4.multipleopyodf table tr td a.remove i.fa {
    font-size: 15px;
    color: #e63838;
}

.dvdcostper h4 {
    margin-bottom: 23px;
    margin-top: 12px;
    border-bottom: 1px solid #ddd;
    padding-bottom: 10px;
}


      </style>
</body>

</html>
