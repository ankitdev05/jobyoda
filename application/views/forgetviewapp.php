
<html>
   <head>
      <title>Jobyoda - Home</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
      <link href="<?php echo base_url().'webfiles/';?>css/all.css" rel="stylesheet" type="text/css">
      <link href="<?php echo base_url().'webfiles/';?>css/fontawesome.css" rel="stylesheet" type="text/css">
      <link href="<?php echo base_url().'webfiles/';?>css/bootstrap.css" rel="stylesheet" type="text/css">
      <link href="<?php echo base_url().'webfiles/';?>css/style.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="<?php echo base_url().'webfiles/';?>css/owl.carousel.min.css">
      <link rel="stylesheet" href="<?php echo base_url().'webfiles/';?>css/owl.theme.default.min.css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url().'webfiles/';?>css/timepicker.less" rel="stylesheet" type="text/css" />
      <style type="text/css">
         #signupform .error{color:#f00;}
         .profileupload img{width: 150px;height: 107px;}
      </style>
   </head>
   <style>
     
   

   </style>
   <body>
 
      <header class="mainhead">

         <div class="container-fluid">
            <div class="row">
              
               <div class="logomain col-md-3">
                  <a href="<?php echo base_url(); ?>user/index"><img src="<?php echo base_url().'recruiterfiles/';?>images/jobyoDa.png" style="width:179px;"></a>
               </div>
             
            </div>
         </div>
      </header>

   
      <div class="bannerarea">
         <div class="container">
            <div class="banrcontentt appars">

               <h2>Forgot Password</h2>
            
<?php 

 if(!empty($this->session->flashdata('msgg')))
 {
   echo $this->session->flashdata('msgg');
 }

?>

<?php 
     if(!empty($this->session->flashdata('msg')))
     {
      echo $this->session->flashdata('msg');
     }

     if(!empty($mismatch_err))
     {
      echo "<p class='text-center text-danger'>".$mismatch_err."</p>";;
     }

   ?>
               <form action="<?php echo base_url('User/showapp')?>" id="registration-form" method="post">
               
                <div class="row">
                  <div class="col-md-4">Enter Verification Code</div>
                  <div class="col-md-8">
                     <input type="text" name="verify_code" placeholder="Enter Verification Code" data-validation="required" class="form-control" value="<?php if(!empty($userData['verify_code'])){ echo $userData['verify_code']; } ?>">

                  </div>
                 
                </div>      
               <?php if(isset($signuperrors['verify_code'])){echo "<p class='text-center text-danger'>".$signuperrors['verify_code']."</p>"; } ?>
               <div class="form-group"></div> 
               <div class="row">
                  <div class="col-md-4">Enter New Password</div>
                  <div class="col-md-8">
                     <input type="password" id="password-field" name="new_pass" placeholder="Enter New Password" data-validation="required"  class="form-control" value="<?php if(!empty($userData['new_pass'])){ echo $userData['new_pass']; } ?>">
                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    <?php if(isset($signuperrors['new_pass'])){echo "<p class='text-center text-danger'>".$signuperrors['new_pass']."</p>"; } ?>
                  </div>
                
                  </div>
                    
                  <div class="form-group"></div>
                   
                  <div class="row">
                  <div class="col-md-4">Enter Confirm Password</div>
                  <div class="col-md-8">
                     <input type="password" id="cpassword-field" name="con_pass" placeholder="Enter Confirm Password" data-validation="required" class="form-control" value="<?php if(!empty($userData['con_pass'])){ echo $userData['con_pass']; } ?>">
                     <span toggle="#cpassword-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                     <?php if(isset($signuperrors['con_pass'])){echo "<p class='text-center text-danger'>".$signuperrors['con_pass']."</p>"; } ?>
                  </div>
                 
                  </div>
                       
                   <div class="form-group"></div>
                  <div class="row">
                      <input type="hidden" name="uri_data" value="<?php echo base64_decode($uri_data) ?>">
                     <div class="col-md-4"></div>
                     <div class="col-md-8">
                     <div class="subtn">
                        <button type="submit" name="subdata" class="btn btn-primary">Submit</button>
                        </div>
                     </div>
                  </div>
                  </form>
               
            </div>
         </div>
      
      </div>
     

      
      <?php //include_once('footer.php'); ?>
     

      
   
      <script src="<?php echo base_url().'webfiles/';?>js/jquery.min.js"></script>
      <script src="<?php echo base_url().'webfiles/';?>js/popper.min.js"></script>
      <script src="<?php echo base_url().'webfiles/';?>js/bootstrap.js"></script>
      <script src="<?php echo base_url().'webfiles/';?>js/owl.carousel.js"></script>
      <script src="<?php echo base_url().'webfiles/';?>js/adminyoda.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
      <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk&libraries=places&callback=initMap"></script>
    
   </body>
</html>

<script>
   $(document).ready(function(){   
    $("#forgetpass").click(function() {
      //alert();die();
      var user_email = $("#user_email").val();
     // alert(user_email);
      //return false;
     // var pass = $("#loginpass").val();
     $.ajax({
          type: "POST",
          url: "<?php echo base_url(); ?>" + "user/forgetpassword",
          data: {email:user_email},
          cache:false,
          success:function(htmldata){
            //alert(htmldata);
              $('#errorfieldd').html(htmldata);
              
          },
          error:function(){
            console.log('error');
          }
      });
      
  });
});
</script>
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
  $(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});

  $.validate({
    modules : 'location, date, security, file',
    onModulesLoaded : function() {
      $('#country').suggestCountry();
    }
  });

  // Restrict presentation length
  //$('#presentation').restrictLength( $('#pres-max-length') );

</script>
