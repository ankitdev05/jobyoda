<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>JoYoDA</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	
	<script type="text/javascript">
		$('document').ready(function() {

			if(localStorage.getItem('lastactivity')) {

				var date1 = localStorage.getItem('lastactivity');
			       var date2 = Date.now();
			        if (date1 !== undefined) {
			          	let difference = date2 - date1;
			          	let diff = ((difference) / 1000).toFixed(0);
			          
			          	if (diff > 800) {
			            	localStorage.clear();

			              	if ("geolocation" in navigator) {

			                navigator.geolocation.getCurrentPosition(function(position) { 
			                  var currentLatitude = position.coords.latitude;
			                  var currentLongitude = position.coords.longitude;
			                  var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
			                  var geocoder = geocoder = new google.maps.Geocoder();
			                  geocoder.geocode({ 'latLng': latlng }, function (results, status) {
			                         
			                     if (status == google.maps.GeocoderStatus.OK) {
			                         if (results[1]) {
			                             var fill_address = results[1].formatted_address;
			                         }
			                     }
			                  });
			                 
			                  //$('#cur_lat').val(currentLatitude);
			                  //$('#cur_long').val(currentLongitude);
			                  //$('#lat').val(currentLatitude);
			                  //$('#long').val(currentLongitude);
			                
			                  //if (localStorage) {

			                    localStorage.setItem('currentLatitude', currentLatitude);
			                    localStorage.setItem('currentLongitude', currentLongitude);
			                    localStorage.setItem('test','1');
			                    var date = Date.now();
			                    localStorage.setItem("lastactivity", date);

			                    $.ajax({
			                       type: "POST",
			                       url: "<?php echo base_url(); ?>" + "user/fetch_location",
			                       data: {currentLatitude:currentLatitude,currentLongitude:currentLongitude},
			                       success:function(data) {
			                          location.reload();
			                       },
			                       error:function() {
			                       		location.reload();
			                       }
			                    });
			                    
			                    
			                  //}
			                },showError);
			              }
			          }
			    }

			} else {

			      if ("geolocation" in navigator) {

			        navigator.geolocation.getCurrentPosition(function(position) { 

			        	//alert('ssss');

			          var currentLatitude = position.coords.latitude;
			          var currentLongitude = position.coords.longitude;
			          var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
			          var geocoder = geocoder = new google.maps.Geocoder();
			          geocoder.geocode({ 'latLng': latlng }, function (results, status) {
			                 
			             if (status == google.maps.GeocoderStatus.OK) {
			                 if (results[1]) {
			                     var fill_address = results[1].formatted_address;
			                 }
			             }
			          });

			          //alert(currentLatitude);
			         
			          //$('#cur_lat').val(currentLatitude);
			          //$('#cur_long').val(currentLongitude);
			          //$('#lat').val(currentLatitude);
			          //$('#long').val(currentLongitude);
			        
			          //if (localStorage) {

			            localStorage.setItem('currentLatitude', currentLatitude);
			            localStorage.setItem('currentLongitude', currentLongitude);
			            localStorage.setItem('test','1');
			            var date = Date.now();
			            localStorage.setItem("lastactivity", date);

			            $.ajax({
			               type: "POST",
			               url: "<?php echo base_url(); ?>" + "user/fetch_location",
			               data: {currentLatitude:currentLatitude,currentLongitude:currentLongitude},
			               success:function(data) {
			                   //console.log(data);
			                   location.reload();
			               },
			               error:function(){
			                 	//console.log('error');
			                 	location.reload();
			               }
			            });
			         
			        },showError);
			      }
			  }
	    

			  function showError(error) {
			    switch(error.code) {
			      case error.PERMISSION_DENIED:
			        $('#gpsModal').modal('show');
			        break;
			      case error.POSITION_UNAVAILABLE:
			        alert("Location information is unavailable.")
			        break;
			      case error.TIMEOUT:
			        alert("The request to get user location timed out.")
			        break;
			      case error.UNKNOWN_ERROR:
			        alert("An unknown error occurred.")
			        break;
			    }
			  }

		});
	</script>

</head>
<body>
	
</body>
</html>