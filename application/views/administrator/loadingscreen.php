<style type="text/css">
  .error_span{
    color: red;
  }
</style>

<?php include('header.php'); 
//print_r($jobtitleLists);
?>
<div class="content custom-scrollbar">
<div class="doc data-table-doc page-layout simple full-width">

<div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon s-6 icon-buffer"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Add Quote</div>
                                 
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
						</div>
						
<div class="page-content p-6">
					 <div class="content container">
                           <div class="row">
                              <div class="col-12"> 
					 
					 <div class="example">
					 
					  <div class="source-preview-wrapper">
                                       <div class="preview">
									    <div class="preview-elements">
                    <div id="registers">

                        <div class="form-wrapper">

                         
                    <div class="mainjob editablepartdgf">
                            <form name="registerForm" action="<?php echo base_url();?>administrator/JobSeeker/quoteInsert" method="post" novalidate>
                        <div class="job">
                                 <span class="error_span"><?php if(!empty($this->session->tempdata('quoterr'))){ echo $this->session->tempdata('quoterr'); } ?></span> 
                                <div class="form-group mb-4">
								                    <select name="title" class="form-control" id="registerFormInputName">
                                      <option value="">Select Screen</option>        
                                      <option <?php if(!empty($singlequote[0]['title'])){ if($singlequote[0]['title']=='Home Screen'){ echo "Selected"; }  } ?> value="Home Screen">Home Screen</option>        
                                      <option <?php if(!empty($singlequote[0]['title'])){ if($singlequote[0]['title']=='JobListing'){ echo "Selected"; }  } ?> value="JobListing">JobListing</option>        
                                      <option <?php if(!empty($singlequote[0]['title'])){ if($singlequote[0]['title']=='JobDetail'){ echo "Selected"; }  } ?> value="JobDetail">JobDetail</option>        
                                      <option <?php if(!empty($singlequote[0]['title'])){ if($singlequote[0]['title']=='HotJob'){ echo "Selected"; }  } ?> value="HotJob">HotJob</option>        
                                      <option <?php if(!empty($singlequote[0]['title'])){ if($singlequote[0]['title']=='AppliedJob'){ echo "Selected"; }  } ?> value="AppliedJob">AppliedJob</option>        
                                      <option <?php if(!empty($singlequote[0]['title'])){ if($singlequote[0]['title']=='Saved job'){ echo "Selected"; }  } ?> value="Saved job">Saved job</option>
                                      <option <?php if(!empty($singlequote[0]['title'])){ if($singlequote[0]['title']=='Profile'){ echo "Selected"; }  } ?> value="Profile">Profile</option>
                                      <option <?php if(!empty($singlequote[0]['title'])){ if($singlequote[0]['title']=='Filter'){ echo "Selected"; }  } ?> value="Filter">Filter</option>        
                                    </select>
                                    <!-- <input type="text" name="title" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" value="<?php if(!empty($singlequote[0]['title'])){ echo $singlequote[0]['title']; } ?>" required="required" /> -->
                                    <span class="error_span"><?php if(!empty($errors['title'])){ echo $errors['title']; } ?></span>
                                    
                                </div>
                                <div class="form-group mb-4">
                                    <textarea name="quote" class="form-control" id="registerFormInputQuote" required=""><?php if(!empty($singlequote[0]['quote'])){ echo $singlequote[0]['quote']; } ?></textarea> 
                                    <span class="error_span"><?php if(!empty($errors['quote'])){ echo $errors['quote']; } ?></span>
                                    <input type="hidden" name="quote_id" value="<?php if(!empty($singlequote[0]['id'])){ echo $singlequote[0]['id']; }?>">
                                    <label for="registerFormInputQuote">Description</label>
                                </div>
                  </div>      
                           <div class="jobbtn">     
                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN"><?php if(!empty($singlequote[0]['id'])){?>Update Quote<?php }else{?>
                                    Add Quote <?php }?>
                                </button>
                     </div>   
                     </div>
                               <div class="newcustom">
                           <div class="preview">
                                          <div class="preview-elements">
                                           <!--  <div id="sample-data-table_wrapper" class="dataTables_wrapper no-footer"><div class="dataTables_length" id="sample-data-table_length"><label>Show <select name="sample-data-table_length" aria-controls="sample-data-table" class=""><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div><div id="sample-data-table_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="sample-data-table"></label></div>-->
                                  <table id="sample-data-table" class="table dataTable no-footer" role="grid" aria-describedby="sample-data-table_info" style="width: 855px;">
                                                <thead>
                                                   <tr role="row"><th class="secondary-text sorting_asc" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-sort="ascending" aria-label="
                                                         
                                                            ID
                                                         
                                                      : activate to sort column descending" style="width: 40px;">
                                                         <div class="table-header">
                                                            <span class="column-title">SNo</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Screen Name</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Quotation</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Action
                                                         
                                                      : activate to sort column ascending" style="width: 68px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Action</span>
                                                         </div>
                                                      </th></tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $x1 =1;
                                                    foreach($Lists as $joblevels){
                                                    ?>
                                                    <tr role="row" class="odd">
                                                      <td class="sorting_1"><?php echo $x1;?></td>
                                                      <td><?php echo $joblevels['title'];?></td>
                                                      <td><?php echo $joblevels['quote'];?></td>
                                                      
                                                      
                                                      
                                                      <td>
                                         
                                           <a title="Edit" href="<?php echo base_url(); ?>administrator/JobSeeker/loadingscreen?id=<?php echo base64_encode($joblevels['id']) ?>" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Job Level"><i class="icon s-4 icon-pencil"></i></a> 

                                           <a title="Delete" href="#" data-toggle="modal" id="<?php echo $joblevels['id'];?>" onclick="getrid(this.id)" data-target="#myModal1" class="btn btn-icon usricos" aria-label="Product details"><i class="icon s-4 icon-trash"></i></a> 
                                              
                                                      </td> 
                                                      
                                                   </tr>
                                                   <?php $x1++;
                                                   }?>
                                                   </tbody>
                                             </table><!--
                                  <div class="dataTables_info" id="sample-data-table_info" role="status" aria-live="polite">Showing 1 to 10 of 17 entries</div><div class="dataTables_paginate paging_simple_numbers" id="sample-data-table_paginate"><a class="paginate_button previous disabled" aria-controls="sample-data-table" data-dt-idx="0" tabindex="0" id="sample-data-table_previous">Previous</a><span><a class="paginate_button current" aria-controls="sample-data-table" data-dt-idx="1" tabindex="0">1</a><a class="paginate_button " aria-controls="sample-data-table" data-dt-idx="2" tabindex="0">2</a></span><a class="paginate_button next" aria-controls="sample-data-table" data-dt-idx="3" tabindex="0" id="sample-data-table_next">Next</a></div></div>-->
                                             <script type="text/javascript">
                                                $('#sample-data-table').DataTable();
                                             </script>
                                          </div>
                                       </div>
                        </div>
                            </form>
                        </div>
                    </div>
					
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					
                 </div>
				 
                </div>
            </div>
            

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
        </nav>
    </main>

    <div class="modal" id="myModal1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Quotation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you want to delete this Quotation?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="" class="btn btn-primary" id="createLink">Delete</a>
            </div>
        </div>
    </div>
</div>

<!-- <div class="modal" id="myModal32" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>The message is already added for this screen. Do you want to update it?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <?php $title = $_REQUEST['title'];?>
                <a href="<?php echo base_url('administrator/JobSeeker/quoteInsert?id=$title'); ?>" class="btn btn-primary" id="createLink">Update</a>
            </div>
        </div>
    </div>
</div> -->
</body>

</html>
<!-- <?php if($this->session->tempdata('quoterr')!=null){
  if($this->session->tempdata('quoterr')){$this->session->unset_tempdata('quoterr');}?>

<script type="text/javascript">
  $(window).on('load',function(){
           $('#myModal32').modal('show');
           var registerFormInputName = $('#registerFormInputName').val();
           var registerFormInputQuote = $('#registerFormInputQuote').val(); 
       });
  
  if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
          //window.location.href = "<?php echo base_url();?>recruiter/recruiter/index";
      }
</script>
<?php }?> -->
<script type="text/javascript">
  function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/deleteQuote?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
    }
</script>