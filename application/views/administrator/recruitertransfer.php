<style type="text/css">
  .validError {
     color: red;
     }

#myModal .modal-dialog{ width: 500px }
#myModal .modal-dialog .modal-body{
    text-align: center;
}
#myModal .modal-dialog .modal-body h4{
    text-align: center;
    margin: 0 0 10px;
    font-weight: 500;
    color: #000;
}
#myModal .modal-header button.close {
    position: absolute;
    padding: 0 !important;
    margin: 0 !important;
    top: -13px;
    right: -13px;
    width: 26px;
    height: 26px;
    background-color: #fff;
    opacity: 1;
    border-radius: 50%;
    border: 2px solid rgba(0, 0, 0, 0.5);
    font-size: 14px;
}
#myModal .modal-header{
    padding: 15px 15px 0 15px
}
#myModal .modal-header h4.modal-title {
    display: block;
    float: left;
    width: 100%;
    text-align: center;
    font-weight: 500;
    font-size: 25px;
}
#myModal .modal-dialog .modal-body p{
    font-size: 16px;
}
#myModal .modal-dialog .modal-body form button.Submit{
    padding: 0 30px;
    box-shadow: none;
}


</style>

<?php include( 'header.php'); ?>
<div class="content custom-scrollbar">
  <div class="doc data-table-doc page-layout simple full-width">
    <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
      <!-- APP TITLE -->
      <div class="col-12 col-sm">
        <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
          <div class="logo-icon mr-3 mt-1"> <i class="icon s-6 icon-playlist-check"></i>
          </div>
          <div class="logo-text">
            <div class="h4">Transfer Request</div>
          </div>
        </div>
      </div>
      <!-- / APP TITLE -->
    </div>
    <div class="page-content p-6">
      <div class="content container">
        <div class="row">
          <div class="col-12">
            <div class="example">
              <div class="source-preview-wrapper">
                <div class="preview">
                  <div class="preview-elements">
                    <div id="registers">
                      <div class="form-wrapper">
                        <div class="mainjob">
                          <form name="registerForm" action="<?php echo base_url();?>administrator/recruiter/jobcategoryInsert" method="post" novalidate>
                            <div class="job">
                            </div>
                        </div>
                        <div class="newcustom">
                          <div class="preview">
                            <div class="preview-elements">
                              <table id="sample-data-table" class="table dataTable no-footer" role="grid" aria-describedby="sample-data-table_info" style="width: 855px;">
                                <thead>
                                  <tr role="row">
                                    <th class="secondary-text sorting_asc" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-sort="ascending" aria-label="
                                       ID
                                       : activate to sort column descending" style="width: 40px;">
                                      <div class="table-header"> <span class="column-title">ID</span>
                                      </div>
                                    </th>
                                    
                                    <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                       Name
                                       : activate to sort column ascending" style="width: 102px;">
                                      <div class="table-header"> <span class="column-title">Request By</span>
                                      </div>
                                    </th>

                                    <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                       Name
                                       : activate to sort column ascending" style="width: 102px;">
                                      <div class="table-header"> <span class="column-title">Company Name</span>
                                      </div>
                                    </th>
                                    
                                    <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" 
                                    rowspan="1" colspan="1" aria-label="
                                       Name
                                       : activate to sort column ascending" style="width: 102px;">
                                      <div class="table-header"> <span class="column-title">Request Type</span>
                                      </div>
                                    </th>

                                    <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" 
                                    rowspan="1" colspan="1" aria-label="
                                       Name
                                       : activate to sort column ascending" style="width: 102px;">
                                      <div class="table-header"> <span class="column-title">Request To</span>
                                      </div>
                                    </th>

                                    <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" 
                                    rowspan="1" colspan="1" aria-label="
                                       Name
                                       : activate to sort column ascending" style="width: 102px;">
                                      <div class="table-header"> <span class="column-title">Request To Name</span>
                                      </div>
                                    </th>

                                    <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" 
                                    rowspan="1" colspan="1" aria-label="
                                       Name
                                       : activate to sort column ascending" style="width: 102px;">
                                      <div class="table-header"> <span class="column-title">Request To Phone</span>
                                      </div>
                                    </th>

                                    <th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                       Action
                                       : activate to sort column ascending" style="width: 68px;">
                                      <div class="table-header"> <span class="column-title">Action</span>
                                      </div>
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php 
                                    $x1=1; 
                                    
                                    foreach($jobcategoryLists as $jobcategory) { 
                                  ?>
                                      <tr role="row" class="odd">
                                        <td class="sorting_1"><?php echo $x1;?></td>
                                        
                                        <td> <?php echo $jobcategory['nameby']; ?></td>

                                        <td> <?php echo $jobcategory['cname']; ?> </td>
                                        
                                        <td> <?php if($jobcategory['request_type'] == 1){ echo "Existing Recruiter"; }else{ echo "New Recruiter"; } ?> </td>

                                        <?php if($jobcategory['request_type'] == 1) {

                                              $reData = $this->Recruiteradmin_Model->recruiter_single($jobcategory['request_to']);
                                              $reMoreData = $this->Recruiteradmin_Model->company_details_single($jobcategory['request_to']);
                                        ?>

                                          <td> <?php echo $reData[0]['email']; ?> </td>
                                          <td> <?php echo $reData[0]['fname'].' '.$reData[0]['lname']; ?> </td>
                                          <td> <?php echo $reMoreData[0]['phone']; ?> </td>
                                        
                                        <?php } else { ?>
                                        
                                          <td> <?php echo $jobcategory['email']; ?> </td>
                                          <td> <?php echo $jobcategory['fname'].' '.$jobcategory['lname']; ?> </td>
                                          <td> <?php echo $jobcategory['phone']; ?> </td>
                                        
                                        <?php  } ?>

                                        <td> <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="btn" aria-label="Job Level"> Accept </a>
                                        </td>
                                      </tr>
                                  <?php 
                                    $x1++; 
                                    }
                                  ?>
                                </tbody>
                              </table>
                              <script type="text/javascript">
                                $('#sample-data-table').DataTable();
                              </script>
                            </div>
                          </div>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6"></nav>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Request Transfer</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure? Want to accept request transer.</p>

        <a href="<?php echo base_url(); ?>administrator/recruiter/recruitertransferaccepted?id=<?php echo $jobcategory['id'] ?>" class="btn" aria-label="Job Level"> Accept </a>

      </div>
    </div>

  </div>
</div>

</main>

</body>

</html>