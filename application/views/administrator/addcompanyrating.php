

<?php include('header.php'); ?>
<div class="content custom-scrollbar">
   <div class="doc data-table-doc page-layout simple full-width">
      <!-- HEADER -->
      <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
         <!-- APP TITLE -->
         <div class="col-12 col-sm">
            <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
               <div class="logo-icon mr-3 mt-1">
                  <i class="icon-cube-outline s-6"></i>
               </div>
               <div class="logo-text">
                  <div class="h4">Company Site Rating</div>
                  <!--<div class="">Total: <?php if($Lists){echo count($Lists); }?></div>-->
               </div>
            </div>
         </div>
         <!-- / APP TITLE -->
         <!--<div class="col-auto">
            <a href="<?php echo base_url();?>administrator/recruiter/addRecruiter" class="btn btn-light">ADD NEW</a>
            </div>-->
      </div>
      <!-- / HEADER -->
      <!-- CONTENT -->
      <div class="page-content p-6">
         <div class="content container">
            <div class="row">
               <div class="col-12">
                  <div class="example ">
                     <div class="source-preview-wrapper">
                        <div class="preview">
                           <div class="companydtls thumb siteview">
                              <form action="<?php echo base_url();?>administrator/recruiter/insertRating" method="post">
                              <div class="txtdesc">
                                 <div class="main-hda right">
                                  <label>Add Company Rating</label>
                                    <input type="number" name="ratings" required="required">
                                 </div>
                                 <div class="companymanagesd">
                                    <div class="clasetis">
                                      <input type="hidden" name="cid" value="<?php echo $cid;?>">
                                       <input type="submit" name="submit" value="Submit">
                                    </div>
                                    
                                 </div>
                              </div>

                           </div>
                        </div>

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- CONTENT -->
   </div>
</div>
</div>
<div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
   <div class="list-group" class="date">
      <div class="list-group-item subheader">TODAY</div>
      <div class="list-group-item two-line">
         <div class="text-muted">
            <div class="h1"> Friday</div>
            <div class="h2 row no-gutters align-items-start">
               <span> 5</span>
               <span class="h6">th</span>
               <span> May</span>
            </div>
         </div>
      </div>
   </div>
   <div class="divider"></div>
   <div class="list-group">
      <div class="list-group-item subheader">Events</div>
      <div class="list-group-item two-line">
         <div class="list-item-content">
            <h3>Group Meeting</h3>
            <p>In 32 Minutes, Room 1B</p>
         </div>
      </div>
      <div class="list-group-item two-line">
         <div class="list-item-content">
            <h3>Public Beta Release</h3>
            <p>11:00 PM</p>
         </div>
      </div>
      <div class="list-group-item two-line">
         <div class="list-item-content">
            <h3>Dinner with David</h3>
            <p>17:30 PM</p>
         </div>
      </div>
      <div class="list-group-item two-line">
         <div class="list-item-content">
            <h3>Q&amp;A Session</h3>
            <p>20:30 PM</p>
         </div>
      </div>
   </div>
   <div class="divider"></div>
   <div class="list-group">
      <div class="list-group-item subheader">Notes</div>
      <div class="list-group-item two-line">
         <div class="list-item-content">
            <h3>Best songs to listen while working</h3>
            <p>Last edit: May 8th, 2015</p>
         </div>
      </div>
      <div class="list-group-item two-line">
         <div class="list-item-content">
            <h3>Useful subreddits</h3>
            <p>Last edit: January 12th, 2015</p>
         </div>
      </div>
   </div>
   <div class="divider"></div>
   <div class="list-group">
      <div class="list-group-item subheader">Quick Settings</div>
      <div class="list-group-item">
         <div class="list-item-content">
            <h3>Notifications</h3>
         </div>
         <div class="secondary-container">
            <label class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" />
            <span class="custom-control-indicator"></span>
            </label>
         </div>
      </div>
      <div class="list-group-item">
         <div class="list-item-content">
            <h3>Cloud Sync</h3>
         </div>
         <div class="secondary-container">
            <label class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" />
            <span class="custom-control-indicator"></span>
            </label>
         </div>
      </div>
      <div class="list-group-item">
         <div class="list-item-content">
            <h3>Retro Thrusters</h3>
         </div>
         <div class="secondary-container">
            <label class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" />
            <span class="custom-control-indicator"></span>
            </label>
         </div>
      </div>
   </div>
</div>
</div>
<nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
   <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
      <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
      </a>-->
</nav>
</main>
<script type="text/javascript">
   function getrid(id) {
       var rid = id;
       var link = "<?php echo base_url();?>administrator/recruiter/deleteRecruiter?id="+rid;
       var aa = document.getElementById('createLink');
       aa.setAttribute("href", link);
   }
</script>
<style>
</style>
</body>
</html>


