<?php include('header.php'); ?>
<style type="text/css"></style>
<div class="content custom-scrollbar">
                  <div class="doc data-table-doc page-layout simple full-width">
                     <!-- HEADER -->
                     <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="mainheadicos">
                                 <i class="icon s-4 icon-account-search"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Hired Candidates</div>
                                 <div class="">Total: <?php if($hired_data){echo count($hired_data); }?></div>
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                        <div class="col-auto">
                          <!-- <a href="<?php echo base_url();?>administrator/recruiter/addRecruiter" class="btn btn-light">ADD NEW</a>-->
                        </div>
                     </div>
                     <!-- / HEADER -->
                     <!-- CONTENT -->
                     <div class="page-content p-6">
                        <div class="content container">
                           <div class="row">
                              <div class="col-12">
                                 <div class="example ">
                                    <div class="source-preview-wrapper">
                                       <div class="preview">
                                          <div class="preview-elements">
                                             <table id="sample-data-table" class="table">
                                                <thead>
                                                   <tr>
                                                      <th class="secondary-text" style="width:40px;">
                                                         <div class="table-header">
                                                            <span class="column-title">SNo</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Job Title</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text" >
                                                         <div class="table-header">
                                                            <span class="column-title">Site Name</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Candidate Name</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Basic Salary</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Total Guaranteed Allowance</span>
                                                         </div>
                                                      </th>
                                                       <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Job SubCategory</span>
                                                         </div>
                                                      </th>
                                                      
                                                      <!--<th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Active</span>
                                                         </div>
                                                      </th>
                                                     
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Delete</span>
                                                         </div>
                                                      </th> -->

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Job Posted By</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Candidate Hired By</span>
                                                         </div>
                                                      </th>

                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Hired Date</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Special Profile</span>
                                                         </div>
                                                      </th>
                                                      <th class="secondary-text">
                                                         <div class="table-header">
                                                            <span class="column-title">Final Cost Per Hire</span>
                                                         </div>
                                                      </th>
                                                   </tr>
                                                </thead>
                                                <tbody> 
                                                   <?php
                                                      $x1 =1;
                                                      foreach($hired_data as $List) {
                                                      ?>
                                                   <tr>
                                                      <td style="width:30px;"><?php echo $x1;?></td>
                                                      <td style="width:40px;"><?php echo $List['jobtitle'];?></td>
                                                      <td style="width:40px;"><?php echo $List['site_name'];?></td>
                                                      <td style="width:40px;"><?php echo $List['name'];?></td>
                                                      <td style="width:40px;"><?php  echo $List['salary'];?></td>   
                                                      <td style="width:40px;"><?php  echo $List['allowance'];?></td>   
                                                      <td style="width:40px;"><?php  echo $List['subcategory'];?></td>   
                                                      <td style="width:40px;"><?php  echo $List['fname'].' '.$List['lname'];?></td>   
                                                      <td style="width:40px;"><?php  echo $List['fname'].' '.$List['lname'];?></td>   
                                                      <td style="width:40px;"><?php  echo $List['updated_at'];?></td>   
                                                      <td style="width:40px;"><?php if($List['special']=='Yes'){
                                                         if($List['special']){
                                                            echo $List['special'];
                                                         ?>

                                                         <?php }else{?>
                                                       <a  data-toggle="modal" id="<?php echo $List['app_id'];?>" onclick="getBonus(this.id)" data-target="#modalBonus" class="btn btn-block btn-secondary fuse-ripple-ready" aria-label="Product details">Add</a> 
                                                      <?php }}else{ echo "No"; }?></td>   
                                                      <td style="width:40px;"><?php  echo $List['costing'];?></td>   
                                                      
                                                   </tr>
                                                   <?php
                                                      $x1++;
                                                      }
                                                      ?>
                                                </tbody>
                                             </table>
                                             <script type="text/javascript">
                                                $('#sample-data-table').DataTable();
                                             </script>
                                          </div>
                                       </div>
                                       <div class="source custom-scrollbar">
                                          <div class="highlight">
                                             <pre style="background-color:#fff;-moz-tab-size:4;-o-tab-size:4;tab-size:4"><code class="language-html" data-lang="html">
    
                                                    </code></pre>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- CONTENT -->
                  </div>
               </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
               <div class="list-group" class="date">
                  <div class="list-group-item subheader">TODAY</div>
                  <div class="list-group-item two-line">
                     <div class="text-muted">
                        <div class="h1"> Friday</div>
                        <div class="h2 row no-gutters align-items-start">
                           <span> 5</span>
                           <span class="h6">th</span>
                           <span> May</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Events</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Group Meeting</h3>
                        <p>In 32 Minutes, Room 1B</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Public Beta Release</h3>
                        <p>11:00 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Dinner with David</h3>
                        <p>17:30 PM</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Q&amp;A Session</h3>
                        <p>20:30 PM</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Notes</div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Best songs to listen while working</h3>
                        <p>Last edit: May 8th, 2015</p>
                     </div>
                  </div>
                  <div class="list-group-item two-line">
                     <div class="list-item-content">
                        <h3>Useful subreddits</h3>
                        <p>Last edit: January 12th, 2015</p>
                     </div>
                  </div>
               </div>
               <div class="divider"></div>
               <div class="list-group">
                  <div class="list-group-item subheader">Quick Settings</div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Notifications</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Cloud Sync</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
                  <div class="list-group-item">
                     <div class="list-item-content">
                        <h3>Retro Thrusters</h3>
                     </div>
                     <div class="secondary-container">
                        <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-indicator"></span>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
            <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
         </nav>
      </main>


 <!-- Modal -->
<div id="modalBonus" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <h3>Add More Amount</h3>
        <form method="post" action="<?php echo base_url();?>administrator/Recruiter/bonusamountInsert">
            <div class="form-group">
                <input type="number" name="bonus_amount" class="form-control" placeholder="Enter Amount" required="">
                <label>Amount</label>
                <input type="hidden" name="appId" id="userId">
                <input type="hidden" name="rId" value="<?php echo $_GET['id'] ?>">
            </div>
            
            <button type="submit" class="btn btn-info">Submit</button>
            
        </form>

      </div>
      <div class="modal-footer">
         
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
    function getrid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/deleteJobSeeker?id="+rid;
        var aa = document.getElementById('createLink');
        aa.setAttribute("href", link);
    }
    function getbid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/jobseekerBlock?id="+rid;
        var aa = document.getElementById('createbLink');
        aa.setAttribute("href", link);
    }

    function getaid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/jobseekerActive?id="+rid;
        var aa = document.getElementById('createaLink');
        aa.setAttribute("href", link);
    }

    function getuid(id) {
        var rid = id;
        var link = "<?php echo base_url();?>administrator/JobSeeker/withdrawBonus?id="+rid;
        var aa = document.getElementById('createwLink');
        aa.setAttribute("href", link);
        $.ajax({
         type:"POST",
         url:"<?php echo base_url(); ?>/administrator/JobSeeker/fetchBonusAmount",
         data:{
            rid:rid
         },
         success:function(data){
            if(data>0){
               $('#bonus_message').html('Do you want to withdraw the Bonus Amount of'+ data +' Peso ?');
               $('#createwLink').show();
            }else{
               $('#bonus_message').html('You donot have sufficient balance to withdraw');
               $('#createwLink').hide();
            }
            
         }
        });
    }

    function getBonus(rid){
      var userId = document.getElementById('userId');
      userId.setAttribute("value",rid);
    }

    function getid2(id) {
        var rid = id;
        //alert(id);
          var url =   '<?php echo base_url(); ?>administrator/JobSeeker/fetchbonusHistory';
       
          $.ajax({
                   type:"POST",
                   url:url,
                   data:{
                       rid : rid
                   },

                   success:function(data)

                    {
                     //alert(data);
                     
                  $("#bonus_history").html(data);
                    }
                });
    }


</script>
   </body>
</html>

