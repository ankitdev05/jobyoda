<style type="text/css">
  .validError{
    color: red;
  }
</style>
<?php include('header.php'); 
//print_r($jobtitleLists);
?>
<div class="content custom-scrollbar">
<div class="doc data-table-doc page-layout simple full-width">

 <div class="page-header bg-primary text-auto p-6 row no-gutters align-items-center justify-content-between">
                        <!-- APP TITLE -->
                        <div class="col-12 col-sm">
                           <div class="logo row no-gutters justify-content-center align-items-start justify-content-sm-start">
                              <div class="logo-icon mr-3 mt-1">
                                 <i class="icon s-6 icon-briefcase-upload"></i>
                              </div>
                              <div class="logo-text">
                                 <div class="h4">Update Price</div>
                                 
                              </div>
                           </div>
                        </div>
                        <!-- / APP TITLE -->
                       
                     </div>
					 <div class="page-content p-6">
					 <div class="content container">
                           <div class="row">
                              <div class="col-12">
					 
					 <div class="example ">
					 
					  <div class="source-preview-wrapper">
                                       <div class="preview">
									    <div class="preview-elements">
                    <div id="registers">

                        <div class="form-wrapper">

                          
                    <div class="mainjob">
                            <form name="registerForm" action="<?php echo base_url();?>administrator/recruiter/advideopriceInsert" method="post" novalidate>
                        <div class="job">
                                <div class="form-group mb-4">
                                    <input type="number" name="price" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" required="required" / value="<?php echo $jobtitleLists[0]['price'] ?>">
                                    <label for="registerFormInputName">Price</label>
                                    <?php if(!empty($errors['price'])){?>
                                    <span class="validError"><?php echo $errors['price']; ?></span>
                                    <?php } ?>
                                </div>
						</div>		
                           <div class="jobbtn">     
                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                    Update
                                </button>
							</div>	
							</div>
                       <!--         <div class="newcustom">
							      <div class="preview">
                                          <div class="preview-elements">
                                           
											                      <table id="sample-data-table" class="table dataTable no-footer" role="grid" aria-describedby="sample-data-table_info" style="width: 855px;">
                                                <thead>
                                                   <tr role="row"><th class="secondary-text sorting_asc" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-sort="ascending" aria-label="
                                                         
                                                            ID
                                                         
                                                      : activate to sort column descending" style="width: 40px;">
                                                         <div class="table-header">
                                                            <span class="column-title">ID</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Name
                                                         
                                                      : activate to sort column ascending" style="width: 102px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Price</span>
                                                         </div>
                                                      </th><th class="secondary-text sorting" tabindex="0" aria-controls="sample-data-table" rowspan="1" colspan="1" aria-label="
                                                         
                                                            Action
                                                         
                                                      : activate to sort column ascending" style="width: 68px;">
                                                         <div class="table-header">
                                                            <span class="column-title">Action</span>
                                                         </div>
                                                      </th></tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $x1 =1;
                                                    foreach($jobtitleLists as $jobtitles){
                                                    ?>
                                                    <tr role="row" class="odd">
                                                      <td class="sorting_1"><?php echo $x1;?></td>
                                                      <td><?php echo $jobtitles['price'];?></td>
                                                      
                                                      
                                                      
                                                      <td>
													  
														 <a href="<?php echo base_url(); ?>administrator/recruiter/deletetitle?id=<?php echo $jobtitles['id'] ?>" class="btn btn-icon usricos fuse-ripple-ready" aria-label="Product details"><i class="icon s-4 icon-trash"></i></a>  
														 	 
                                                      </td> 
                                                      
                                                   </tr>
                                                   <?php $x1++;
                                                   }?>
                                                   </tbody>
                                             </table>
                                             <script type="text/javascript">
                                                $('#sample-data-table').DataTable();
                                             </script>
                                          </div>
                                       </div>
							   </div> -->
                            </form>
                        </div>
                    </div>
					</div>
					
					</div>
					</div>
					</div>
					</div>
					</div>
					</div>
					
					</div>
</div>
                </div>
            
        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
        </nav>
    </main>
</body>

</html>