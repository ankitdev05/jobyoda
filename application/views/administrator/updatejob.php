<style type="text/css">
    .validError{
        color: red;
    }

.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

p.addmre {
    background: #46a56f;
    display: inline-block;
    color: #fff;
    padding: 4px 5px;
    margin-top: 16px;
    box-shadow: #bdb9b9 1px 2px 5px 0px;
    border-radius: 3px;
}

ul.multiselect-container.dropdown-menu {
    padding: 7px;
        width: 240px;
}

ul.multiselect-container.dropdown-menu input[type="checkbox"] {
    height: auto;
    width: auto;
    position: relative;
    opacity: 1;
}

.screeningurlRadio {
    position: relative!important;
    opacity: 1!important;
    z-index: 9!important;
    margin: 0!important;
    left: 0!important;
    right: 0!important;
    padding: 0px 0!important;
    height: 15px!important;
    width: 20px!important;
    margin: auto!important;
}
</style>
<?php include('header.php'); ?>
                <div class="content custom-scrollbar">

                    <div id="register" class="p-8">

                        <div class="form-wrapper md-elevation-8 p-8">

                            <div class="title mt-4 mb-8">Update Job Post</div>

                            <form name="registerForm" action="<?php echo base_url();?>administrator/jobpost/jobPostUpdate" method="post" novalidate>

                                <div class="form-group mb-4">
                                    <input type="text" name="jobTitle" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" value="<?php if($getJobs[0]['jobtitle']){echo $getJobs[0]['jobtitle'];}?>" />
                                    <label for="registerFormInputName">Job Title</label>
                                    <?php if(!empty($errors['jobTitle'])){echo "<span class='validError'>".$errors['jobTitle']."</span>";}?>
                                </div>
                                 <div class="form-group mb-4">
                                     <label for="registerFormInputName">Job Location</label>
                                    <select class="form-control locselect" name="jobLocation" id="jobLoc">
                                      <option> Select Location </option>
                                      <?php
                                         if($addresses) {
                                             foreach($addresses as $address) {
                                         ?>
                                      <option value="<?php echo $address['recruiter_id']; ?>" <?php if($getJobs[0]['company_id'] == $address['recruiter_id']){ echo "selected";}?> > <?php echo $address['cname']; ?> </option>
                                      <?php
                                         }     
                                         }
                                         ?>
                                   </select>
                                    <!-- <input type="text" name="jobLocation" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" value="<?php if(!empty($getJobloc[0]['address'])){echo $getJobloc[0]['address'];}?>" /> -->
                                   
                                     <?php if(!empty($errors['jobLocation'])){echo "<span class='validError'>".$errors['jobLocation']."</span>";}?>
                                </div>
                                <div class="form-group mb-4">
                                    <input type="number" name="opening" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" value="<?php if($getJobs[0]['opening']){echo $getJobs[0]['opening'];}?>" />
                                    <label for="registerFormInputName">No. of openings</label>
                                    <?php if(!empty($errors['opening'])){echo "<span class='validError'>".$errors['opening']."</span>";}?>
                                </div>
                                <div class="form-group mb-4">
                                    <input type="date" name="jobExpire" class="form-control" id="registerFormInputPasswordConfirm" value="<?php echo $getJobs[0]['jobexpire'];?>"/>
                                    <label for="registerFormInputPasswordConfirm">Job Expire</label>
                                    <?php if(!empty($errors['jobExpire'])){echo "<span class='validError'>".$errors['jobExpire']."</span>";}?>
                                </div>
                                <div class="form-group mb-4" style="display: flex;">
                                     <label for="registerFormInputName">Select Level</label>
                              <select class="form-control locselect" name="level" id="jobLoc" style="width:79%; margin-right: 11px;">
                                      <option> Select Level </option>
                                      <?php
                                         if($levels) {
                                             foreach($levels as $level) {
                                         ?>
                                      <option value="<?php echo $level['id']; ?>" <?php if(!empty($getJobs[0]['level'])){ if($getJobs[0]['level'] == $level['id']) {echo "selected";}} ?>> <?php echo $level['level']; ?> </option>
                                      <?php
                                         }
                                         }
                                         ?>
                                   </select>
                                    <label class="switch" style="width:66px;">
                                        <input type="checkbox" name="level_status" <?php if(!empty($getJobs[0]['level_status'])){ if($getJobs[0]['level_status']=="1"){ echo "checked"; } else{ echo ""; } } ?> >
                                        <span class="slider round" title="If turned ON, applicants who are not meeting these requirements will be automatically rejected."></span>
                                   </label>
                                   
                                     <?php if(!empty($errors['level'])){echo "<span class='validError'>".$errors['level']."</span>";}?>
                                </div>
                                <div class="form-group mb-4" style="display: flex;">
                                     <label for="registerFormInputName">Select Education Level</label>
                                    <select name="education" class="form-control halfsideth" style="width:79%; margin-right: 11px;">
                                                      <option value=""> Select Education Level </option>
                                                      <option value="No Requirement" <?=$getJobs[0]['education'] == 'No Requirement' ? ' selected="selected"' : '';?>>No Requirement</option>
                                                      <option value="Vocational" <?=$getJobs[0]['education'] == 'Vocational' ? ' selected="selected"' : '';?>>Vocational</option>
                                                      <option value="High School Graduate" <?=$getJobs[0]['education'] == 'High School Graduate' ? ' selected="selected"' : '';?>>High School Graduate</option>
                                                      
                                                      <option value="Undergraduate" <?=$getJobs[0]['education'] == 'Undergraduate' ? ' selected="selected"' : '';?>>Undergraduate</option>
                                                      <option value="Associate Degree" <?=$getJobs[0]['education'] == 'Associate Degree' ? ' selected="selected"' : '';?>>Associate Degree</option>
                                                      <option value="College Graduate" <?=$getJobs[0]['education'] == 'College Graduate' ? ' selected="selected"' : '';?>>College Graduate</option>
                                                      <option value="Post Graduate" <?=$getJobs[0]['education'] == 'Post Graduate' ? ' selected="selected"' : '';?>>Post Graduate</option>
                                                   </select>
                                                   <label class="switch" style="width:66px;">
                                                        <input type="checkbox" name="education_status" <?php if(!empty($getJobs[0]['education_status'])){ if($getJobs[0]['education_status']=="1"){ echo "checked"; } else{ echo ""; } } ?>>
                                                        <span class="slider round" title="If turned ON, applicants who are not meeting these requirements will be automatically rejected."></span>
                                                   </label>
                                </div>
                                <div class="form-group mb-4">
                                    <input type="text"  class="form-control" id="registerFormInputName" aria-describedby="nameHelp" name="certification" placeholder="Enter Certifications" value="<?php if(!empty($getJobs[0]['certification'])){ echo $getJobs[0]['certification'];}?>">
                                    <label for="registerFormInputName">Certifications</label>
                                    <!-- <?php if(!empty($errors['certification'])){ echo "<span class='validError'>".$errors['certification']."</span>"; }?> -->
                                </div>
                                <div class="form-group mb-4" style="display: flex;">
                                  <label for="registerFormInputName">Select Experience</label>
                                    <select name="experience" id="requiredexp" class="form-control halfsideth" style="width:79%; margin-right: 11px;">
                                      <option value=""> Select Experience </option>
                                      <option value="All Tenure" <?=$getJobs[0]['experience'] == 'All Tenure' ? ' selected="selected"' : '';?>>All Tenure</option>
                                      <option value="No Experience" <?=$getJobs[0]['experience'] == 'No Experience' ? ' selected="selected"' : '';?>>No Experience</option>
                                      
                                      <option value="< 6 months" <?=$getJobs[0]['experience'] == '< 6 months' ? ' selected="selected"' : '';?>>< 6 months</option>
                                      <option value="> 6 months" <?=$getJobs[0]['experience'] == '> 6 months' ? ' selected="selected"' : '';?>>> 6 months</option>
                                      <option value="> 1 yr" <?=$getJobs[0]['experience'] == '> 1 yr' ? ' selected="selected"' : '';?>>> 1 yr</option>
                                      <option value="> 2 yr" <?=$getJobs[0]['experience'] == '> 2 yr' ? ' selected="selected"' : '';?>>> 2 yr</option>
                                      <option value="> 3 yr" <?=$getJobs[0]['experience'] == '> 3 yr' ? ' selected="selected"' : '';?>>> 3 yr</option>
                                      <option value="> 4 yr" <?=$getJobs[0]['experience'] == '> 4 yr' ? ' selected="selected"' : '';?>>> 4 yr</option>
                                      <option value="> 5 yr" <?=$getJobs[0]['experience'] == '> 5 yr' ? ' selected="selected"' : '';?>>> 5 yr</option>
                                      <option value="> 6 yr" <?=$getJobs[0]['experience'] == '> 6 yr' ? ' selected="selected"' : '';?>>> 6 yr</option>
                                      <option value="> 7 yr" <?=$getJobs[0]['experience'] == '> 7 yr' ? ' selected="selected"' : '';?>>> 7 yr</option>
                                   </select>
                                   <label class="switch" style="width: 66px;">
                                        <input type="checkbox" name="experience_status" <?php if(!empty($getJobs[0]['experience_status'])){ if($getJobs[0]['experience_status']=="1"){ echo "checked"; } else{ echo ""; } } ?>>
                                        <span class="slider round" title="If turned ON, applicants who are not meeting these requirements will be automatically rejected."></span>
                                   </label>
                                    <?php if(!empty($errors['experience'])){echo "<span class='validError'>".$errors['experience']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                    <table id="myTable">
                                       <tbody>
                                      <?php
                                        if($jobExps) {
                                          foreach($jobExps as $getJobexp) {
                                      ?>
                                          <tr>
                                             <td>
                                                <select name="expRange[]" class="form-control">
                                                   <option  <?=$getJobexp['grade_id'] == '0' ? ' selected="selected"' : '';?> value="0">All Tenure</option>
                                                   <option <?=$getJobexp['grade_id'] == '1' ? ' selected="selected"' : '';?> value="1">Minimum Experience</option>
                                                   <option <?=$getJobexp['grade_id'] == '2' ? ' selected="selected"' : '';?> value="2">less than 6 months</option>
                                                   <option <?=$getJobexp['grade_id'] == '3' ? ' selected="selected"' : '';?> value="3">6mo to 1 yr</option>
                                                   <option <?=$getJobexp['grade_id'] == '4' ? ' selected="selected"' : '';?> value="4">1 yr to 2 yr</option>

                                                   <option <?=$getJobexp['grade_id'] == '5' ? ' selected="selected"' : '';?> value="5">2 yr to 3 yr</option>
                                                   <option <?=$getJobexp['grade_id'] == '6' ? ' selected="selected"' : '';?> value="6">3 yr to 4 yr</option>
                                                   <option <?=$getJobexp['grade_id'] == '7' ? ' selected="selected"' : '';?> value="7">4 yr to 5 yr</option>
                                                   <option <?=$getJobexp['grade_id'] == '8' ? ' selected="selected"' : '';?> value="8">5 yr to 6 yr</option>
                                                   <option <?=$getJobexp['grade_id'] == '9' ? ' selected="selected"' : '';?> value="9">6 yr to 7 yr</option>
                                                   <option <?=$getJobexp['grade_id'] == '10' ? ' selected="selected"' : '';?> value="10">7 yr & up</option>
                                                </select>
                                             </td>
                                             <td><input type="number" class="form-control" name="expBasicSalary[]" value="<?php if(!empty($getJobexp['basicsalary'])){ echo $getJobexp['basicsalary'];}?>" placeholder="Basic Salary"></td>
                                          </tr>
                                        <?php
                                          }}
                                        ?>
                                       </tbody>
                                    </table>
                                    <p onclick="myFunction()" class="addmre">Add More</p>
                                    <!-- <input type="email" name="salaryOffered" class="form-control" id="registerFormInputEmail" aria-describedby="emailHelp" value="<?php if($getJobs[0]['salary']){echo $getJobs[0]['salary'];}?>" />
                                    <label for="registerFormInputEmail">Salary Offered</label>
                                    <?php if(!empty($errors['salaryOffered'])){echo "<span class='validError'>".$errors['salaryOffered']."</span>";}?> -->
                                </div>


                                <div class="form-group mb-4">
                                    <label for="registerFormInputEmail">Interview Mode?</label>
                                    
                                    <input  class="screeningurlRadio" type="radio" name="modecheck" value="Walk-in" <?php if(!empty($getJobs[0]['mode']) && $getJobs[0]['mode'] =="Walk-in"){ echo "checked";}?>> <span>Walk-in</span>
                                    
                                    <input  class="screeningurlRadio" type="radio" name="modecheck" value="Call" <?php if(!empty($getJobs[0]['mode']) && $getJobs[0]['mode'] =="Call"){ echo "checked";}?>> <span>Call</span>

                                    <input  class="screeningurlRadio" type="radio" name="modecheck" value="Instant screening" <?php if(!empty($getJobs[0]['mode']) && $getJobs[0]['mode'] =="Instant screening"){ echo "checked";}?>> <span>Instant screening</span>
                                    
                                </div>

                                <div class="form-group mb-4" id="screeningurl" <?php if(!empty($getJobs[0]['mode']) && $getJobs[0]['mode'] =="Instant screening"){} else { ?> style="display: none;" <?php } ?> >
                                    <input type="text" name="modeurl" class="form-control" id="registerFormInputName1" aria-describedby="nameHelp1" value="<?php if($getJobs[0]['modeurl']){echo $getJobs[0]['modeurl'];}?>" />
                                    <label for="registerFormInputName1">Instant screening URL</label>      
                                </div>


                                <div class="form-group mb-4">
                                    <label for="registerFormInputEmail">Category</label>
                                    <select name="category" class="form-control" id="category" aria-describedby="nameHelp" />
                                        <option value=""> Select Category</option>
                                    <?php
                                     if($category) {
                                         foreach($category as $categorys) {
                                     ?>
                                  <option value="<?php echo $categorys['id']; ?>" <?php if(!empty($getJobs[0]['category'])){ if($getJobs[0]['category'] == $categorys['id']) {echo "selected";}} ?>> <?php echo $categorys['category']; ?> </option>
                                  <?php
                                     }
                                     }
                                     ?>
                                    </select>    
                                </div>

                                <div class="form-group mb-4">
                                    <label for="registerFormInputEmail">Subcategory</label>
                                    <select name="subcategory" class="form-control" id="subcategory" aria-describedby="nameHelp" />
                                  <?php
                                     foreach($subcategory as $subcategorys) {
                                     ?>
                                  <option value="<?php echo $subcategorys['id'];?>" <?php if(!empty($getJobs[0]['subcategory'])){ if($getJobs[0]['subcategory'] == $subcategorys['id']) {echo "selected";}} ?>><?php echo $subcategorys['subcategory'];?></option>
                                  <?php }?>
                               </select>
                                   
                                    <?php if(!empty($errors['channel'])){echo "<span class='validError'>".$errors['channel']."</span>";}?>
                                </div>
                                <div class="form-group mb-4">
                                    <label for="registerFormInputEmail">Language</label>
                                    <select name="lang" class="form-control" id="registerFormInputName" aria-describedby="nameHelp" />
                                        <option> Select Language </option>
                                    <?php
                                        foreach($langs as $lang) {
                                    ?>
                                        <option value="<?php echo $lang['id'];?>" <?php if($getJobs[0]['language']){ if($getJobs[0]['language'] == $lang['id']){ echo "selected";}} ?>> <?php echo $lang['name'];?></option>
                                    <?php
                                        }
                                    ?>
                                    </select>
                                    
                                </div>
                                <div class="form-group mb-4">
                                    <label for="registerFormInputEmail">Other Language</label>
                                    <div class="autocomplete">
                                      <input id="myInput" type="text" name="otherlanguage" placeholder="Select Languange" class="form-control" value="<?php if(!empty($getJobs[0]['other_language'])){ echo $getJobs[0]['other_language'];}?>">
                                   </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label for="registerFormInputEmail">Job Description</label>
                                    <textarea name="jobDesc" class="form-control"/><?php echo $getJobs[0]['jobDesc'];?></textarea>
                                    <?php if(!empty($errors['jobDesc'])){echo "<span class='validError'>".$errors['jobDesc']."</span>";}?>
                                </div>

                                <div class="form-group mb-4">
                                <label for="registerFormInputPasswordConfirm">Skills</label>
                                    <select name="skills[]" id="multiselect" multiple="multiple" class="form-control">
                                      <!-- <option value=""> Select Skill </option> -->
                                      <?php //print_r($skills2);
                                         if($skills) {
                                             foreach($skills as $skill) {
                                         ?>
                                      <option  value="<?php echo $skill['id']; ?>" <?php if(!empty($skills2)){ if(in_array($skill['id'], $skills2)){ echo "selected";}}?>> <?php echo $skill['skill']; ?> </option>
                                      <?php
                                         }
                                         }
                                         ?>
                                         <option value="12" <?php if(!empty($skills2)){ if(in_array('12', $skills2)){ echo "selected";}}?>>Others</option>
                                   </select>
                                    
                                    <?php if(!empty($errors['skills'])){echo "<span class='validError'>".$errors['skills']."</span>";}?>
                                    <?php if(!empty($getJobs[0]['skills'])){?>
                                       <label style="margin-top: 20px;">Other Skills</label>
                                      <input type="text" class="form-control" name="skill" placeholder="Other Skills" value="<?php if(!empty($getJobs[0]['skills'])){ echo $getJobs[0]['skills'];}?>" autocomplete="off" >
                                      <?php }else{?>
                                      <label  style="margin-top: 20px;">Other Skills</label>
                                      <input type="text" class="form-control" name="skill" id="other_skill" placeholder="Enter Other Skills" value="" style="visibility: hidden;opacity: 0;" autocomplete="off">
                                      <?php }?>
                                </div>

                                <div class="form-group mb-4">
                                     <textarea maxlength="2000" class="form-control" placeholder="Make it count! This will attract jobseekers to view your job posting!" name="jobPitch"><?php if(!empty($getJobs[0]['jobPitch'])){ echo $getJobs[0]['jobPitch'];}?></textarea>
                                    <label for="registerFormInputPasswordConfirm">Job Pitch</label>
                                    <?php if(!empty($errors['jobPitch'])){echo "<span class='validError'>".$errors['jobPitch']."</span>";}?>
                                </div>
                                

                                <!-- <div class="form-group mb-4">
                                    <label for="registerFormInputEmail">Company Details</label>
                                    <textarea name="compDetail" class="form-control"/><?php echo $getJobs[0]['companydetail'];?></textarea>
                                    <?php if(!empty($errors['compDetail'])){echo "<span class='validError'>".$errors['compDetail']."</span>";}?>
                                </div> -->

                                <input type="hidden" name="rid" value="<?php echo $getJobs[0]['id'];?>">
                                <input type="hidden" name="role" value="<?php echo $getJobs[0]['company_id'];?>">
                                <button type="submit" class="submit-button btn btn-block btn-secondary my-4 mx-auto" aria-label="LOG IN">
                                    Update JOBPOST
                                </button>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <div class="quick-panel-sidebar custom-scrollbar" fuse-cloak data-fuse-bar="quick-panel-sidebar" data-fuse-bar-position="right">
                <div class="list-group" class="date">

                    <div class="list-group-item subheader">TODAY</div>

                    <div class="list-group-item two-line">

                        <div class="text-muted">

                            <div class="h1"> Friday</div>

                            <div class="h2 row no-gutters align-items-start">
                                <span> 5</span>
                                <span class="h6">th</span>
                                <span> May</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Events</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Group Meeting</h3>
                            <p>In 32 Minutes, Room 1B</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Public Beta Release</h3>
                            <p>11:00 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Dinner with David</h3>
                            <p>17:30 PM</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Q&amp;A Session</h3>
                            <p>20:30 PM</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Notes</div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Best songs to listen while working</h3>
                            <p>Last edit: May 8th, 2015</p>
                        </div>
                    </div>

                    <div class="list-group-item two-line">

                        <div class="list-item-content">
                            <h3>Useful subreddits</h3>
                            <p>Last edit: January 12th, 2015</p>
                        </div>
                    </div>

                </div>

                <div class="divider"></div>

                <div class="list-group">

                    <div class="list-group-item subheader">Quick Settings</div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Notifications</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Cloud Sync</h3>
                        </div>

                        <div class="secondary-container">
                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>

                    </div>

                    <div class="list-group-item">

                        <div class="list-item-content">
                            <h3>Retro Thrusters</h3>
                        </div>

                        <div class="secondary-container">

                            <label class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" />
                                <span class="custom-control-indicator"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <nav id="footer" class="bg-dark text-auto row no-gutters align-items-center px-6">
            <!--<a class="btn btn-secondary text-capitalize" href="http://themeforest.net/item/fuse-angularjs-material-design-admin-template/12931855?ref=srcn" target="_blank">
                <i class="icon icon-cart mr-2 s-4"></i>Purchase FUSE Bootstrap
            </a>-->
        </nav>
    </main>
    <script type="text/javascript">

    $(document).ready(function () {
    $('#multiselect').multiselect({
      includeSelectAllOption: true,
      nonSelectedText: 'Select an Option' });

    $('#multiselect').on('change', function() {

        //alert($(this).val());
    if($("#multiselect").val().includes("12")){
        $('#other_skill').css('visibility','visible');
        $('#other_skill').css('opacity','1');
    }else{
        $('#other_skill').css('visibility','hidden');
        $('#other_skill').css('opacity','0');
    }
  });

  });

    function getSelectedValues() {
  var selectedVal = $("#multiselect").val();
  for (var i = 0; i < selectedVal.length; i++) {
    function innerFunc(i) {
      setTimeout(function () {
        location.href = selectedVal[i];
      }, i * 2000);
    }
    innerFunc(i);
  }
}
        $("#category").change(function(){
              //get category value
              var cat_val = $("#category").val();
              //alert(cat_val);
              // put your ajax url here to fetch subcategory
              var url             =   '<?php echo base_url(); ?>/recruiter/Jobpost/fetchSubcategory';
              // call subcategory ajax here 
              $.ajax({
                             type:"POST",
                             url:url,
                             data:{
                                 cat_val : cat_val
                             },
         
                             success:function(data)
                              {
                                  $("#subcategory").html(data);
                              }
                          });
          });
    </script>

    <script>
         function myFunction() {
           var table = document.getElementById("myTable");
           var rows = document.getElementById("myTable").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
           
           
           var row = table.insertRow(rows);
           var cell1 = row.insertCell(0);
           var cell2 = row.insertCell(1);
           var exp_select = $('#requiredexp').val();

           cell1.innerHTML = '<select name="expRange[]" class="form-control expgrid"><option value="">Select Experience</option>';
            if(exp_select=='All Tenure'){
                  cell1.innerHTML = '<select name="expRange[]" class="form-control expgrid"><option value="">Select Experience</option><option value="0">All Tenure</option></select>';
            }else if(exp_select=='No Experience'){
                  cell1.innerHTML = '<select name="expRange[]" class="form-control expgrid"><option value="">Select Experience</option><option value="1">Minimum Experience</option></select>';
            } 
            else if(exp_select=='< 6 months'){
                  cell1.innerHTML = '<select name="expRange[]" class="form-control expgrid"><option value="">Select Experience</option><option value="1">Minimum Experience</option><option value="2">less than 6 months</option></select>';
            } else if(exp_select=='> 6 months'){
                  cell1.innerHTML ='<select name="expRange[]" class="form-control expgrid"><option value="">Select Experience</option><option value="3">6mo to 1 yr</option><option value="4">1 yr to 2 yr</option><option value="5">2 yr to 3 yr</option><option value="6">3 yr to 4 yr</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            }  
            else if(exp_select=='> 1 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="form-control expgrid"><option value="">Select Experience</option><option value="4">1 yr to 2 yr</option><option value="5">2 yr to 3 yr</option><option value="6">3 yr to 4 yr</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            } 
            else if(exp_select=='> 2 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="form-control expgrid"><option value="">Select Experience</option><option value="5">2 yr to 3 yr</option><option value="6">3 yr to 4 yr</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            } else if(exp_select=='> 3 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="form-control expgrid"><option value="">Select Experience</option><option value="6">3 yr to 4 yr</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            } else if(exp_select=='> 4 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="form-control expgrid"><option value="">Select Experience</option><option value="7">4 yr to 5 yr</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            }  
             else if(exp_select=='> 5 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="form-control expgrid"><option value="">Select Experience</option><option value="8">5 yr to 6 yr</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            } 
             else if(exp_select=='> 6 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="form-control expgrid"><option value="">Select Experience</option><option value="9">6 yr to 7 yr</option><option value="10">7 yr & up</option></select>';
            }
            else if(exp_select=='> 7 yr'){
                  cell1.innerHTML ='<select name="expRange[]" class="form-control expgrid"><option value="">Select Experience</option><option value="10">7 yr & up</option></select>';
            }        
           /*cell1.innerHTML = '<select name="expRange[]"><option value="">Select Experience</option><option value="All Tenure">All Tenure</option><option value="less than 6 months">less than 6 months</option><option value="6mo to 1 yr">6mo to 1 yr</option><option value="1 yr to 2 yr">1 yr to 2 yr</option><option value="2yr to 3 yr">2yr to 3 yr</option><option value="3yr and up">3yr and up</option></select>';*/
           cell2.innerHTML = "<input type='text' name='expBasicSalary[]' class='form-control' placeholder='Salary per Month'/><a href='javascript:void(0);' class='remove'><i class='fa fa-trash'></i></a>";
         }
         
         $(document).on("click", "a.remove" , function() {
            $(this).parent().parent().remove();
        });

         $(document).ready(function() {
         $('.screeningurlRadio').on('click', function() {
               var selectedValue = $(this).val();
               if(selectedValue == "Instant screening") {
                  $('#screeningurl').css('display','flex');
               } else {
                  $('#screeningurl').css('display','none');
               }
         });
   });
      </script>
      <script src='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js'></script>
</body>

</html>