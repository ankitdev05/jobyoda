<?php include_once('header.php'); ?>

<div class="managerpart">
     <div class="container-fluid">
        <div class="row">
           <div class="col-md-3 sideshift">
		   <div class="dashboardleftmenu">
                     <div class="menuinside">
                        <?php include_once('sidebar.php'); ?>
                     </div>
                     <div class="skillgraph">
                        <h6>Skills Percentage</h6>
                        <p>Put value for "Cover Image" field to <br>
                           increase your skill up to "15%"
                        </p>
                      <!--   <img src="<?php //echo base_url().'webfiles/';?>img/skillpercent.png"> -->
                       <link rel="stylesheet" href="<?php echo base_url().'webfiles/';?>css/progresscircle.css">
    <style>
      #demo {font-family: 'Roboto'; background-color:#fefefe; padding:2em;}
      .circlechart {
         float: left;
         padding: 20px;
      }

   </style> 

   <div id="demo">
     <div class="circlechart" data-percentage="<?php if(!empty($getCompletenes)){
      echo $getCompletenes['comp'];
     }?>"></div>
 
   </div>
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
   
   <script src="<?php echo base_url().'webfiles/';?>js/progresscircle.js"></script>
   <script>
      $('.circlechart').circlechart(); // Initialization
   </script>
    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
                     </div>
                  </div>
				  
		   </div>
		   <div class="col-md-9 expanddiv">
		   <div class="innerbglay">
			 <div class="mainheadings-yoda">
                     <h5>About Us</h5>
                  </div>
				 <div class="contentare">
            <?php echo $content[0]['content']; ?>
				 <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
	
	
	<p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
	
	<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p> -->
	   </div>
	   
		   </div>
		   </div>
		</div>
	 </div>
</div>

    <?php include_once('footer.php'); ?>