<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Dashboard extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('recruiter/Candidate_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');

        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
    }
    
    public function index() {
        $userSession = $this->session->userdata('userSession');
        if(isset($_POST['filters'])){
            $filters = $_POST['filters'];
        }else{
            $filters = "Active";
        }
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $subrecruiter_id = $userSession['id'];
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
            $subrecruiter_id=0;
        }
        $companySites = $this->Recruit_Model->company_sites($userSession['id']);

        $compData = array();

        foreach($companySites as $companySite) {
            $cid = $companySite['id'];
            $hiredcount = $this->Recruit_Model->hired_count($cid,$subrecruiter_id,$filters);
            //echo $this->db->last_query(); 
            $rejectedcount = $this->Recruit_Model->rejected_count($cid,$subrecruiter_id,$filters);
            $acceptedcount = $this->Recruit_Model->acceptedApplication_count($cid,$subrecruiter_id,$filters);
            //echo $this->db->last_query(); die;
            $allcount = $this->Recruit_Model->allApplication_count($cid,$subrecruiter_id,$filters);
            $newcount = $this->Recruit_Model->newApplication_count($cid,$subrecruiter_id,$filters);
            $hiredcandidatecount = $this->Recruit_Model->hiredApplication_count($cid,$subrecruiter_id,$filters);
            
            //echo $this->db->last_query();die;
            //print_r($allcount);
            if($allcount) {
               if($hiredcount[0]['hiredCount'] != 0) {
                  //$hiredPer = round($hiredcount[0]['hiredCount'] / $allcount[0]['rejectCount'] * 100);
                $hiredPer = round($hiredcount[0]['hiredCount']);
               } else {
                  $hiredPer = 0;
               }
               //echo $rejectedcount[0]['rejectCount'];die;
               if($rejectedcount[0]['rejectCount'] != 0) {
                //echo $allcount[0]['rejectCount'];die;
                  //$rejectPer = round($rejectedcount[0]['rejectCount'] / $allcount[0]['rejectCount'] * 100);
                $rejectPer = round($rejectedcount[0]['rejectCount']);
               } else{
                  $rejectPer = 0;
               }
               if($acceptedcount[0]['rejectCount'] != 0) {
                  //$acceptPer = round($acceptedcount[0]['rejectCount'] / $allcount[0]['rejectCount'] * 100);
                  $acceptPer = round($acceptedcount[0]['rejectCount']);
               } else{
                  $acceptPer = 0;
               }
            } else{
               $hiredPer = 0;
               $rejectPer = 0;
               $acceptPer = 0;
            }

            $target = $this->Recruit_Model->target_count($cid,$subrecruiter_id,$filters);
            
            if($target[0]['targetSum'] != NULL) {

                $pending = $target[0]['targetSum'] - $allcount[0]['rejectCount'];
                if($pending) {

                } else{
                   $pending = 0;
                }
                $target = $target[0]['targetSum'];
            } else{
               $target =0;
               $pending = 0;
            }

            $open = $this->Recruit_Model->open_count($cid,$subrecruiter_id,$filters);
            if($open){

            } else{
               $open = 0;
            }
            
            
            $postedcount = $this->Recruit_Model->jobposted($cid,$subrecruiter_id,$filters);
            //echo $this->db->last_query();die;
            if($postedcount){
              $jobcount = $postedcount[0]['targetSum'];
            } else{
               $jobcount = 0;
            }
            
            if($subrecruiter_id>0){
              if($jobcount>0){
                $compData[] = ["cname"=>$companySite['cname'],"cid"=>$companySite['id'], "companyPic"=>$companySite['companyPic'], "companyDesc"=>$companySite['companyDesc'], "address"=>$companySite['address'], "hired"=> $hiredPer, "reject"=> $rejectPer, "accept"=> $acceptPer, "alljob"=> $allcount[0]['rejectCount'], "target" => $target, "pending"=> $newcount[0]['rejectCount'], "open" =>$open[0]['rCount'], "jobcount"=>$jobcount];
              }
            }else{
              $compData[] = ["cname"=>$companySite['cname'],"cid"=>$companySite['id'], "companyPic"=>$companySite['companyPic'], "companyDesc"=>$companySite['companyDesc'], "address"=>$companySite['address'], "hired"=> $hiredPer, "reject"=> $rejectPer, "accept"=> $acceptPer, "alljob"=> $allcount[0]['rejectCount'], "target" => $target, "pending"=> $newcount[0]['rejectCount'], "open" =>$open[0]['rCount'], "jobcount"=>$jobcount];
            }
            
         }

         //var_dump($compData);die;

            $allcount1 = $this->Recruit_Model->allApplication_count1($userSession['id'],$subrecruiter_id,$filters);
            $hiredcounttotal = $this->Recruit_Model->hiredcounttotal($userSession['id'],$subrecruiter_id,$filters);
            $target = $this->Recruit_Model->target_count1($userSession['id'],$subrecruiter_id,$filters);
            
            if($target[0]['targetSum'] != NULL) {
                $pending = $target[0]['targetSum'] - $allcount1[0]['rejectCount'];
                if($pending) {

                } else{
                   $pending = 0;
                }
                $target = $target[0]['targetSum'];
            } else{
               $target =0;
               $pending = 0;
            }

            if($hiredcounttotal[0]['rejectCount'] != NULL){
              $totalhired = $hiredcounttotal[0]['rejectCount'];
            }else{
               $totalhired = 0;
            }

            $open = $this->Recruit_Model->open_count1($userSession['id'],$subrecruiter_id,$filters);
            if($open){

            } else{
               $open = 0;
            }

            $meetrqcount = $this->Recruit_Model->meetreq_count($userSession['id'],$subrecruiter_id,$filters);
            $noacceptcount = $this->Recruit_Model->noaccept_count($userSession['id'],$subrecruiter_id,$filters);
            $refercount = $this->Recruit_Model->refer_count($userSession['id'],$subrecruiter_id,$filters);
            $anothercount = $this->Recruit_Model->another_count($userSession['id'],$subrecruiter_id,$filters);
            $noshowcount = $this->Recruit_Model->noshow_count($userSession['id'],$subrecruiter_id,$filters);
            $allcounttotal = $this->Recruit_Model->allApplication_counted($userSession['id'],$subrecruiter_id,$filters);
            $datecondition="str_to_date(applied_jobs.created_at,'%Y-%m-%d')= str_to_date(applied_jobs.updated_at,'%Y-%m-%d')";
            
            $checknotificationStatus= $this->Recruit_Model->countNotification1($datecondition, $userSession['id'],$subrecruiter_id);
            
            if(!empty($checknotificationStatus && count($checknotificationStatus)>=1))
            {
               $pendingdays = count($checknotificationStatus);
            } else {
               $pendingdays = 0;
            }
            if($noshowcount[0]['rejectCount'] != NULL){
              $noshow = $noshowcount[0]['rejectCount'];
              if($allcounttotal[0]['rejectCount']>0){
                $noshowPer = round($noshowcount[0]['rejectCount'] / $allcounttotal[0]['rejectCount'] * 100);
              }else{
                $noshowPer = 0;
              }
              
            }else{
              $noshow = 0;
              $noshowPer = 0;
            }
            if($meetrqcount[0]['rejectCount'] != NULL){
              $meetrq = $meetrqcount[0]['rejectCount'];
              if($allcounttotal[0]['rejectCount']>0){
                $meetrqPer = round($meetrqcount[0]['rejectCount'] / $allcounttotal[0]['rejectCount'] * 100);
              }else{
                $meetrqPer=0;
              }
              
            }else{
              $meetrq = 0;
              $meetrqPer=0;
            }
            if($noacceptcount[0]['rejectCount'] != NULL){
              $noaccept = $noacceptcount[0]['rejectCount'];
              if($allcounttotal[0]['rejectCount']>0){
                $noacceptPer = round($noacceptcount[0]['rejectCount'] / $allcounttotal[0]['rejectCount'] * 100);
              }else{
                $noacceptPer = 0;
              }
              
            }else{
              $noaccept = 0;
              $noacceptPer = 0;
            }if($refercount[0]['rejectCount'] != NULL){
              $refer = $refercount[0]['rejectCount'];
              if($allcounttotal[0]['rejectCount']>0){
                $referPer = round($refercount[0]['rejectCount'] / $allcounttotal[0]['rejectCount'] * 100);
               } else{
                $referPer=0;
               }
            }else{
              $refer = 0;
              $referPer =0;
            }if($anothercount[0]['rejectCount'] != NULL){
              $another = $anothercount[0]['rejectCount'];
              if($allcounttotal[0]['rejectCount']>0){
               $anotherPer = round($anothercount[0]['rejectCount'] / $allcounttotal[0]['rejectCount'] * 100);
              } else{
                $anotherPer = 0;
              }
            }else{
              $another = 0;
              $anotherPer = 0;
            }
         $data['comLists'] = $compData;
         $data['comsingle'] =  ["target" => $target, "pending"=> $pending, "open" =>$open[0]['rCount'], "pendingdays" => $pendingdays, "noshow"=> $noshow,"noshowPer"=>$noshowPer, "meetrq" => $meetrq,'meetrqPer'=>$meetrqPer, "noaccept"=> $noaccept,'noacceptPer'=>$noacceptPer, "refer"=>$refer,"referPer"=>$referPer, "another"=>$another,"anotherPer"=>$anotherPer, "totalhired" => $totalhired];
         if(!empty($_POST['filters'])){
          $data['filters'] = $_POST['filters'];
         }
         $data['candidatesApplied'] = $this->fetchpending($userSession['id'],$subrecruiter_id );
         //var_dump($data['comsingle']);die;

        $this->load->view('recruiter/dashboard',$data);
    }

    public function logout() {
        //unset($_SESSION['userSession']);
        $this->session->unset_userdata('userSession');
        redirect("recruiter/");
    }

    public function pushweb(){
        $userSess = $this->session->userdata('userSession');
        if(!empty($userSession['label']) && $userSession['label']=='3'){
            $userSession['id'] = $userSession['parent_id'];
        }else{
            $userSession['id'] = $userSession['id'];
        }
      
       
        $notification_list = $this->Recruit_Model->notification_listings($userSess['id']);
        //echo $this->db->last_query();die;
        //print_r($notification_list );
             if(!empty($notification_list))
             {
            foreach ($notification_list as $notification_lists) {
                $companyName = $this->Jobpost_Model->company_detail_fetch($notification_lists['recruiter_id']);
                if(!empty($companyName[0]['cname']))
                {
                  $companynamee=$companyName[0]['cname'];  
                }
                else
                {
                  $companynamee="";
                }
                /*$data['notification_listss'][] = [
                          "id" => $notification_lists['id'],
                          "notification" => $notification_lists['notification'],
                          "status" => $notification_lists['status'],
                          "recruiter_id" => $notification_lists['recruiter_id'],          
                          "job_status" =>   $notification_lists['job_status'],
                          "companyname" =>  $companynamee,
                          "date" => date("d-M-Y h:i:s", strtotime($notification_lists['createdon'])),          

                      ];*/
                      echo "1#".$notification_lists['id'].'#'.$companynamee.'#'.$notification_lists['message'].'#'.date("d-M-Y h:i:s", strtotime($notification_lists['created_at']));
                }
                }
    }

    public function fetchpending($userSession,$subrecruiter_id){
        $data["pendingData"] = [];
        $pending = $this->Candidate_Model->candidateduenotification_list($userSession,$subrecruiter_id ); 
        if (!empty($pending)) {
            foreach ($pending as $pendings) {
                //echo $pendings['updated_at'];die;
                $enddate =  date('Y-m-d', strtotime($pendings['updated_at']. ' + 3 days'));
                //echo $enddate;die;
                $enddate = strtotime($enddate);
                $updated_date=  strtotime($pendings['updated_at']);
                $now = time();
                $datediff =  $now - $updated_date;

                $daydiff = floor($datediff / (60*60*24));
                //echo $daydiff;die;
                if($daydiff >=3)
                {
                    $data["pendingData"][] =["profilePic"=>$pendings['profilePic'],"user_id"=>$pendings['user_id'], "name"=>$pendings['name'], "email"=>$pendings['email'], 'phone'=>$pendings['phone'], "location"=>$pendings['location'], "status"=>$pendings['status'], "interviewdate" => $pendings['interviewdate'], 'interviewtime' => $pendings['interviewtime'] , "date_day1" => $pendings['date_day1'], 'jobDesc' => $pendings['jobDesc'], "jobtitle"=>$pendings['jobtitle'], "compid"=>$pendings['compid'], "id"=>$pendings['id'], 'user_id'=>$pendings['user_id'],'jobpost_id'=>$pendings['jobpost_id'], 'updated_at'=> $pendings['updated_at'], 'fallout_reason' => $pendings['fallout_reason']];
                }

            }
        }
        return $data["pendingData"];
    }
}
?>
