<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Notification extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('Common_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->helper('cookie');
if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
        
    }
    
    public function index() {
        $userSession = $this->session->userdata('userSession');
        $data['notification_lists'] = $this->Recruit_Model->notification_listings($userSession['id']);

        $this->load->view('recruiter/notification',$data);
    }


   
}
?>  
