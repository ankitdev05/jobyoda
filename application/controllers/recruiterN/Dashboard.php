<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Dashboard extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('recruiter/Candidate_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');

        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
    }
    
    public function index() {
        $userSession = $this->session->userdata('userSession');
        $companySites = $this->Recruit_Model->company_sites($userSession['id']);
        $compData = array();

        foreach($companySites as $companySite) {
            $cid = $companySite['id'];
            $hiredcount = $this->Recruit_Model->hired_count($cid);
            $rejectedcount = $this->Recruit_Model->rejected_count($cid);
            $acceptedcount = $this->Recruit_Model->acceptedApplication_count($cid);
            $allcount = $this->Recruit_Model->allApplication_count($cid);
            
            if($allcount) {
               if($hiredcount[0]['hiredCount'] != 0) {
                  $hiredPer = round($hiredcount[0]['hiredCount'] / $allcount[0]['rejectCount'] * 100);
               } else {
                  $hiredPer = 0;
               }
               if($rejectedcount[0]['rejectCount'] != 0) {
                  $rejectPer = round($rejectedcount[0]['rejectCount'] / $allcount[0]['rejectCount'] * 100);
               } else{
                  $rejectPer = 0;
               }
               if($acceptedcount[0]['rejectCount'] != 0) {
                  $acceptPer = round($acceptedcount[0]['rejectCount'] / $allcount[0]['rejectCount'] * 100);
               } else{
                  $acceptPer = 0;
               }
            } else{
               $hiredPer = 0;
               $rejectPer = 0;
               $acceptPer = 0;
            }

            $target = $this->Recruit_Model->target_count($cid);
           
            if($target[0]['targetSum'] != NULL) {

                $pending = $target[0]['targetSum'] - $allcount[0]['rejectCount'];
                if($pending) {

                } else{
                   $pending = 0;
                }
                $target = $target[0]['targetSum'];
            } else{
               $target =0;
               $pending = 0;
            }

            $open = $this->Recruit_Model->open_count($cid);
            if($open){

            } else{
               $open = 0;
            }

            $compData[] = ["cname"=>$companySite['cname'], "companyPic"=>$companySite['companyPic'], "companyDesc"=>$companySite['companyDesc'], "address"=>$companySite['address'], "hired"=> $hiredPer, "reject"=> $rejectPer, "accept"=> $acceptPer, "alljob"=> $allcount[0]['rejectCount'], "target" => $target, "pending"=> $pending, "open" =>$open[0]['rCount']];
         }

         //var_dump($compData);die;

            $allcount1 = $this->Recruit_Model->allApplication_count1($userSession['id']);
            $target = $this->Recruit_Model->target_count1($userSession['id']);
            
            if($target[0]['targetSum'] != NULL) {
                $pending = $target[0]['targetSum'] - $allcount1[0]['rejectCount'];
                if($pending) {

                } else{
                   $pending = 0;
                }
                $target = $target[0]['targetSum'];
            } else{
               $target =0;
               $pending = 0;
            }

            $open = $this->Recruit_Model->open_count1($userSession['id']);
            if($open){

            } else{
               $open = 0;
            }

            $datecondition="str_to_date(applied_jobs.created_at,'%Y-%m-%d')= str_to_date(applied_jobs.updated_at,'%Y-%m-%d')";
            
            $checknotificationStatus= $this->Recruit_Model->countNotification1($datecondition, $userSession['id']);
            
            if(!empty($checknotificationStatus && count($checknotificationStatus)>=1))
            {
               $pendingdays = count($checknotificationStatus);
            } else {
               $pendingdays = 0;
            }

         $data['comLists'] = $compData;
         $data['comsingle'] =  ["target" => $target, "pending"=> $pending, "open" =>$open[0]['rCount'], "pendingdays" => $pendingdays];

         //var_dump($data['comsingle']);die;

        $this->load->view('recruiter/dashboard',$data);
    }

    public function logout() {
        //unset($_SESSION['userSession']);
        $this->session->unset_userdata('userSession');
        redirect("recruiter/recruiter/index");
    }

    public function pushweb(){
        $userSess = $this->session->userdata('userSession');
        
      
       
        $notification_list = $this->Recruit_Model->notification_listings($userSess['id']);
        //echo $this->db->last_query();die;
        //print_r($notification_list );
             if(!empty($notification_list))
             {
            foreach ($notification_list as $notification_lists) {
                $companyName = $this->Jobpost_Model->company_detail_fetch($notification_lists['recruiter_id']);
                if(!empty($companyName[0]['cname']))
                {
                  $companynamee=$companyName[0]['cname'];  
                }
                else
                {
                  $companynamee="";
                }
                /*$data['notification_listss'][] = [
                          "id" => $notification_lists['id'],
                          "notification" => $notification_lists['notification'],
                          "status" => $notification_lists['status'],
                          "recruiter_id" => $notification_lists['recruiter_id'],          
                          "job_status" =>   $notification_lists['job_status'],
                          "companyname" =>  $companynamee,
                          "date" => date("d-M-Y h:i:s", strtotime($notification_lists['createdon'])),          

                      ];*/
                      echo "1#".$notification_lists['id'].'#'.$companynamee.'#'.$notification_lists['message'].'#'.date("d-M-Y h:i:s", strtotime($notification_lists['created_at']));
                }
                }
    }
}
?>
