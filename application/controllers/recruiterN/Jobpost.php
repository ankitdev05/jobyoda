<?php
ob_start();
ini_set("allow_url_fopen", 1);
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Jobpost extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('Common_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');

        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
    }
    public function index() {
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $userSession = $this->session->userdata('userSession');
        $data['jobTitle'] = $this->Common_Model->job_title_list();
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();

        if(isset($_GET['type'])){
            $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($_GET['type']);
        } else{
            $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($userSession['id']);
        }
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        $this->load->view('recruiter/postjob', $data);
    }

    public function index1() {
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $userSession = $this->session->userdata('userSession');
        $data['jobTitle'] = $this->Common_Model->job_title_list();
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();

        if(isset($_GET['type'])){
            $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($_GET['type']);
        } else{
            $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($userSession['id']);
        }
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        $this->load->view('recruiter/postjob1', $data);
    }

    public function fetchSubcategory(){
        $category = $this->input->post('cat_val');
        $data['subcategory'] = $this->Jobpost_Model->subcategory_listsbycatid($category);
        if($data['subcategory']){
            echo "<option value=''>Select SubCategory</option>";
            foreach ($data['subcategory'] as $subcategorys) {
                
                echo '<option value="'.$subcategorys['id'].'">'.$subcategorys['subcategory'].'</option>';
                //echo $category_data;
            }

        }    else{
           echo "<option value=''>Select SubCategory</option>";
        }   
        
    }

    public function jobpostInsert() {
        $jobData = $this->input->post();
        //var_dump($jobData);die;
        $this->form_validation->set_rules('jobTitle', 'Job Title', 'trim|required');
        if(!empty($jobData['bonuscheck']) && $jobData['bonuscheck']=='on'){
            $this->form_validation->set_rules('bonus_amount', 'Bonus amount', 'trim|required');
        }
        $this->form_validation->set_rules('jobLoc', 'Job Location', 'trim|required');
        $this->form_validation->set_rules('opening', 'Opening', 'trim|required|numeric');
        $this->form_validation->set_rules('experience', 'Experience', 'trim|required');
        $this->form_validation->set_rules('allowance', 'Allowance', 'trim|required|numeric');
        $this->form_validation->set_rules('education', 'Education', 'trim|required');
        $this->form_validation->set_rules('subcategory', 'SubCategory', 'trim|required');
        $this->form_validation->set_rules('category', 'Category', 'trim|required');
        $this->form_validation->set_rules('level', 'Level', 'trim|required');
        $this->form_validation->set_rules('lang', 'Language', 'trim|required');
        $this->form_validation->set_rules('jobPitch', 'Job Pitch', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('jobDesc', 'Job Description', 'trim|required|max_length[200]');
        $this->form_validation->set_rules('skills', 'Skills', 'trim|required');
        $this->form_validation->set_rules('qualification', 'Qualification', 'trim|required');
        $this->form_validation->set_rules('jobExpire', 'Job Expire', 'trim|required');
        if(!empty($jobData['level_status']) && $jobData['level_status']=="on"){
            $jobData['level_status']=1;
        }else{
            $jobData['level_status']=0;
        }    
        if(!empty($jobData['education_status']) && $jobData['education_status']=="on"){
            $jobData['education_status']=1;
        }
        else{
            $jobData['education_status']=0;
        }
        if(!empty($jobData['experience_status']) && $jobData['experience_status']=="on"){
            $jobData['experience_status']=1;
        }
        else{
            $jobData['experience_status']=0;
        }
        if ($this->form_validation->run() == FALSE) {
            //echo $jobData["jobId"];die;
            $data['errors'] = $this->form_validation->error_array();
            if(!empty($jobData["jobId"])){
                $data['getJobs'] = $this->Jobpost_Model->jobupdate_detail_fetch($jobData["jobId"]);
                $data['jobImg'] = $this->Jobpost_Model->job_images($jobData["jobId"]);
                $topPicks = $this->Recruit_Model->company_topicks_single($jobData["jobId"]);
        
        $topPicks = $this->Jobpost_Model->jobs_topicks_single($jobData["jobId"]);
        
        if($topPicks) { 
            $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];
              $x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }
        
        $allowances = $this->Jobpost_Model->jobs_allowances_single($jobData["jobId"]);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Jobpost_Model->jobs_medical_single($jobData["jobId"]);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }

        $workshifts = $this->Jobpost_Model->jobs_workshifts_single($jobData["jobId"]);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }
        $leaves = $this->Jobpost_Model->jobs_leaves_single($jobData["jobId"]);
        //echo $this->db->last_query();die;
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leaves1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leaves2'] = $leaves1;
        } else{
            $data['leaves2'] = [];
        }
            }
            //print_r($data['getJobs']);die;
            $data['jobTitle'] = $this->Common_Model->job_title_list();
            $data['industryLists'] = $this->Common_Model->industry_lists();
            $userSession = $this->session->userdata('userSession');
            $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
            $data['levels'] = $this->Jobpost_Model->level_lists();
            $data['category'] = $this->Jobpost_Model->category_lists();
            $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
            $data['jobdata'] = $jobData;

            if(isset($_GET['type'])){
                $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($_GET['type']);
            } else{
                $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($userSession['id']);
            }

            $data['channels'] = $this->Common_Model->channel_lists();
            $data['langs'] = $this->Common_Model->language_lists();
            if(!empty($jobData["jobId"])){
            $job_id = $jobData["jobId"];                
            }
            if(isset($jobData["jobId"])) {
                redirect('recruiter/jobpost/jobpostView?type=$job_id ');
            }else{
                $this->load->view('recruiter/postjob', $data);
            }
            
        } else {
            $userSession = $this->session->userdata('userSession');
            $jobData['jobExpire'] = date("Y-m-d", strtotime($jobData['jobExpire']));
            
            if(isset($jobData["jobId"])) {
                $data = [
                        "recruiter_id" => $jobData['recruId'],
                        "jobtitle" => $jobData['jobTitle'],
                        "company_id" => $jobData['jobLoc'],
                        "opening" => $jobData['opening'],
                        "experience" => $jobData['experience'],
                        "allowance" => $jobData['allowance'],
                        "industry" => 81,
                        "category" => $jobData['category'],
                        "subcategory" => $jobData['subcategory'],
                        "language" => $jobData['lang'],
                        "other_language" => $jobData['otherlanguage'],
                        "jobPitch" => $jobData['jobPitch'],
                        "jobDesc" => $jobData['jobDesc'],
                        "skills" => $jobData['skills'],
                        "qualification" => $jobData['qualification'],
                        "education" => $jobData['education'],
                        "jobexpire" => $jobData['jobExpire'],
                        "level_status" => $jobData['level_status'],
                        "education_status" => $jobData['education_status'],
                        "experience_status" => $jobData['experience_status'],
                        "joining_bonus" => $jobData['bonus_amount'],
                        /*"job_type" => $jobData['job_type'],
                        "walkin_date" => $jobData['walkin_date'],
                        "walkin_from" => $jobData['walkin_from'],
                        "walkin_to" => $jobData['walkin_to']*/
                    ];
                $jobInserted = $this->Jobpost_Model->job_update($data, $jobData['jobId']);
                $count1 = count($_FILES['job_image']['name']);
                // $count1;die;
                if($count1 > 0) {
                    for($i=0;$i<$count1;$i++) {
                        $_FILES['file']['name'] = $_FILES['job_image']['name'][$i];
                          $_FILES['file']['type'] = $_FILES['job_image']['type'][$i];
                          $_FILES['file']['tmp_name'] = $_FILES['job_image']['tmp_name'][$i];
                          $_FILES['file']['error'] = $_FILES['job_image']['error'][$i];
                          $_FILES['file']['size'] = $_FILES['job_image']['size'][$i];
                
                          // Set preference
                          $config['upload_path'] = "./recruiterupload/"; 
                          $config['allowed_types'] = 'jpg|jpeg|png|gif';
                          $config['max_size'] = '5000'; // max_size in kb
                          $config['file_name'] = $_FILES['job_image']['name'][$i];
                 
                          //Load upload library
                          $this->load->library('upload',$config); 
                 
                          // File upload
                          if($this->upload->do_upload('file')){
                            // Get data about the file
                            $uploadData = $this->upload->data();
                            $this->resizeImage($upload_data['file_name']);
                            $filename = base_url() ."recruiterupload/".$uploadData['file_name'];
                            //print_r($filename);        
                            // Initialize array
                            $data['filenames'][] = $filename;
                          }
                          if(!empty($filename)){
                              $data2 = ["jobpost_id"=>$jobData['jobId'],"pic" => $filename];
                                $this->Jobpost_Model->job_image_insert($data2);
                          }
                        
                    }
                }

                if(isset($jobData['expRange'])) {
                    
                    $jobExpRanges[] = $jobData['expRange'];
                    $jobExpBasicSalarys[] = $jobData['expBasicSalary'];
                    $count = count($jobData['expRange']);
                    if($count > 0) {
                        for($i=0;$i<$count;$i++) {

                            $data1[$i] = ["jobpost_id"=>$jobData["jobId"], "exp" => $jobData['expRange'][$i], "basicsalary" => $jobData['expBasicSalary'][$i]];
                        }
                        
                        $this->Jobpost_Model->job_basicsalary_delete($jobData['jobId']);
                        $expsalInserted = $this->Jobpost_Model->basic_salary_insert($data1);
                    }
                }
                if($jobData['toppicks']) {
                    $topPicks = $jobData['toppicks'];
                    $x=0;

                    foreach ($topPicks as $topPick) {
                        $dataPicks[$x] = ["jobpost_id"=>$jobData['jobId'], "picks_id"=>$topPick];
                        $x++;
                    }
                    $this->Jobpost_Model->job_toppicks_delete($jobData['jobId']);
                    $this->Jobpost_Model->job_toppicks_insert($dataPicks);
                }
                if($jobData['allowances']) {
                    $topAllos = $jobData['allowances'];
                    $x=0;
                    foreach ($topAllos as $topAllo) {
                        $dataAllos[$x] = ["jobpost_id"=>$jobData['jobId'], "allowances_id"=>$topAllo];
                        $x++;
                    }
                    $this->Jobpost_Model->job_allowances_delete($jobData['jobId']);
                    $this->Jobpost_Model->job_allowances_insert($dataAllos);
                }
                if($jobData['medical']) {
                    $topMedis = $jobData['medical'];
                    $x=0;
                    foreach ($topMedis as $topMedi) {
                        $dataMedis[$x] = ["jobpost_id"=>$jobData['jobId'], "medical_id"=>$topMedi];
                        $x++;
                    }
                    $this->Jobpost_Model->job_medical_delete($jobData['jobId']);
                    $this->Jobpost_Model->job_medical_insert($dataMedis);
                }
                if($jobData['shifts']) {
                    $topShifts = $jobData['shifts'];
                    $x=0;
                    foreach ($topShifts as $topShift) {
                        $dataShifts[$x] = ["jobpost_id"=>$jobData['jobId'], "workshift_id"=>$topShift];
                        $x++;
                    }
                    $this->Jobpost_Model->job_shifts_delete($jobData['jobId']);
                    $this->Jobpost_Model->job_shifts_insert($dataShifts);
                }
                if($jobData['leavs']) {
                    $leavs = $jobData['leavs'];
                    $x=0;
                    foreach ($leavs as $leav) {
                        $dataLeaves[$x] = ["jobpost_id"=>$jobData['jobId'], "leaves_id"=>$leav];
                        $x++;
                    }
                    $this->Jobpost_Model->job_leaves_delete($jobData['jobId']);
                    $this->Jobpost_Model->job_leaves_insert($dataLeaves);
                }
                if($jobInserted) {
                     $this->session->set_tempdata('inserted', 'Job Post Successfully Updated',5);
                     $dataId = $jobData['jobId'];
                     redirect("recruiter/jobpost/jobpostViews/$dataId");
                } else {
                    $this->session->set_tempdata('postError',  'Job Post not able to Updated',5);
                    $dataId = $jobData['jobId'];
                    redirect("recruiter/jobpost/jobpostViews/$dataId");
                }
            } else {
                $data = [
                        "recruiter_id"=> $jobData['recruId'],
                        "jobtitle" => $jobData['jobTitle'],
                        "company_id" => $jobData['jobLoc'],
                        "opening" => $jobData['opening'],
                        "experience" => $jobData['experience'],
                        "allowance" => $jobData['allowance'],
                        "industry" => 81,
                        "category" => $jobData['category'],
                        "subcategory" => $jobData['subcategory'],
                        "level" => $jobData['level'],
                        "language" => $jobData['lang'],
                        "jobPitch" => $jobData['jobPitch'],
                        "jobDesc" => $jobData['jobDesc'],
                        "skills" => $jobData['skills'],
                        "qualification" => $jobData['qualification'],
                        "education" => $jobData['education'],
                        "jobexpire" => $jobData['jobExpire'],
                        "other_language" => $jobData['otherlanguage'],
                        "level_status" => $jobData['level_status'],
                        "education_status" => $jobData['education_status'],
                        "experience_status" => $jobData['experience_status'],
                        "joining_bonus" => $jobData['bonus_amount'],
                        /*"job_type" => $jobData['job_type'],
                        "walkin_date" => $jobData['walkin_date'],
                        "walkin_from" => $jobData['walkin_from'],
                        "walkin_to" => $jobData['walkin_to']*/
                    ];
                $jobInserted = $this->Jobpost_Model->job_insert($data);
                //echo $this->db->last_query();die;
                $count = count($_FILES['job_image']['name']);
                 //$count;die;
                if($count > 0) {
                    for($i=0;$i<$count;$i++) {
                        $_FILES['file']['name'] = $_FILES['job_image']['name'][$i];
                          $_FILES['file']['type'] = $_FILES['job_image']['type'][$i];
                          $_FILES['file']['tmp_name'] = $_FILES['job_image']['tmp_name'][$i];
                          $_FILES['file']['error'] = $_FILES['job_image']['error'][$i];
                          $_FILES['file']['size'] = $_FILES['job_image']['size'][$i];
                
                          // Set preference
                          $config['upload_path'] = "./recruiterupload/"; 
                          $config['allowed_types'] = 'jpg|jpeg|png|gif';
                          $config['max_size'] = '5000'; // max_size in kb
                          $config['file_name'] = $_FILES['job_image']['name'][$i];
                 
                          //Load upload library
                          $this->load->library('upload',$config); 
                 
                          // File upload
                          if($this->upload->do_upload('file')){
                            // Get data about the file
                            $uploadData = $this->upload->data();
                            $this->resizeImage($upload_data['file_name']);
                            $filename = base_url() ."recruiterupload/".$uploadData['file_name'];
                            //print_r($filename);        
                            // Initialize array
                            $data['filenames'][] = $filename;
                          }else{
                              $filename="";
                          }
                        if(!empty($filename)){
                            $data1 = ["jobpost_id"=>$jobInserted,"pic" => $filename];
                            $this->Jobpost_Model->job_image_insert($data1);
                        }  
                        
                    }
                     
                }
                if(isset($jobData['expRange'])) {
                    $count = count($jobData['expRange']);
                    //echo $count;die;
                    if($count > 0) {
                        for($i=0;$i<$count;$i++) {
                            $data2[$i] = ["jobpost_id"=>$jobInserted, "exp" => $jobData['expRange'][$i], "basicsalary" => $jobData['expBasicSalary'][$i]];
                        }
                        //print_r($data1);die;
                        $expsalInserted = $this->Jobpost_Model->basic_salary_insert($data2);
                       // echo $this->db->last_query();die;
                    }
                }
                if(isset($jobData['toppicks'])) {
                $topPicks = $jobData['toppicks'];
                $x=0;
                foreach ($topPicks as $topPick) {
                    $dataPicks[$x] = ["jobpost_id"=>$jobInserted, "picks_id"=>$topPick];
                    $x++;
                }
                $this->Jobpost_Model->job_toppicks_insert($dataPicks);
                }
                if(isset($jobData['allowances'])) {
                    $topAllos = $jobData['allowances'];
                    $x=0;
                    foreach ($topAllos as $topAllo) {
                        $dataAllos[$x] = ["jobpost_id"=>$jobInserted, "allowances_id"=>$topAllo];
                        $x++;
                    }
                    $this->Jobpost_Model->job_allowances_insert($dataAllos);
                }
                if(isset($jobData['medical'])) {
                    $topMedis = $jobData['medical'];
                    $x=0;
                    foreach ($topMedis as $topMedi) {
                        $dataMedis[$x] = ["jobpost_id"=>$jobInserted, "medical_id"=>$topMedi];
                        $x++;
                    }
                    $this->Jobpost_Model->job_medical_insert($dataMedis);
                }
                if(isset($jobData['shifts'])) {
                    $topShifts = $jobData['shifts'];
                    $x=0;
                    foreach ($topShifts as $topShift) {
                        $dataShifts[$x] = ["jobpost_id"=>$jobInserted, "workshift_id"=>$topShift];
                        $x++;
                    }
                    $this->Jobpost_Model->job_shifts_insert($dataShifts);
                }
                
                if(isset($jobData['leavs'])) {
                    $topLeaves = $jobData['leavs'];
                    $x=0;
                    foreach ($topLeaves as $topLeave) {
                        $dataLeaves[$x] = ["jobpost_id"=>$jobInserted, "leaves_id"=>$topLeave];
                        $x++;
                    }
                    $this->Jobpost_Model->job_leaves_insert($dataLeaves);
                }
                if($jobInserted) {
                     $this->session->set_tempdata('inserted', 'Job Successfully Uploaded',5);
                     redirect("recruiter/jobpost/manageJobView", $dataId);
                } else {
                    $this->session->set_tempdata('postError', 'Job Post not able to created',5);
                    $this->load->view('recruiter/jobpost/postjob');
                }
            }
        }
    }

    public function jobpostViews() {
        $jobId = $this->uri->segment(4);
        $userSession = $this->session->userdata('userSession');
        $data['getJobs'] = $this->Jobpost_Model->jobupdate_detail_fetch($jobId);
        $data['jobImg'] = $this->Jobpost_Model->job_images($jobId);
        $data['getJobexps'] = $this->Jobpost_Model->jobupdate_detailexp_fetch($jobId);
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $data['jobTitle'] = $this->Common_Model->job_title_list();
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($data['getJobs'][0]['company_id']);
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
        $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
        $topPicks = $this->Recruit_Model->company_topicks_single($jobId);
        
        $topPicks = $this->Jobpost_Model->jobs_topicks_single($jobId);
        
        if($topPicks) { 
            $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];
              $x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }
        
        $allowances = $this->Jobpost_Model->jobs_allowances_single($jobId);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Jobpost_Model->jobs_medical_single($jobId);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }

        $workshifts = $this->Jobpost_Model->jobs_workshifts_single($jobId);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }
        $leaves = $this->Jobpost_Model->jobs_leaves_single($jobId);
        //echo $this->db->last_query();die;
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leaves1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leaves2'] = $leaves1;
        } else{
            $data['leaves2'] = [];
        }
        $this->load->view('recruiter/updatejob', $data);
    }

    public function manageJobView() {
        $userSession = $this->session->userdata('userSession');
        $getJobs = $this->Jobpost_Model->job_fetch($userSession['id']);
        //echo $this->db->last_query();die;
        foreach($getJobs as $getJob) {
            $getappliedCount = $this->Jobpost_Model->jobappliedCount_fetch($getJob['id']);
            $data['jobFetch'][] = [
                                    "id"=>$getJob['id'],
                                    "address" => $getJob['address'],
                                    "jobtitle"=>$getJob['jobtitle'],
                                    "jobDesc"=>$getJob['jobDesc'],
                                    "companydetail"=>$getJob['companydetail'],
                                    "qualification"=>$getJob['qualification'],
                                    "skills"=>$getJob['skills'],
                                    "salary"=>$getJob['salary'],
                                    "totalApply"=>$getappliedCount[0]['appliedCount'],
                                    ];
        }

        $data['jobApplicationFetch'] = $this->Jobpost_Model->jobApplication_fetch($userSession['id']);
        $data['activejobFetch'] = $this->Jobpost_Model->activeJob_fetch($userSession['id']);
        $data['boostAmountFetch'] = $this->Jobpost_Model->boostamount_fetch();
        //echo $this->db->last_query();
        $this->load->view('recruiter/manage_jobs', $data);
    }

    public function jobpostView() {
        $jobId = $_GET['type'];
        $userSession = $this->session->userdata('userSession');
        $data['getJobs'] = $this->Jobpost_Model->jobupdate_detail_fetch($jobId);
        $data['getJobexps'] = $this->Jobpost_Model->jobupdate_detailexp_fetch($jobId);
        //print_r($data['getJobexps']);die;
        $data['jobImg'] = $this->Jobpost_Model->job_images($jobId);
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $data['jobTitle'] = $this->Common_Model->job_title_list();
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
         $data['subcategory'] = $this->Jobpost_Model->subcategory_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        if(isset($data['getJobs'][0]['recruiter_id'])){
            $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($data['getJobs'][0]['recruiter_id']);
        }
        
        //echo $this->db->last_query();die;
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        
        $topPicks = $this->Jobpost_Model->jobs_topicks_single($jobId);
        
        if($topPicks) { 
            $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];
              $x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }
        
        $allowances = $this->Jobpost_Model->jobs_allowances_single($jobId);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Jobpost_Model->jobs_medical_single($jobId);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }

        $workshifts = $this->Jobpost_Model->jobs_workshifts_single($jobId);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }
        
        $leaves = $this->Jobpost_Model->jobs_leaves_single($jobId);
        //echo $this->db->last_query();die;
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leaves1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leaves2'] = $leaves1;
        } else{
            $data['leaves2'] = [];
        }
        $this->load->view('recruiter/updatejob', $data);
    }
    public function jobpostView1() {
        $jobId = $_GET['type'];
        $userSession = $this->session->userdata('userSession');
        $data['getJobs'] = $this->Jobpost_Model->jobupdate_detail_fetch($jobId);
        $data['getJobexps'] = $this->Jobpost_Model->jobupdate_detailexp_fetch($jobId);
        $data['jobImg'] = $this->Jobpost_Model->job_images($jobId);
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $data['jobTitle'] = $this->Common_Model->job_title_list();
        $data['channels'] = $this->Common_Model->channel_lists();
        $data['levels'] = $this->Jobpost_Model->level_lists();
        $data['category'] = $this->Jobpost_Model->category_lists();
        $data['langs'] = $this->Common_Model->language_lists();
        $data["getExps"] = $this->Recruit_Model->recruiter_exp_fetch($data['getJobs'][0]['recruiter_id']);
        //echo $this->db->last_query();die;
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        
        $topPicks = $this->Jobpost_Model->jobs_topicks_single($jobId);
        
        if($topPicks) { 
            $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];
              $x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }
        
        $allowances = $this->Jobpost_Model->jobs_allowances_single($jobId);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Jobpost_Model->jobs_medical_single($jobId);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }

        $workshifts = $this->Jobpost_Model->jobs_workshifts_single($jobId);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }
        
        $leaves = $this->Jobpost_Model->jobs_leaves_single($jobId);
        //echo $this->db->last_query();die;
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leaves1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leaves2'] = $leaves1;
        } else{
            $data['leaves2'] = [];
        }
        $this->load->view('recruiter/updatejob1', $data);
    }

    public function booststatus() {
        $jid = $this->input->post('jid');
        $data1 = ["boost_status"=>1];
        $jobseekerInserted = $this->Jobpost_Model->jobboostStatus($data1, $jid);
        //echo $this->db->last_query();die;
        if($jobseekerInserted) {
            
            redirect("recruiter/jobpost/manageJobView");
        } else {
            redirect("recruiter/jobpost/manageJobView");
        }
        
    }



    public function getLatLong($address){
        if(!empty($address)){
            //Formatted address
            $formattedAddr = str_replace(' ','+',$address);
            //Send request and receive json data by address
            $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk'); 
            $output = json_decode($geocodeFromAddr);
            //Get latitude and longitute from json data
            $data['latitude']  = $output->results[0]->geometry->location->lat; 
            $data['longitude'] = $output->results[0]->geometry->location->lng;
            //Return latitude and longitude of the given address
            if(!empty($data)) {
                return $data;
            }else{
                return false;
            }
        }else{
            return false;   
        }
    }

    function deleteimg(){
        $id = $this->input->post('id');
        $delete = $this->Jobpost_Model->deleteimg($id);
        
    }

    public function deleteJob(){
        $id = $this->input->get('id');
        $this->Jobpost_Model->deleteJob($id);
    }

    public function resizeImage($filename)
       {
          $source_path =  './recruiterupload/' . $filename;
          $target_path = './recruiterupload/thumbs/';
          $config_manip = array(
              'image_library' => 'gd2',
              'source_image' => $source_path,
              'new_image' => $target_path,
              'maintain_ratio' => TRUE,
              'create_thumb' => TRUE,
              'width' => 150,
              'height' => 150
          );
    
    
          $this->load->library('image_lib', $config_manip);
          if (!$this->image_lib->resize()) {
              echo $this->image_lib->display_errors();
          }
    
    
          //$this->image_lib->clear();
       }
}
?>
