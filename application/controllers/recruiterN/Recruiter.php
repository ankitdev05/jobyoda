<?php
ob_start();
ini_set('display_errors', 1);
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @category        Controller
 * @author          Ankit Mittal
 * @license         Mobulous
 */
class Recruiter extends CI_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('recruiter/Recruit_Model');
        $this->load->model('Common_Model');
        $this->load->model('recruiter/Jobpost_Model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
        $this->load->helper('cookie');
        /*if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }*/
    }
    
    public function index() {
        $this->load->view('recruiter/login');
    }

    public function addrecruiter() {
        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
        $data["phonecodes"] = $data = $this->Common_Model->phonecode_lists(); 
        $this->load->view('recruiter/addrecruiter',$data);
    }

    public function editrecruiter() {
        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
        $id = base64_decode($this->input->get('id'));
        $data["phonecodes"] = $data = $this->Common_Model->phonecode_lists(); 
        $data['recruitFetch'] = $this->Recruit_Model->subrecruitersfetch($id);
        //print_r($data['recruitFetch'])
        $this->load->view('recruiter/editrecruiter',$data);
    }

    public function recruiterlist() {
        //echo "hi";die;
        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        $data['recruitFetch'] = $this->Recruit_Model->subrecruiters($userSession['id']); 
        //echo $this->db->last_query();die;
        //print_r($data['recruitFetch']);die;
        $this->load->view('recruiter/recruiterlist',$data);
    }

    public function contact() {
        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
        $this->load->view('recruiter/contact');
    }

    public function contact_form(){
        $contactData = $this->input->post();
        
        //$data1 = ["name"=>$contactData['name'], "email"=>$contactData['email'], "phone"=> $contactData['phone'], "message"=> $contactData['message']];

        //$contactInserted = $this->Candidate_Model->contact_insert($data1);
        //$userId = $contactData['user_id'];
        /*if($contactInserted) {*/
            $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
            $this->form_validation->set_rules('message', 'Message', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data['errors'] = $this->form_validation->error_array();
                $data['contactData'] = $contactData;
                $this->load->view('recruiter/contact', $data);
            } else {
            $this->load->library('email');
            $this->email->from('jobyoda123@gmail.com', "JobYoda");
            $this->email->to('villaverred@gmail.com','digvijayankoti29@gmail.com');
            $this->email->subject('Contact from JobYoda');
            $msg = "Dear " ;
            $msg .= ". Jobyoda Recruiter contacted you. Please check the message below. ";
            $msg .= ". Subject ". $contactData['subject'];
            $msg .= ". and Message ". $contactData['message'];
            $this->email->message($msg);
            $this->email->send();

            $this->session->set_tempdata('contactSuccess','Email sent Successfully');
            
            redirect("recruiter/recruiter/contact");
        }
        /*} else {*/
            //redirect("recruiter/candidate/singleCandidates?id=$userId");
        //}
    
    }
    public function faq() {
        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
        $this->load->view('recruiter/faq');
    }



    public function logincheck() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_message('required', 'Password not generated yet');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['userData'] = $userData;
            $this->load->view('recruiter/login', $data);
        } else {
            $password = $userData['password'];
            $data = ["email" => $userData['email']];
            $recruiterCheck = $this->Recruit_Model->recruiter_login($data);
            //var_dump($recruiterCheck);die;
            if(!empty($recruiterCheck)){
               if($recruiterCheck[0]['status'] == 1) {
                if($recruiterCheck[0]['active'] == 1) {
                    if($recruiterCheck) {
                        $getPass = $recruiterCheck[0]['password'];
                        $vPass = $this->encryption->decrypt($getPass);
                        
                        if($password == $vPass) {
                            $getData = $this->fetchRecruiterSingleData($recruiterCheck[0]['id']);
                            $this->session->set_userdata('userSession', $getData);
                            $recruiterId = $this->session->userdata('userSession');
            $datecondition="str_to_date(applied_jobs.created_at,'%Y-%m-%d')= str_to_date(applied_jobs.updated_at,'%Y-%m-%d')";
            $checknotificationStatus= $this->Recruit_Model->countNotification($datecondition);
            
            if(!empty($checknotificationStatus && count($checknotificationStatus)>=1))
            {

               $checkcountmessage = $this->Recruit_Model->checkCountmessage($recruiterId);
                  
              $enddate =  date('Y-m-d', strtotime($checkcountmessage[0]['updated_at']. ' + 3 days'));
              $enddate = strtotime($enddate);
                $updated_date=  strtotime($checkcountmessage[0]['updated_at']);
              $datediff =  $enddate - $updated_date;
              $daydiff = floor($datediff / (60*60*24));

              if(!empty($checkcountmessage) && $datediff >=3)
              {
                   $Notificationupdate =      array(
                                                    'nitifycount'  =>'2',
                                                    );
                    
                   $notificationmessageupdate = array(
                                          'recruiter_id' =>$recruiterId['id'],
                                           'message'=>"Its been observed that the status of job application are not getting change kindly change the job application status. ",
                                           'job_status'        =>'1',
                                          );
                   $this->Recruit_Model->update_notificationcount($Notificationupdate,$recruiterId['id']);

                   $this->Recruit_Model->Notificationmessageinsert($notificationmessageupdate);
                   $this->session->set_flashdata('successAlert','successfully');
               }
               if(!empty($checkcountmessage) && count($checkcountmessage)>=5)
               {
                $blockrecruiter_id= array('recruiter_id' =>$recruiterId['id'],

                                         );
                 $this->Recruit_Model->blockRecruiter($blockrecruiter_id);
               }
               if(empty($checkcountmessage))
               {
                  $arrayNotification = array('recruiter_id' =>$recruiterId['id'],
                                         'nitifycount'  =>'1',
                                        );
                  $notificationmessage = array('recruiter_id' =>$recruiterId['id'],
                                           'message'=>"Your Application status is 'new Application'.",
                                           'job_status'        =>'1',
                                          );
                  $this->Recruit_Model->countNotificationinsert($arrayNotification);
                  $this->Recruit_Model->Notificationmessageinsert($notificationmessage);
               }  
               
            }
            else
            {
             $this->Recruit_Model->deleteRecruiter($recruiterId['id']); 
             $this->Recruit_Model->Notificationmessageinsert($notificationmessage);
            }
                            if(isset($userData['remember'])){
                                $this->input->set_cookie('uemail', $userData['email'], 86500);
                                $this->input->set_cookie('upassword', $vPass, 86500);
                            } else{
                                delete_cookie('uemail');
                                delete_cookie('upassword');
                            }

                            redirect("recruiter/dashboard");
                        } else {
                            $this->session->set_tempdata('loginerror','Password is incorrect',5);
                            redirect("recruiter/recruiter/index");
                        }
                    } else{
                        $this->session->set_tempdata('loginerror','Email id is incorrect',5);
                        redirect("recruiter/recruiter/index");
                    }
                } else{
                    $this->session->set_tempdata('loginerror','Your Account is blocked',5);
                    redirect("recruiter/recruiter/index");    
                }
            } else{
                $this->session->set_tempdata('loginerror','Please contact admin to get your account approved',5);
                redirect("recruiter/recruiter/index");
            }
             
          } else{
               $this->session->set_tempdata('loginerror','Email Id not exists',5);
               redirect("recruiter/recruiter/index"); 
          }
        }
    }

    public function signup() {
        $data["phonecodes"] = $data = $this->Common_Model->phonecode_lists(); 
        $this->load->view('recruiter/signup', $data);
    }

    public function signupinsert() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|alpha|max_length[25]');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|alpha|max_length[25]');
        $this->form_validation->set_rules('cname', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('domain', 'Domain Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[recruiter.email]');
        $this->form_validation->set_message('is_unique','Email is already registered');
        $this->form_validation->set_message('valid_email','Email is not valid');
        $this->form_validation->set_rules('phonecode', 'Phone Code', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');

          if ($this->form_validation->run() == FALSE) {
              $data['errors'] = $this->form_validation->error_array();
              $data["phonecodes"] = $this->Common_Model->phonecode_lists();
              $data['userData'] = $userData;
              $this->load->view('recruiter/signup',$data);
          } else {

            $domain = $userData['domain'];
            $domain1 = substr($domain, strpos($domain, ".") + 1);;
            $email1 = $userData['email'];
            $email2 = explode('@', $email1);
            
            if($domain1 == $email2[1]) {
                $hash = $this->encryption->encrypt($userData['password']);
                $data1 = ["fname"=>$userData['fname'], "lname"=> $userData['lname'], "cname"=> $userData['cname'], "email"=> $userData['email'], "password"=> $hash, "status"=>0 ];

                $recruiterInserted = $this->Recruit_Model->recruiter_stepone_insert($data1);

                $addLatLong = $this->getLatLong($userData['address']);
                //print_r($addLatLong);die;
                $data2 = ["recruiter_id"=>$recruiterInserted, "phonecode"=>$userData['phonecode'], "phone"=> $userData['phone'], "address"=> $userData['address'], "city"=> $addLatLong['city'], "latitude" => $addLatLong['latitude'], "longitude" => $addLatLong['longitude']];

                $this->Recruit_Model->recruiter_steptwo_insert($data2);

                if($recruiterInserted) {
                    $email = $userData['email'];
                    $name = $userData['fname'];
                    $this->load->library('email');
                    
                    $data['full_name'] = ucfirst($name).' '.ucfirst($userData['lname']);
                    $msg = $this->load->view('recruiter/email',$data,TRUE);
                    $config=array(
                    'charset'=>'utf-8',
                    'wordwrap'=> TRUE,
                    'mailtype' => 'html'
                    );

                    $this->email->initialize($config);
                    $this->email->from("jobyoda123@gmail.com", "JobYoda");
                    $this->email->to($email);
                    $this->email->subject('JobYoda Recruiter - New Registration');
                    /*$msg = "Dear " . $name.' '.$userData['lname'];
                    $msg .= "You have Successfully Registered. You can login into your account once Admin approves your account within 24 hours!";*/

                    $this->email->message($msg);
                    if($this->email->send()){

                    }else{
                        echo $this->email->print_debugger();
                    }
//exit();
                    /*$verifyCheck = $this->Recruit_Model->verification_check($recruiterInserted);
                    if($verifyCheck) {
                        $vCode = ["verifyCode"=>$token];
                        $this->Recruit_Model->verification_update($vCode, $recruiterInserted);
                    } else {
                        $vCode = ["user_id"=>$recruiterInserted, "verifyCode"=>$token];
                        $this->Recruit_Model->verification_insert($vCode);
                    }*/

                    /*$data1['forgotsuccess'] = "Check your Email for verification code";
                    $data1["userId"] = $recruiterInserted;
                    redirect("recruiter/recruiter/loginverify");*/
                    $data1['verifysuccess'] = "Successfully Registered. Now Admin will approve your account within 24 hours!";
                    $this->load->view('recruiter/login', $data1);
                    //$this->load->view('recruiter/newverification', $data1);
                } else {
                    redirect("recruiter/recruiter/signup");
                }
            } else {
                $data['errors'] = ["domain" => "Domain and Email domain did not match"];
                $data['userData'] = $userData;
                $data["phonecodes"] = $this->Common_Model->phonecode_lists();
                $this->load->view('recruiter/signup',$data);
            }
          }
    }

    public function verifysignupemail() {
        $userData = $this->input->post();
        $verifyCheck = $this->Recruit_Model->verification_check($userData['ids']);
        
        if($verifyCheck) {
            if($verifyCheck[0]['verifyCode'] == $userData['codeverify']) {
                redirect("recruiter/recruiter/loginverify");
            } else {
                $data1['verifyerror'] = "Verification code is incorrect";
                $data1["userId"] = $userData['ids'];
                $this->load->view('recruiter/newverification', $data1);
            }
        } else{
            $data1['verifyerror'] = "Check your Email for change password";
            $data1["userId"] = $userData['ids'];
            $this->load->view('recruiter/newverification', $data1);
        }
    }

    public function resendMail() {
        $userData = $this->input->post();
        $dataa = ["id" => $userData['ids']];
        $check = $this->Recruit_Model->recruiter_login($dataa);
        
        if($check) {
            $email = $check[0]['email'];
            $name = $check[0]['fname'];
            $this->load->library('email');
            $this->email->from("ayush.jain@mobulous.com", "JobYoda");
            $this->email->to($email);
            $this->email->subject('JobYoda Recruiter - Resend Email Verification Code');
            $token = $this->getToken();
            $msg = "Dear " . $name;
            $msg .= ". Verify your Email Id by using this code: ";
            $msg .= ". Your verification code is ". $token;
            $this->email->message($msg);
            $this->email->send();

            $verifyCheck = $this->Recruit_Model->verification_check($userData['ids']);
            
            if($verifyCheck) {
                $vCode = ["verifyCode"=>$token];
                $this->Recruit_Model->verification_update($vCode, $userData['ids']);
            } else {
                $vCode = ["user_id"=>$userData['ids'], "verifyCode"=>$token];
                $this->Recruit_Model->verification_insert($vCode);
            }

            $data1['forgotsuccess'] = "Check your Email for change password";
            $data1["userId"] = $userData['ids'];
            $this->load->view('recruiter/newverification', $data1);
        } else{
            $data1['verifyerror'] = "Check your Email for change password";
            $data1["userId"] = $userData['ids'];
            $this->load->view('recruiter/newverification', $data1);
        }
    }

    public function loginverify(){
        $data1['verifysuccess'] = "Successfully Registered. Now Admin will approve your account within 24 hours!";
        $this->load->view('recruiter/login', $data1);
    }

    public function companyprofile() {
        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);

        $data['companyDetails'] = $this->Recruit_Model->company_details_single($userSession['id']);
        
        $data['companySites'] = $this->Recruit_Model->company_sites($userSession['id']);
        //print_r($data['companySites']);die;
        $topPicks = $this->Recruit_Model->company_topicks_single($userSession['id']);
        if($topPicks) { $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];$x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($userSession['id']);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($userSession['id']);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }
        
        $leaves = $this->Recruit_Model->company_leaves_single($userSession['id']);
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leave1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leave2'] = $leave1;
        } else{
            $data['leave2'] = [];
        }
        
        $workshifts = $this->Recruit_Model->company_workshifts_single($userSession['id']);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }

        $this->load->view('recruiter/companyprofile', $data); 
    }

    public function companySite() {
        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
      $data["regions"] = $this->Common_Model->region_lists();
      $data["phonecodes"] = $this->Common_Model->phonecode_lists();
      $data['industryLists'] = $this->Common_Model->industry_lists();
      $topPicks = $this->Recruit_Model->company_topicks_single($userSession['id']);
        if($topPicks) { $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];$x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($userSession['id']);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($userSession['id']);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }
        
        $leaves = $this->Recruit_Model->company_leaves_single($userSession['id']);
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leave1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leave2'] = $leave1;
        } else{
            $data['leave2'] = [];
        }
        
        $workshifts = $this->Recruit_Model->company_workshifts_single($userSession['id']);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }
      $this->load->view('recruiter/addnewsite', $data); 
    }

    public function companySiteInsert() {
        //print_r($_FILES['site_image']);die;
        $userData = $this->input->post();
        if($userData['comm_channel']){
           $channel = implode(',', $userData['comm_channel']) ;
        }
        /*$this->form_validation->set_rules('fname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('cname', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[recruiter.email]');*/
        $this->form_validation->set_rules('site_name', 'Site/Location', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('dayfrom', 'Day', 'trim|required');
        $this->form_validation->set_rules('dayto', 'Day', 'trim|required');
        $this->form_validation->set_rules('from_time', 'From Time', 'trim|required');
        $this->form_validation->set_rules('to_time', 'To Time', 'trim|required');
        $this->form_validation->set_rules('recruiter_contact', 'Recruiter Contact', 'trim|required');
        $this->form_validation->set_rules('recruiter_email', 'Recruiter Email', 'trim|required|valid_email');

        /*$this->form_validation->set_rules('rating', 'Rating', 'trim');
        $this->form_validation->set_rules('gross_salary', 'Gross Salary', 'trim');
        $this->form_validation->set_rules('basic_salary', 'Basic Salary', 'trim');
        $this->form_validation->set_rules('work_off', 'Work Off', 'trim');
        $this->form_validation->set_rules('leave', 'Annual leaves', 'trim');
        $this->form_validation->set_rules('job_type', 'Job Type', 'trim');*/

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $data['userdata'] = $userData;
            $data['industryLists'] = $this->Common_Model->industry_lists();
            $this->load->view('recruiter/addnewsite',$data);
        } else {
            $userSession = $this->session->userdata('userSession');

            $data1 = ["parent_id"=>$userSession['id'], "fname"=>'', "lname"=> '', "cname"=> $userData['site_name'], "email"=> '', "status"=>1 ];
            $recruiterInserted = $this->Recruit_Model->recruiter_stepone_insert($data1);

            $config = array(
                'upload_path' => "./recruiterupload/",
                'allowed_types' => "jpg|png|jpeg",
            );
            $this->load->library('upload', $config);
            
            if($this->upload->do_upload('profilePic'))
            {
                
                $data = array('upload_data' => $this->upload->data());
                $this->resizeImage($data['upload_data']['file_name']);
                $picPath = base_url() ."recruiterupload/". $data['upload_data']['file_name'];
                
            } else {
                $picPath = "";
            }

            $addLatLong = $this->getLatLong($userData['address']);
            
            $data = [
                    "recruiter_id"=>$recruiterInserted,
                    "phone"=> $userData['phone'], 
                    "address"=> $userData['address'],
                    "site_name"=> '',
                    "dayfrom"=> $userData['dayfrom'],
                    "dayto"=> $userData['dayto'],
                    "from_time"=> $userData['from_time'],
                    "to_time"=> $userData['to_time'],
                    "latitude" => $addLatLong['latitude'],
                    "longitude" => $addLatLong['longitude'],
                    "city" => $addLatLong['city'],
                    "region" => $userData['region'],
                    "rphonecode" => $userData['rphonecode'],
                    "recruiter_contact" => $userData['recruiter_contact'],
                    "comm_channel" => $channel,
                    "recruiter_email" => $userData['recruiter_email'],
                    "rating"=> '', 
                    "gross_salary"=> '', 
                    "basic_salary"=> '', 
                    "work_off"=> '', 
                    "annual_leave"=> '', 
                    "job_type"=> '', 
                    "companyDesc" => $userData['compDesc'],
                    "companyPic" => $userData['cropimg'],
                    //"industry" => $userData['industry']
                ];
            $this->Recruit_Model->recruiter_steptwo_insert($data);
            $count = count($_FILES['site_image']['name']);
            //echo $count;die;
            if($count > 0) {
                for($i=0;$i<$count;$i++) {
                    $_FILES['file']['name'] = $_FILES['site_image']['name'][$i];
                      $_FILES['file']['type'] = $_FILES['site_image']['type'][$i];
                      $_FILES['file']['tmp_name'] = $_FILES['site_image']['tmp_name'][$i];
                      $_FILES['file']['error'] = $_FILES['site_image']['error'][$i];
                      $_FILES['file']['size'] = $_FILES['site_image']['size'][$i];
            
                      // Set preference
                      $config['upload_path'] = "./recruiterupload/"; 
                      $config['allowed_types'] = 'jpg|jpeg|png|gif';
                      $config['max_size'] = '5000'; // max_size in kb
                      $config['file_name'] = $_FILES['site_image']['name'][$i];
             
                      //Load upload library
                      $this->load->library('upload',$config); 
             
                      // File upload
                      if($this->upload->do_upload('file')){
                        // Get data about the file
                        $uploadData1 = $this->upload->data();
                        $this->resizeImage($uploadData1['file_name']);
                        $filename = base_url() ."recruiterupload/".$uploadData1['file_name'];
                        //print_r($filename);        
                        // Initialize array
                        $data['filenames'][] = $filename;
                      }else{
                          $filename="";
                      }
                    $data1 = ["recruiter_id"=>$recruiterInserted,"pic" => $filename];
                    $this->Recruit_Model->site_image_insert($data1);
                }
                 
            }
            if(isset($userData['toppicks'])) {
                $topPicks = $userData['toppicks'];
                $x=0;
                foreach ($topPicks as $topPick) {
                    $dataPicks[$x] = ["recruiter_id"=>$recruiterInserted, "picks_id"=>$topPick];
                    $x++;
                }
                $this->Recruit_Model->recruiter_toppicks_insert($dataPicks);
            }
            if(isset($userData['allowances'])) {
                $topAllos = $userData['allowances'];
                $x=0;
                foreach ($topAllos as $topAllo) {
                    $dataAllos[$x] = ["recruiter_id"=>$recruiterInserted, "allowances_id"=>$topAllo];
                    $x++;
                }
                $this->Recruit_Model->recruiter_allowances_insert($dataAllos);
            }
            if(isset($userData['medical'])) {
                $topMedis = $userData['medical'];
                $x=0;
                foreach ($topMedis as $topMedi) {
                    $dataMedis[$x] = ["recruiter_id"=>$recruiterInserted, "medical_id"=>$topMedi];
                    $x++;
                }
                $this->Recruit_Model->recruiter_medical_insert($dataMedis);
            }
            if(isset($userData['shifts'])) {
                $topShifts = $userData['shifts'];
                $x=0;
                foreach ($topShifts as $topShift) {
                    $dataShifts[$x] = ["recruiter_id"=>$recruiterInserted, "workshift_id"=>$topShift];
                    $x++;
                }
                $this->Recruit_Model->recruiter_shifts_insert($dataShifts);
            }
            if(isset($userData['leavs'])) {
                $topLeaves = $userData['leavs'];
                $x=0;
                foreach ($topLeaves as $topLeave) {
                    $dataLeaves[$x] = ["recruiter_id"=>$recruiterInserted, "leaves_id"=>$topLeave];
                    $x++;
                }
                $this->Recruit_Model->recruiter_leaves_insert($dataLeaves);
            }

            $this->session->set_tempdata('inserted','Company site added Successfully',5);
            redirect("recruiter/recruiter/companyprofile");
        }
    }

    public function insertsubrecruiter() {
        $userData = $this->input->post();
        //echo $domain = $userData['domain'];die;
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|callback_alpha_dash_space|max_length[25]');
        $this->form_validation->set_message('alpha','Name field should contain only alphabets');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|callback_alpha_dash_space|max_length[25]');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[recruiter.email]');
        $this->form_validation->set_message('is_unique','Email is already registered');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['userdata'] = $userData;
            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $this->load->view('recruiter/addrecruiter',$data);
        
        } else {
        
            $domain = $userData['domain'];
            $domain1 = substr($domain, strpos($domain, ".") + 1);;
            $email1 = $userData['email'];
            $email2 = explode('@', $email1);
        
            if(strtolower($domain1) == strtolower($email2[1])) {
                $userSession = $this->session->userdata('userSession');

                $data1 = ["parent_id"=>$userSession['id'], "fname"=>$userData['fname'], "lname"=> $userData['lname'], "cname"=> '', "email"=> $userData['email'], "password" => '' , "status"=>1, "label" => 3 ];
                $recruiterInserted = $this->Recruit_Model->recruiter_stepone_insert($data1);
                
                $data = [
                    "recruiter_id"=>$recruiterInserted,
                    "phone"=> $userData['phone'], 
                    "address"=> '',
                    "site_name"=> '',
                    "dayfrom"=> '',
                    "dayto"=> '',
                    "from_time"=> '',
                    "to_time"=> '',
                    "latitude" => '',
                    "longitude" => '',
                    "city" => '',
                    "region" => '',
                    "phonecode" => $userData['phonecode'],
                    "recruiter_contact" => '',
                    "comm_channel" => '',
                    "recruiter_email" => '',
                    "rating"=> '', 
                    "gross_salary"=> '', 
                    "basic_salary"=> '', 
                    "work_off"=> '', 
                    "annual_leave"=> '', 
                    "job_type"=> '', 
                    "companyDesc" => '',
                    "companyPic" => '',
                    //"industry" => $userData['industry']
                ];
                
                $this->Recruit_Model->recruiter_steptwo_insert($data);

                $datapermission = ["subrecruiter_id"=>$recruiterInserted, "site_details"=>1, 'invoice_view' => 0, 'edit_delete' => 1];
                $this->Recruit_Model->permission_insert($datapermission);
                
                if($recruiterInserted) {
                    
                    $email = $userData['email'];
                    $name = $userData['fname'];
                    
                    $this->load->library('email');
                    
                    $data['full_name'] = ucfirst($name).' '.ucfirst($userData['lname']);
                    $data['user_id'] = $recruiterInserted;
                    $data['token'] = $this->getToken();
                    
                    $msg = $this->load->view('recruiter/subrecruiteremail',$data,TRUE);

                    $config =[
                          'protocol' => 'smtp',
                          'smtp_host' => 'smtpout.asia.secureserver.net',
                          'smtp_user' => 'help@jobyoda.com',
                          'smtp_pass' => 'Usa@1234567',
                          'smtp_port' => 465,
                          'smtp_crypto' => 'ssl',
                          'charset'=>'utf-8',
                          'mailtype' => 'html',
                          'crlf' => "\r\n",
                          'newline' => "\r\n"
                        ];

                    $this->email->initialize($config);
                    $this->email->from("help@jobyoda.com", "JobYoDA");
                    $this->email->to($userData['email']);
                    $this->email->subject('JobYoDA Recruiter - Confirmation Email');
                    $this->email->message($msg);

                    $token = $data['token'];
                    
                    if($this->email->send()) {

                        $forgotPassCheck = $this->Recruit_Model->forgotPass_check($recruiterInserted);
                    
                        if($forgotPassCheck) {
                            $vCode = ["verifyCode"=>$token];
                            $this->Recruit_Model->forgotPass_update($vCode, $recruiterInserted);
                        } else {
                            $vCode = ["user_id"=>$recruiterInserted, "verifyCode"=>$token];
                            $this->Recruit_Model->forgotPass_insert($vCode);
                        }
                    } else {
                        echo $this->email->print_debugger();
                    }

                    $data1['verifysuccess'] = "Successfully Registered. Now the account can login after confirmation";
                    $this->load->view('recruiter/login', $data1);

                } else {
                    redirect("recruiterN/recruiter/addrecruiter");
                }
                
                $this->session->set_tempdata('inserted','SubRecruiter Added Successfully.',5);
                redirect("recruiter/subrecuriterlist");
            
            } else {
                $data['errors'] = ["domain" => "Domain and Email domain did not match"];
                $data['userdata'] = $userData;
                $data["phonecodes"] = $this->Common_Model->phonecode_lists();
                $this->load->view('recruiter/addrecruiter',$data);
            }
        }
    }

    public function updatesubrecruiter() {
        $userData = $this->input->post();
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required|alpha|max_length[25]');
        $this->form_validation->set_message('alpha','Name field should contain only alphabets');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required|alpha|max_length[25]');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['userdata'] = $userData;
            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $this->load->view('recruiter/addrecruiter',$data);
        } else {
            $userSession = $this->session->userdata('userSession');

            $data1 = ["fname"=>$userData['fname'], "lname"=> $userData['lname'], "email"=> $userData['email'] ];
            $recruiterInserted = $this->Recruit_Model->recruiter_stepone_update($data1, $userData['rid']);
            
            $data = [
                    "phone"=> $userData['phone'], 
                    "phonecode"=> $userData['phonecode'], 
                ];
            $this->Recruit_Model->recruiter_steptwo_update($data, $userData['rid']);
           if($recruiterInserted) {
                    //$data1['verifysuccess'] = "Successfully Registered. Now the account can login after confirmation";
                    $this->load->view('recruiter/recruiterlist');
                    //$this->load->view('recruiter/newverification', $data1);
                } else {
                    redirect("recruiterN/recruiter/recruiterlist");
                }
            $this->session->set_tempdata('inserted','SubRecruiter Updated Successfully',5);
            redirect("recruiterN/recruiter/recruiterlist");
        }
    }

    public function updatepermission(){
         if($this->session->userdata('userSession')) {
            
            } else {
                redirect("recruiter/recruiter/index");
            }
            $userData = $this->input->post();
            if(!empty($userData['site_details']) && $userData['site_details']=='on'){
                $site_details = 1;
            }else{
                $site_details = 0;
            }
            if(!empty($userData['invoice_view']) && $userData['invoice_view']=='on'){
                $invoice_view = 1;
            }else{
                $invoice_view = 0;
            }
            if(!empty($userData['edit_delete']) && $userData['edit_delete']=='on'){
                $edit_delete = 1;
            }else{
                $edit_delete = 0;
            }
            
            $datapermission = ["subrecruiter_id  "=>$userData['subrecruiterid'], "site_details"=>$site_details, 'invoice_view' => $invoice_view, 'edit_delete' => $edit_delete];
            $this->Recruit_Model->permission_delete($userData['subrecruiterid']);
            $this->Recruit_Model->permission_insert($datapermission);
             $this->session->set_tempdata('inserted','Updated Successfully',5);
            redirect("recruiterN/recruiter/recruiterlist");
    }
    
    public function manageCompany() {
        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);
        $data['companyDetails'] = $this->Recruit_Model->company_details_single($userSession['id']);
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
        $data['industryLists'] = $this->Common_Model->industry_lists();
        $topPicks = $this->Recruit_Model->company_topicks_single($userSession['id']);
       // print_r($topPicks);die;
        if($topPicks) { $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];$x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($userSession['id']);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($userSession['id']);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }
        $leaves = $this->Recruit_Model->company_leaves_single($userSession['id']);
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leave1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leave2'] = $leave1;
        } else{
            $data['leave2'] = [];
        }    
        $workshifts = $this->Recruit_Model->company_workshifts_single($userSession['id']);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }

        $this->load->view('recruiter/managesites', $data); 
    }

    public function companyupdate() {
        $userSession = $this->session->userdata('userSession');
        $userData = $this->input->post();

        /*echo "<pre>";
        print_r($userData);die;*/
        $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
        $this->form_validation->set_rules('lname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('cname', 'Company Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|is_unique[recruiter.email]');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');

        /*$this->form_validation->set_rules('rating', 'Rating', 'trim');
        $this->form_validation->set_rules('gross_salary', 'Gross Salary', 'trim');
        $this->form_validation->set_rules('basic_salary', 'Basic Salary', 'trim');
        $this->form_validation->set_rules('work_off', 'Work Off', 'trim');
        $this->form_validation->set_rules('leave', 'Annual leaves', 'trim');
        $this->form_validation->set_rules('job_type', 'Job Type', 'trim');*/

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data["phonecodes"] = $this->Common_Model->phonecode_lists();
            $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);
            $data['companyDetails'] = $this->Recruit_Model->company_details_single($userSession['id']);
            $data['industryLists'] = $this->Common_Model->industry_lists();
            $this->load->view('recruiter/managesites',$data);
        } else {
            $data1 = ["fname"=>$userData['fname'], "lname"=> $userData['lname'], "cname"=> $userData['cname'], "email"=> $userData['email'] ];

            $recruiterInserted = $this->Recruit_Model->recruiter_stepone_update($data1, $userSession['id']);
            //echo $recruiterInserted;die;
            if($recruiterInserted) {
                if($_FILES['profilePic']['tmp_name']) {
                    $config = array(
                        'upload_path' => "./recruiterupload/",
                        'allowed_types' => "jpg|png|jpeg",
                    );
                    $this->load->library('upload', $config);
                    
                    if($this->upload->do_upload('profilePic'))
                    {
                        $data = array('upload_data' => $this->upload->data());
                        $picPath = base_url() ."recruiterupload/". $data['upload_data']['file_name'];
                    } else {
                        $picPath = " ";
                    }
                } else{
                    $picPath = " ";
                }

                $addLatLong = $this->getLatLong($userData['address']);

                if($picPath == " "){
                    $data2 = ["phone"=> $userData['phone'], "address"=> $userData['address'], "latitude" => $addLatLong['latitude'], "longitude" => $addLatLong['longitude'], "rating"=> '', "gross_salary"=> '', "basic_salary"=> '', "work_off"=> '', "annual_leave"=> '', "job_type"=> '', "companyDesc" => $userData['compDesc'], "industry" => $userData['industry']];
                }else{
                $data2 = ["phone"=> $userData['phone'], "address"=> $userData['address'], "latitude" => $addLatLong['latitude'], "longitude" => $addLatLong['longitude'], "rating"=> '', "gross_salary"=> '', "basic_salary"=> '', "work_off"=> '', "annual_leave"=> '', "job_type"=> '', "companyDesc" => $userData['compDesc'],  "industry" => $userData['industry'] ,"companyPic" => $userData['cropimg']];
                }
                $this->Recruit_Model->recruiter_steptwo_update($data2, $userSession['id']);
               // echo $this->db->last_query();die;            
                
                if(isset($userData['toppicks'])) {
                    $topPicks = $userData['toppicks'];
                    $x=0;

                    foreach ($topPicks as $topPick) {
                        $dataPicks[$x] = ["recruiter_id"=>$userSession['id'], "picks_id"=>$topPick];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_toppicks_delete($userSession['id']);
                    $this->Recruit_Model->recruiter_toppicks_insert($dataPicks);
                }
                if($userData['allowances']) {
                    $topAllos = $userData['allowances'];
                    $x=0;
                    foreach ($topAllos as $topAllo) {
                        $dataAllos[$x] = ["recruiter_id"=>$userSession['id'], "allowances_id"=>$topAllo];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_allowances_delete($userSession['id']);
                    $this->Recruit_Model->recruiter_allowances_insert($dataAllos);
                }
                if($userData['medical']) {
                    $topMedis = $userData['medical'];
                    $x=0;
                    foreach ($topMedis as $topMedi) {
                        $dataMedis[$x] = ["recruiter_id"=>$userSession['id'], "medical_id"=>$topMedi];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_medical_delete($userSession['id']);
                    $this->Recruit_Model->recruiter_medical_insert($dataMedis);
                }
                if($userData['leavs']) {
                    $topLeaves = $userData['leavs'];
                    $x=0;
                    foreach ($topLeaves as $topLeave) {
                        $dataLeaves[$x] = ["recruiter_id"=>$userSession['id'], "leaves_id"=>$topLeave];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_leaves_delete($userSession['id']);
                    $this->Recruit_Model->recruiter_leaves_insert($dataLeaves);
                }
                if($userData['shifts']) {
                    $topShifts = $userData['shifts'];
                    $x=0;
                    foreach ($topShifts as $topShift) {
                        $dataShifts[$x] = ["recruiter_id"=>$userSession['id'], "workshift_id"=>$topShift];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_shifts_delete($userSession['id']);
                    $this->Recruit_Model->recruiter_shifts_insert($dataShifts);
                }

                $this->session->set_tempdata('inserted','Company details updated successfully',5);
                redirect("recruiter/recruiter/companyprofile");
            } else {
                redirect("recruiter/recruiter/manageCompany");
            }
        }
    }

    public function companySiteView() {
        if($this->session->userdata('userSession')) {
            
        } else {
            redirect("recruiter/recruiter/index");
        }
        $userSession = $this->session->userdata('userSession');
        $view = $_GET['view'];
        $data['companyImg'] = $this->Recruit_Model->company_images($view);
        $data['companyDetails'] = $this->Recruit_Model->companysite_details_single1($userSession['id'], $view);
        //echo $this->db->last_query();die;
        $data['companySites'] = $this->Recruit_Model->company_sites1($userSession['id'], $view);
        $topPicks = $this->Recruit_Model->company_topicks_single($view);
        if($topPicks) { $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];$x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($view);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($view);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }
        $leaves = $this->Recruit_Model->company_leaves_single($view);
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leave1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leave2'] = $leave1;
        } else{
            $data['leave2'] = [];
        }
        $workshifts = $this->Recruit_Model->company_workshifts_single($view);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }

        //echo $this->db->last_query();die;
        $this->load->view('recruiter/sitedetails', $data);
    }

    public function fetchRecruiterSingleData($id) {
                $recruiterDatas = $this->Recruit_Model->recruiter_single($id);
        foreach($recruiterDatas as $recruiterData) {
            $rid = $recruiterData['id'];

            $recruitpermission = $this->Recruit_Model->recruiter_permission($rid);
            if(!empty($recruitpermission[0]['site_details'])){
                $recruitpermission[0]['site_details'] = $recruitpermission[0]['site_details'];
            }else{
                $recruitpermission[0]['site_details'] = '';
            }
            if(!empty($recruitpermission[0]['invoice_view'])){
                $recruitpermission[0]['invoice_view'] = $recruitpermission[0]['invoice_view'];
            }else{
                $recruitpermission[0]['invoice_view'] = '';
            }
            if(!empty($recruitpermission[0]['edit_delete'])){
                $recruitpermission[0]['edit_delete'] = $recruitpermission[0]['edit_delete'];
            }else{
                $recruitpermission[0]['edit_delete'] = '';
            }
            //print_r($recruitpermission);die;
            $recruitData = ["id" => $recruiterData['id'], "parent_id" => $recruiterData['parent_id'], "label" => $recruiterData['label'], "fname" => $recruiterData['fname'], "lname" => $recruiterData['lname'], "email" => $recruiterData['email'], 'site_details' => $recruitpermission[0]['site_details'], "invoice_view" => $recruitpermission[0]['invoice_view'], "edit_delete" => $recruitpermission[0]['edit_delete'] ];
         }
        
        //$data = ["id" => $recruiterData[0]['id'], "fname" => $recruiterData[0]['fname'], "lname" => $recruiterData[0]['lname'], "email" => $recruiterData[0]['email']];
        return  $recruitData;
    }

    public function getLatLong($address){
        if(!empty($address)){
            //Formatted address
            $formattedAddr = str_replace(' ','+',$address);
            //Send request and receive json data by address
            $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&sensor=false&key=AIzaSyCHu8t8gkBK-yT0hkzrp5P5Fa51kFNUjUk'); 
            $output = json_decode($geocodeFromAddr);
            //Get latitude and longitute from json data
            $data['latitude']  = $output->results[0]->geometry->location->lat; 
            $data['longitude'] = $output->results[0]->geometry->location->lng;
            //var_dump($output->results[0]->address_components[0]->types);die;
            for($j=0;$j<count($output->results[0]->address_components);$j++){
                $city = $output->results[0]->address_components[$j]->types;
                if($city[0]=="locality"){
                     $data['city'] = $output->results[0]->address_components[$j]->long_name;    
                }
            }
            //Return latitude and longitude of the given address
            if(!empty($data)) {
                return $data;
            }else{
                return false;
            }
        }else{
            return false;   
        }
    }
    
    public function resizeImage($filename)
       {
          $source_path =  './recruiterupload/' . $filename;
          $target_path = './recruiterupload/thumbs/';
          $config_manip = array(
              'image_library' => 'gd2',
              'source_image' => $source_path,
              'new_image' => $target_path,
              'maintain_ratio' => FALSE,
              'create_thumb' => FALSE,
              'width' => 150,
              'height' => 150
          );
    
    
          $this->load->library('image_lib', $config_manip);
          if (!$this->image_lib->resize()) {
              echo $this->image_lib->display_errors();
          }
    
    
          //$this->image_lib->clear();
       }

    public function forgotPassword() {
        $userData = $this->input->post();

        if(!empty($userData['email'])) {
            $data = ["email" => $userData['email']];
            $userTokenCheck = $this->Recruit_Model->email_match($data);
            if($userTokenCheck) {
                $email = $userTokenCheck[0]['email'];
                $name = $userTokenCheck[0]['fname'];
                /*$this->load->library('email');
                $this->email->from("ayush.jain@mobulous.com", "JobYoda");
                $this->email->to($email);
                $this->email->subject('JobYoda Recruiter- Forgot Password Link');
                $token = $this->getToken();
                $msg = "Dear " . $name;
                $msg .= ", Please click on the below link to change your account password: ";
                //$hash = $this->encryption->encrypt($userTokenCheck[0]['id']);
                $msg .= base_url() ."recruiter/recruiter/passwordChange/".$userTokenCheck[0]['id'];
                $msg .= ". Your verification code is ". $token;
                $this->email->message($msg);*/
                $this->load->library('email');
                    $token = $this->getToken();
                    $data['full_name'] = ucfirst($name).' '.ucfirst($userTokenCheck[0]['lname']);
                    $data['token'] = $this->getToken();
                    $data['user_id'] = $userTokenCheck[0]['id'];
                    $msg = $this->load->view('recruiter/passwordemail',$data,TRUE);
                    $config=array(
                    'charset'=>'utf-8',
                    'wordwrap'=> TRUE,
                    'mailtype' => 'html'
                    );

                    $this->email->initialize($config);
                    $this->email->from("jobyoda123@gmail.com", "JobYoDA");
                    $this->email->to($email);
                    $this->email->subject('JobYoDA Recruiter- Forgot Password Link');
                    /*$msg = "Dear " . $name.' '.$userData['lname'];
                    $msg .= "You have Successfully Registered. You can login into your account once Admin approves your account within 24 hours!";*/

                    $this->email->message($msg);
                    
                
                if($this->email->send()) {
                    $forgotPassCheck = $this->Recruit_Model->forgotPass_check($userTokenCheck[0]['id']);
                    
                    if($forgotPassCheck) {
                        $vCode = ["verifyCode"=>$token];
                        $this->Recruit_Model->forgotPass_update($vCode, $userTokenCheck[0]['id']);
                    } else {
                        $vCode = ["user_id"=>$userTokenCheck[0]['id'], "verifyCode"=>$token];
                        $this->Recruit_Model->forgotPass_insert($vCode);
                    }
                    //$data1['forgotsuccess'] = "";
                    $this->session->set_tempdata('forgotsuccess','A verification link and code is sent to your registered email id. Please check your email id to change your password.',1);
                    $this->load->view('recruiter/login');
                }
            } else{
                $data1['forgoterror'] = "Bad Request";
                $this->load->view('recruiter/login', $data1);
            }
        } else{
            redirect('recruiter/recruiter/index');
        }
    }

    public function passwordChange() {
        $this->load->view('recruiter/forgot');
    }

    public function createpassword() {
        $this->load->view('recruiter/createpassword');
    }

    public function generatePassword() {
        $userData = $this->input->post();
        //print_r($userData);die;
        $encrypt_id = $userData['id'];
        //echo $encrypt_id;die;
        $forgotPassCheck = $this->Recruit_Model->forgotPass_check($encrypt_id);
            //echo $this->db->last_query();die;
            //print_r($forgotPassCheck);die;
            $this->form_validation->set_rules('newpassword', 'New Password', 'trim|required|callback_valid_password');
            $this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'trim|required|matches[newpassword]');
            $this->form_validation->set_rules('verifyCode', 'Verify Code', 'trim|required');
            
            if ($this->form_validation->run() == FALSE) { 
                $data1['errors'] = $this->form_validation->error_array();
                $data1['id']=$encrypt_id;
                $data1['userData'] = $userData;
                $this->load->view('recruiter/createpassword', $data1);
            } else{
                $newPass = $userData['newpassword'];
                //echo $newPass;die;
                $verifyCode = $userData['verifyCode'];
            if($forgotPassCheck[0]['verifyCode'] == $verifyCode)  {
                $hash = $this->encryption->encrypt($newPass);
                $data = ["password" => $hash];
                $this->Recruit_Model->password_update($data, $encrypt_id);
                $vCode = ["verifyCode"=> " "];
                $this->Recruit_Model->forgotPass_update($vCode, $encrypt_id);
                redirect("recruiterN/recruiter/loginPage");
            } else{
                $data1['forgoterror'] = "Verification code incorrect";
                $data1['id'] = $encrypt_id;
                $data1['userData'] = $userData;
                $this->load->view("recruiter/createpassword", $data1);    
            }
            }   
        //$dec_id = $this->encryption->decrypt($encrypt_id);
        /*$data = ["id" => $encrypt_id];
        
        $userTokenCheck = $this->Recruit_Model->email_match($data);
        //echo $this->db->last_query();die;
        //print_r($userTokenCheck);die;
        if($userTokenCheck) {
            $newPass = $userData['newpassword'];
            $confirmPass = $userData['confirmpassword'];
            $verifyCode = $userData['verifyCode'];
            
            $forgotPassCheck = $this->Recruit_Model->forgotPass_check($userTokenCheck[0]['id']);
            if($newPass == $confirmPass){
               if($forgotPassCheck[0]['verifyCode'] == $verifyCode)  {
                $hash = $this->encryption->encrypt($newPass);
                $data = ["password" => $hash];
                $this->Recruit_Model->password_update($data, $userTokenCheck[0]['id']);
                $vCode = ["verifyCode"=> " "];
                $this->Recruit_Model->forgotPass_update($vCode, $userTokenCheck[0]['id']);
                redirect("recruiterN/recruiter/loginPage");
            } else{
                $data1['forgoterror'] = "Verification code incorrect";
                $data1['id'] = $encrypt_id;
                $this->load->view("recruiter/createpassword", $data1);    
            } 
        }else{
            $data1['forgoterror'] = "Confirm Password does not match";
                $data1['id'] = $encrypt_id;
                $this->load->view("recruiter/createpassword", $data1);
        }
            

        } else{
            $data1['forgoterror'] = "Please fill the fields";
            $data1['id'] = $encrypt_id;
            $this->load->view("recruiter/createpassword", $data1);
        }*/
    }

    public function resetPassword() {
        $userData = $this->input->post();
        $encrypt_id = $userData['id'];
        //$dec_id = $this->encryption->decrypt($encrypt_id);
        $data = ["id" => $encrypt_id];
        
        $userTokenCheck = $this->Recruit_Model->email_match($data);
        
        if($userTokenCheck) {
            $newPass = $userData['newpassword'];
            $verifyCode = $userData['verifyCode'];
            
            $forgotPassCheck = $this->Recruit_Model->forgotPass_check($userTokenCheck[0]['id']);

            if($forgotPassCheck[0]['verifyCode'] == $verifyCode)  {
                $hash = $this->encryption->encrypt($newPass);
                $data = ["password" => $hash];
                $this->Recruit_Model->password_update($data, $userTokenCheck[0]['id']);
                $vCode = ["verifyCode"=> " "];
                $this->Recruit_Model->forgotPass_update($vCode, $userTokenCheck[0]['id']);
                redirect("recruiter/recruiter/loginPage");
            } else{
                $data1['forgoterror'] = "Verification code incorrect";
                $data1['id'] = $encrypt_id;
                $this->load->view("recruiter/forgot", $data1);    
            }

        } else{
            $data1['forgoterror'] = "Email id does not match";
            $data1['id'] = $encrypt_id;
            $this->load->view("recruiter/forgot", $data1);
        }
    }
    
    public function loginPage() {
        $this->session->set_tempdata('changesuccess','Password created sucessfully',1);
        $this->load->view('recruiter/login');
    }
    
    public function addExperience() {
        $userSession = $this->session->userdata('userSession');
        $data['addresses'] = $this->Common_Model->companysite_address_lists($userSession['id']);
        $data['expData'] = $this->Recruit_Model->recruiter_exp_fetch($userSession['id']);
        
        if(isset($_GET['id'])) {
            $idd = $_GET['id'];
            $data['expSingle'] = $this->Recruit_Model->recruiter_exp_singlefetch($idd, $userSession['id']);
        }
        $this->load->view('recruiter/addExp', $data);
    }

    public function experienceInsert() {
        $expData = $this->input->post();
        $userSession = $this->session->userdata('userSession');
        $data = ["recruiter_id"=> $userSession['id'], "comp_id"=> $expData['recruiter_id'], "exp" => $expData['exp']];
        $this->Recruit_Model->recruiter_exp_insert($data);
        redirect("recruiter/recruiter/addExperience");
    }

    public function experienceUpdate() {
        $expData = $this->input->post();
        $data = ["comp_id"=> $expData['recruiter_id'], "exp" => $expData['exp']];
        $this->Recruit_Model->recruiter_exp_update($data, $expData['eid']);
        //echo $this->db->last_query(); die;
        redirect("recruiter/recruiter/addExperience");
    }

    public function experienceDelete() {
        $expData = $this->input->get();
        $this->Recruit_Model->recruiter_exp_delete($expData['id']);
       // echo $this->db->last_query();die;
        redirect("recruiter/recruiter/addExperience");
    }

    public function deleteJob(){
        $id = $this->input->get('id');
        $this->Jobpost_Model->deleteJob($id);
    }

    public function deleteRecruiter(){
        $id = $this->input->get('id');
        $this->Recruit_Model->deleteSubRecruiter($id);
        redirect("recruiterN/recruiter/recruiterlist");
        //echo $this->db->last_query();die;
    }

    public function deleteSite(){
        $id = $this->input->get('id');
        $this->Recruit_Model->site_delete($id);
        $this->Recruit_Model->sitedetails_delete($id);
        $this->Recruit_Model->recruiter_toppicks_delete($id);
        $this->Recruit_Model->recruiter_allowances_delete($id);
        $this->Recruit_Model->recruiter_medical_delete($id);
        redirect('recruiter/recruiter/companyprofile');
    }
    
    public function transaction() {
         $userSession = $this->session->userdata('userSession');
         $data['invoices'] = $this->Recruit_Model->get_invoices_data($userSession['id']);
         $this->load->view('recruiter/transaction',$data);
    }
    
    public function glassdoor() {
        $this->load->view('recruiter/glassdoor');
    }
    
    public function rating() {
         $userSession = $this->session->userdata('userSession');
         $data['review_list'] = $this->Jobpost_Model->review_lists($userSession['id']);

         $companySites = $this->Recruit_Model->company_sites($userSession['id']);
         $compData = array();
         
         foreach($companySites as $companySite) {
            $cid = $companySite['id'];

            $reviewlists = $this->Jobpost_Model->review_lists($cid);
            $average_rating = $this->Jobpost_Model->fetch_companyRating($cid);
            $company_rating = $this->Jobpost_Model->fetch_gcompanyRating($cid);
            if(!empty($company_rating[0]['rating'])){
              $company_rating[0]['rating']= $company_rating[0]['rating'];
            }else{
              $company_rating[0]['rating']='';
            }
            //print_r($company_rating);die;
            $compData[] = ["cname"=>$companySite['cname'], "companyPic"=>$companySite['companyPic'], "companyDesc"=>$companySite['companyDesc'], "address"=>$companySite['address'], "review" => $reviewlists, 'rating' => $average_rating[0]['average'], 'grating' => $company_rating[0]['rating']];
         }
         $data['comLists'] = $compData;
        $this->load->view('recruiter/rating',$data);
    }

    public function editSite() {
        $view = $this->input->get('view');
        $userSession = $this->session->userdata('userSession');
        
        $data['companie'] = $this->Recruit_Model->company_single($view);
        $data['companyDetails'] = $this->Recruit_Model->company_details_single1($view);
        $data['companyImg'] = $this->Recruit_Model->company_images($view);
        $data["phonecodes"] = $this->Common_Model->phonecode_lists();
         $data["regions"] = $this->Common_Model->region_lists();
          $data['industryLists'] = $this->Common_Model->industry_lists();
        $topPicks = $this->Recruit_Model->company_topicks_single($view);
        if($topPicks) { $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];$x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($view);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($view);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }

        $leaves = $this->Recruit_Model->company_leaves_single($view);
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leave1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leave2'] = $leave1;
        } else{
            $data['leave2'] = [];
        }
        
        $workshifts = $this->Recruit_Model->company_workshifts_single($view);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }

        $this->load->view('recruiter/editSite', $data); 
    }
    
    public function siteupdate() {
        //print_r($_FILES);die;
          //      echo $count;die;
        $userSession = $this->session->userdata('userSession');
        $userData = $this->input->post();
        if($userData['comm_channel']){
           $channel = implode(',', $userData['comm_channel']) ;
        }
        //echo "<PRE>";
        //var_dump($_FILES['site_image11']['name']);die;
        $this->form_validation->set_rules('site_name', 'Site Name', 'trim|required');
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        /*$this->form_validation->set_rules('recruiter_contact', 'Recruiter Contact', 'trim|required');
        $this->form_validation->set_rules('recruiter_email', 'Recruiter Email', 'trim|required');*/

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);
            $data['companyDetails'] = $this->Recruit_Model->company_details_single1($userData['rid']);
            $data['industryLists'] = $this->Common_Model->industry_lists();
            $comp_id = $userData['rid'];
            $this->load->view("recruiter/editSite",$data);
        } else {
            /*$data1 = ["fname"=>$userData['fname'], "lname"=> $userData['lname'], "cname"=> $userData['cname'] ];

            $recruiterInserted = $this->Recruit_Model->recruiter_stepone_update($data1, $userSession['id']);*/

            /*if($recruiterInserted) {*/
                
                if($_FILES['profilePic']['tmp_name']) {
                  $config = array(
                      'upload_path' => "./recruiterupload/",
                      'allowed_types' => "jpg|png|jpeg",
                  );
                  $this->load->library('upload', $config);
                  
                  if($this->upload->do_upload('profilePic'))
                  {
                      $data = array('upload_data' => $this->upload->data());
                      $this->resizeImage($data['upload_data']['file_name']);
                      $picPath = base_url() ."recruiterupload/". $data['upload_data']['file_name'];
                  } else {
                      $picPath = " ";
                  }
                } else {
                      $picPath = " ";
                }
                $addLatLong = $this->getLatLong($userData['address']);
                
                if($picPath == " ") {
                    $data2 = ["site_name"=> $userData['site_name'], 
                            "dayfrom"=> $userData['dayfrom'],
                            "dayto"=> $userData['dayto'], 
                            "from_time"=> $userData['from_time'], 
                            "to_time"=> $userData['to_time'], 
                            "rphonecode"=> $userData['rphonecode'], 
                            "recruiter_contact"=> $userData['recruiter_contact'], 
                            "comm_channel"=> $channel, 
                            "recruiter_email"=> $userData['recruiter_email'], 
                            "phonecode"=> $userData['phonecode'], 
                            "phone"=> $userData['phone'], 
                            "address"=> $userData['address'], 
                            "city"=> $addLatLong['city'], 
                            "region" => $userData['region'],
                            "latitude" => $addLatLong['latitude'], 
                            "longitude" => $addLatLong['longitude'], 
                            "rating"=> '', 
                            "gross_salary"=> '', 
                            "basic_salary"=> '', 
                            "work_off"=> '', 
                            "annual_leave"=> '', 
                            "job_type"=> '', 
                            "recruiter_contact" => $userData['recruiter_contact'],
                            "recruiter_email" => $userData['recruiter_email'],
                            "companyDesc" => $userData['compDesc'],
                            //"industry" => $userData['industry']
                        ];
    
                } else{
                    $data2 = ["site_name"=> $userData['site_name'], 
                            "dayfrom"=> $userData['dayfrom'],
                            "dayto"=> $userData['dayto'], 
                            "from_time"=> $userData['from_time'], 
                            "to_time"=> $userData['to_time'],
                            "rphonecode"=> $userData['rphonecode'], 
                            "recruiter_contact"=> $userData['recruiter_contact'], 
                            "recruiter_email"=> $userData['recruiter_email'],
                            "phonecode"=> $userData['phonecode'], 
                            "phone"=> $userData['phone'], 
                            "address"=> $userData['address'], 
                            "city"=> $addLatLong['city'],
                            "region" => $userData['region'],
                            "latitude" => $addLatLong['latitude'], 
                            "longitude" => $addLatLong['longitude'], 
                            "rating"=> '', 
                            "gross_salary"=> '', 
                            "basic_salary"=> '', 
                            "work_off"=> '', 
                            "annual_leave"=> '', 
                            "job_type"=> '', 
                            "recruiter_contact" => $userData['recruiter_contact'],
                            "recruiter_email" => $userData['recruiter_email'],
                            "companyDesc" => $userData['compDesc'], 
                            "companyPic" => $userData['cropimg'],
                            "industry" => $userData['industry']];

                }
                $this->Recruit_Model->recruiter_steptwo_update($data2, $userData['rid']);
                //echo $this->db->last_query();die;
                $count = count($_FILES['site_image11']['name']);
                //echo $count;die;
                if($count > 0) {
                    
                    for($i=0;$i<$count;$i++) {
                        $_FILES['file']['name'] = $_FILES['site_image11']['name'][$i];
                          $_FILES['file']['type'] = $_FILES['site_image11']['type'][$i];
                          $_FILES['file']['tmp_name'] = $_FILES['site_image11']['tmp_name'][$i];
                          $_FILES['file']['error'] = $_FILES['site_image11']['error'][$i];
                          $_FILES['file']['size'] = $_FILES['site_image11']['size'][$i];
                
                          // Set preference
                          $config['upload_path'] = "./recruiterupload/"; 
                          $config['allowed_types'] = 'jpg|jpeg|png|gif';
                          $config['max_size'] = '5000'; // max_size in kb
                          $config['file_name'] = $_FILES['site_image11']['name'][$i];
                 
                          //Load upload library
                          $this->load->library('upload',$config); 
                 
                          // File upload
                          if($this->upload->do_upload('file')){
                            // Get data about the file
                            $uploadData = $this->upload->data();
                            $this->resizeImage($uploadData['file_name']);
                            $filename = base_url() ."recruiterupload/".$uploadData['file_name'];
                            //print_r($filename);        
                            // Initialize array
                            //$data['filenames'][] = $filename;
                          }
                        if(!empty($filename)){
                            $data1 = ["recruiter_id"=>$userData['rid'],"pic" => $filename];
                            $this->Recruit_Model->site_image_insert($data1);
                        }
                        /*$data1 = ["recruiter_id"=>$userData['rid'],"pic" => $filename];
                        $this->Recruit_Model->site_image_insert($data1);*/
                    }
                    
                     
                }
                if($userData['toppicks']) {
                    $topPicks = $userData['toppicks'];
                    $x=0;

                    foreach ($topPicks as $topPick) {
                        $dataPicks[$x] = ["recruiter_id"=>$userData['rid'], "picks_id"=>$topPick];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_toppicks_delete($userData['rid']);
                    $this->Recruit_Model->recruiter_toppicks_insert($dataPicks);
                }
                if($userData['allowances']) {
                    $topAllos = $userData['allowances'];
                    $x=0;
                    foreach ($topAllos as $topAllo) {
                        $dataAllos[$x] = ["recruiter_id"=>$userData['rid'], "allowances_id"=>$topAllo];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_allowances_delete($userData['rid']);
                    $this->Recruit_Model->recruiter_allowances_insert($dataAllos);
                }
                if($userData['medical']) {
                    $topMedis = $userData['medical'];
                    $x=0;
                    foreach ($topMedis as $topMedi) {
                        $dataMedis[$x] = ["recruiter_id"=>$userData['rid'], "medical_id"=>$topMedi];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_medical_delete($userData['rid']);
                    $this->Recruit_Model->recruiter_medical_insert($dataMedis);
                }
                if($userData['leavs']) {
                    $topLeaves = $userData['leavs'];
                    $x=0;
                    foreach ($topLeaves as $topLeave) {
                        $dataLeaves[$x] = ["recruiter_id"=>$userData['rid'], "leaves_id"=>$topLeave];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_leaves_delete($userData['rid']);
                    $this->Recruit_Model->recruiter_leaves_insert($dataLeaves);
                }
                
                if($userData['shifts']) {
                    $topShifts = $userData['shifts'];
                    $x=0;
                    foreach ($topShifts as $topShift) {
                        $dataShifts[$x] = ["recruiter_id"=>$userData['rid'], "workshift_id"=>$topShift];
                        $x++;
                    }
                    $this->Recruit_Model->recruiter_shifts_delete($userData['view']);
                    $this->Recruit_Model->recruiter_shifts_insert($dataShifts);
                }

                $this->session->set_tempdata('inserted','Company details updated successfully',5);
                redirect("recruiter/recruiter/companyprofile");
            /*} else {
                redirect("recruiter/recruiter/editSite");
            }*/
        }
    }
    
    public function recruiterprofile() {
        $userSession = $this->session->userdata('userSession');
        $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);
        $data['companyDetails'] = $this->Recruit_Model->company_details_single($userSession['id']);
        $this->load->view("recruiter/editprofile", $data);
    }

    public function profileupdate() {
        $userSession = $this->session->userdata('userSession');
        $userData = $this->input->post();
        $this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[25]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric');

        if ($this->form_validation->run() == FALSE) {
            $data['errors'] = $this->form_validation->error_array();
            $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);
            $data['companyDetails'] = $this->Recruit_Model->company_details_single($userSession['id']);
            $this->load->view('recruiter/editprofile', $data);
        } else {
            $name = $userData['name'];
            $name = explode(" ", $name);
            $data1 = ["fname" => $name[0], "lname"=> $name[1], "email"=> $userData['email']];
            $recruiterInserted = $this->Recruit_Model->recruiter_stepone_update($data1, $userSession['id']);
            $data2 = ["phone"=> $userData['phone']];
            $this->Recruit_Model->recruiter_steptwo_update($data2, $userSession['id']);
            redirect("recruiter/recruiter/recruiterprofile");
        }
    }

    public function changePassword() {
        $userSession = $this->session->userdata('userSession');
        $userData = $this->input->post();
        $userData = $this->input->post();
        $this->form_validation->set_rules('password', 'New Password', 'trim|required');
        $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|matches[password]');
        
        if ($this->form_validation->run() == FALSE) { 
            $data['errors'] = $this->form_validation->error_array();
            $data['companie'] = $this->Recruit_Model->company_single($userSession['id']);
            $data['companyDetails'] = $this->Recruit_Model->company_details_single($userSession['id']);
            $this->load->view('recruiter/editprofile', $data);
        } else {
          $data = ["email" => $userSession['email']];
          $recruiterCheck = $this->Recruit_Model->recruiter_login($data);
          
          if($recruiterCheck) {
              $hash = $this->encryption->encrypt($userData['password']);
              $data = ["password" => $hash];
              $this->Recruit_Model->password_update($data, $recruiterCheck[0]['id']);
              $this->session->set_tempdata('item', 'Password Changed Successfully', 2);
              redirect("recruiter/recruiter/recruiterprofile");
          } else{
              redirect("recruiter/recruiter/recruiterprofile");
          }
        }
    }

    public function recruiter_location() {
        $recruiter_id = $this->input->post('recruiter_id');
        //echo $recruiter_id;die;
        /*$data['companie'] = $this->Recruit_Model->company_single($recruiter_id);
        $data['companyDetails'] = $this->Recruit_Model->company_details_single($recruiter_id);
        $data['companySites'] = $this->Recruit_Model->company_sites($recruiter_id);*/
        $topPicks = $this->Recruit_Model->company_topicks_single($recruiter_id);
        //var_dump($topPicks);die;
        if($topPicks) { $x1=0;
            foreach ($topPicks as $topPick) {
              $topPicks1[$x1] = $topPick['picks_id'];$x1++;
            }
            $data['topPicks2'] = $topPicks1;
        } else{
            $data['topPicks2'] = [];
        }

        $allowances = $this->Recruit_Model->company_allowances_single($recruiter_id);
        if($allowances) { $x1=0;
            foreach ($allowances as $allowance) {
              $allowance1[$x1] = $allowance['allowances_id'];$x1++;
            }
            $data['allowance2'] = $allowance1;
        } else{
            $data['allowance2'] = [];
        }

        $medicals = $this->Recruit_Model->company_medical_single($recruiter_id);
        if($medicals) { $x1=0;
            foreach ($medicals as $medical) {
              $medical1[$x1] = $medical['medical_id'];$x1++;
            }
            $data['medical2'] = $medical1;
        } else{
            $data['medical2'] = [];
        }
        
        $leaves = $this->Recruit_Model->company_leaves_single($recruiter_id);
        if($leaves) { $x1=0;
            foreach ($leaves as $leave) {
              $leave1[$x1] = $leave['leaves_id'];$x1++;
            }
            $data['leave2'] = $leave1;
        } else{
            $data['leave2'] = [];
        }
        
        $workshifts = $this->Recruit_Model->company_workshifts_single($recruiter_id);
        if($workshifts) { $x1=0;
            foreach ($workshifts as $workshift) {
              $workshift1[$x1] = $workshift['workshift_id'];$x1++;
            }
            $data['workshift2'] = $workshift1;
        } else{
            $data['workshift2'] = [];
        }
        
        echo json_encode($data);
       
    }
    

    public function sendSms(){

    $this->load->library('twilio');
    $sms_sender = trim($this->input->post('sms_sender'));
    $sms_reciever = $this->input->post('sms_recipient');
    $sms_message = trim($this->input->post('sms_message'));
    $from = '+'.$sms_sender; //trial account twilio number
    $to = '+'.$sms_reciever; //sms recipient number
    $response = $this->twilio->sms("+12014823386","+91 821 852 4174","hello");
    print_r($response);
    if($response->IsError){

    echo 'Sms Has been Not sent';
    }
    else{

    echo 'Sms Has been sent';
    }
    }

    
    function deleteimg(){
        $id = $this->input->post('id');
        $delete = $this->Recruit_Model->deleteimg($id);
        //echo $id;
        //echo $this->db->last_query();die;
        
    }
    
    function getToken(){
        $string = "";
        $chars = "0123456789";
        for($i=0;$i<4;$i++)
        $string.=substr($chars,(rand()%(strlen($chars))), 1);
        return $string;
    }


   public function upload(){

    //print_r($_FILES);die;
    if(isset($_POST["image"]))
    {
      $data = $_POST["image"];

      
      $image_array_1 = explode(";", $data);

      
      $image_array_2 = explode(",", $image_array_1[1]);

      
      $data = base64_decode($image_array_2[1]);

      $imageName = time() . '.png';

      file_put_contents($imageName, $data);
      echo base_url().$imageName;
      

    }

    }

    function alpha_dash_space($str_in = '')
    {
        $str_in = trim($str_in);
        if (empty($str_in))
        {
            $this->form_validation->set_message('alpha_dash_space', 'Name field is required.');

            return FALSE;
        }
        if (! preg_match("/^([a-z, ])+$/i", $str_in))
        {
            $this->form_validation->set_message('alpha_dash_space', 'The name field may only contain characters and spaces.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function valid_password($password = '')
    {
        $password = trim($password);

        $regex_lowercase = '/[a-z]/';
        $regex_uppercase = '/[A-Z]/';
        $regex_number = '/[0-9]/';
        $regex_special = '/[!@#$%^&*()\-_=+{};:,<.>§~]/';

        if (empty($password))
        {
            $this->form_validation->set_message('valid_password', 'Password is required.');

            return FALSE;
        }

        if (preg_match_all($regex_lowercase, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets,numeric and special characters.');

            return FALSE;
        }

        /*if (preg_match_all($regex_uppercase, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password must have at least one uppercase letter.');

            return FALSE;
        }*/

        if (preg_match_all($regex_number, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets,numeric and special characters.');

            return FALSE;
        }

        if (preg_match_all($regex_special, $password) < 1)
        {
            $this->form_validation->set_message('valid_password', 'Password should be mixture of alphabets,numeric and special characters.');

            return FALSE;
        }

        /*if (strlen($password) < 5)
        {
            $this->form_validation->set_message('valid_password', 'Password must be at least 5 characters in length.');

            return FALSE;
        }

        if (strlen($password) > 32)
        {
            $this->form_validation->set_message('valid_password', 'Password cannot exceed 32 characters in length.');

            return FALSE;
        }*/

        return TRUE;
    }
}
?>  
